# Notice

Code and documentation Copyright (C) 2015-2018 Creare. Code released under the Apache v2 License, provided in [LICENSE](LICENSE). All rights reserved.

Creare has used commercially reasonable efforts in preparing the
TabSINT Software but makes no guarantee or warranty of any nature
with regard to its use, performance, or operation.

Creare makes no representations or warranties, and Creare shall
incur no liability or other obligation of any nature whatsoever to
any person from any and all actions arising from the use of this
software.  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. The final
responsibility for the proper use and functioning of the TabSINT
Software shall rest solely with the USAMRAA.

## Awknowledgement

This code development was supported by the US Army Medical Research Materiel Command and the Army Public Health Command under SBIR Phase III Award #W81XWH-13-C-0194 to [Creare LLC](www.creare.com). In particular, we gratefully acknowledge the support and contributions of the Audiology and Speech Center at the Walter Reed National Military Medical Center and the Department of Defense Hearing Center of Excellence in the development and extensive testing of this software.

## Third-Party Licenses

The TabSINT Software relies on many open source libraries.
We recommend you read their licenses, as their terms may differ from the terms described in our [LICENSE](LICENSE):

* AngularJS ([MIT license](https://github.com/angular/angular.js/blob/master/LICENSE))
* Cordova  ([Apache V2 License ](https://github.com/apache/cordova-android/blob/master/LICENSE))
* jquery ([License](https://github.com/jquery/jquery/blob/master/LICENSE.txt))
* Angular-UI ([MIT](https://github.com/angular-ui/bootstrap/blob/master/LICENSE))
* Bootstrap ([MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE))
* Lodash ([MIT](https://raw.githubusercontent.com/lodash/lodash/4.12.0/LICENSE))
* RequireJs ([BSD, MIT](https://github.com/jrburke/requirejs/blob/master/LICENSE))
* d3 ([License](https://github.com/mbostock/d3/blob/master/LICENSE))
* ngStorage ([MIT](https://github.com/gsklee/ngStorage/blob/master/LICENSE))
* ZXing barcode scanning library ([Apache V2](https://github.com/zxing/zxing/wiki/License-Questions))
* Gitbook ([Apache V2 License](https://github.com/GitbookIO/gitbook/blob/master/LICENSE))
* CryptoJS ([MIT](https://github.com/brix/crypto-js/blob/develop/LICENSE))

Additional files included in `/node_modules`, `/www/bower_components`, `/plugins`, and `tabsint_plugins` directories are externally maintained libraries used by this software, which have their own licenses. 
