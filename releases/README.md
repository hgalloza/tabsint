# TabSINT Releases


> Releases before TabSINT 2.0 are available for [TabSINT](https://gitlab.com/creare-com/tabsint/tags) and for [TabSINT with WAHTS](https://gitlab.com/creare-com/tabsint-cha/tags) separately on Gitlab. 
> Releases for TabSINT 2.0, which include support for the WAHTS, are listed below.

<hr>

## Latest

#### v2.6.1 `beta`


- Firmware: `2018-09-14_rhino_4`
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/b20e92c2d910c9213d6f26572a74ea72/tabsint.apk)

<hr>

## Stable

#### v2.5.2

- Firmware: `2018-09-14_rhino_4`
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/542abe855c37e3ee2011c3a078cc3321/tabsint.apk)

<hr>

## Previous

#### v2.6

- [v2.6.0 (`2018-09-14_rhino_4`)](https://gitlab.com/creare-com/tabsint/uploads/d00843d7863680184308e40a5a378646/tabsint.apk)

#### v2.5

- [v2.5.2 (`2018-09-14_rhino_4`)](https://gitlab.com/creare-com/tabsint/uploads/542abe855c37e3ee2011c3a078cc3321/tabsint.apk)
- [v2.5.1 (`2018-09-14_rhino_4`)](https://gitlab.com/creare-com/tabsint/uploads/4d29193e6e5140bd8ad243015a0fd533/tabsint.apk)
- [v2.5.0 (`2018-08-24_rhino_3`)](https://gitlab.com/creare-com/tabsint/uploads/4214c3534489ba0c061fd62e9f4f7233/tabsint.apk)

#### v2.4

- [v2.4.4 (`2018-07-03_rhino_2a`)](https://gitlab.com/creare-com/tabsint/uploads/befbe8fc5317e6c4cc2694e42bb90c66/tabsint.apk)
- [v2.4.3 (`2018-07-03_rhino_2a`)](https://gitlab.com/creare-com/tabsint/uploads/8af1278cf6212e7cc5050c450c3da515/tabsint.apk)
- [v2.4.2 (`2018-07-03_rhino_2a`)](https://gitlab.com/creare-com/tabsint/uploads/a8fcdaaffd2e74ab4ecd92089666fe78/tabsint.apk)
- [v2.4.1 (`2018-07-03_rhino_2`)](https://gitlab.com/creare-com/tabsint/uploads/529950c386ec4a7b3a5aef9fec47f973/tabsint.apk)
- [v2.4.0 (`2018-07-03_rhino_2`)](https://gitlab.com/creare-com/tabsint/uploads/5c5c4fc72bd3be435fa39b6db25c73bd/tabsint.apk)

#### v2.3

- [v2.3.1 (`2018-05-07_rhino_1`)](https://gitlab.com/creare-com/tabsint/uploads/ec15cf51e6d88cfc8e4f56c6a6abf7f5/tabsint.apk)
- [v2.3.0 (`2018-05-07_rhino_0`)](https://gitlab.com/creare-com/tabsint/uploads/cb65475d08f46ec3ce48eab899dac062/tabsint.apk)

#### v2.2

- [v2.2.2 (`2018-04-05_porpoise_0`)](https://gitlab.com/creare-com/tabsint/uploads/fb0aeacd95827baa8e5d26d3ea3e0e22/tabsint.apk)
- [v2.2.1 (`2018-04-05_porpoise_0`)](https://gitlab.com/creare-com/tabsint/uploads/f248a4eab349dba64435de44ac13e2f5/tabsint.apk)
- [v2.2.0 (`2018-04-05_porpoise_0`)](https://gitlab.com/creare-com/tabsint/uploads/22990aadb000b8a95a535fe7bfccd8af/tabsint.apk)

#### v2.1
- [v2.1.11 (`2018-06-28_orca_9`)](https://gitlab.com/creare-com/tabsint/uploads/d6e6460c32c25f5a4441f3ccea08a92d/tabsint.apk)
- [v2.1.10 (`2018-04-05_orca_8`)](https://gitlab.com/creare-com/tabsint/uploads/b11b72a4b062b7b66e4c71267930eec6/tabsint.apk)
- [v2.1.9 (`2018-04-05_orca_8`)](https://gitlab.com/creare-com/tabsint/uploads/5ece9687782c0895fd26105e5ca421cf/tabsint.apk)
- [v2.1.8 (`2018-04-05_orca_8`)](https://gitlab.com/creare-com/tabsint/uploads/a2e2ea793ba2b3c1c571ea594932fd8a/tabsint.apk)
- [v2.1.7 (`2018-04-05_orca_8`)](https://gitlab.com/creare-com/tabsint/uploads/238df432680463a8caf4de083b66ae6d/tabsint.apk)
- [v2.1.6 (`2018-04-05_orca_8`)](https://gitlab.com/creare-com/tabsint/uploads/03c2ab5e108011fcebe8406082595a69/tabsint.apk)
- [v2.1.5 (`2018-03-19_orca_7`)](https://gitlab.com/creare-com/tabsint/uploads/8cd731d3b31f0c2b48b483169b56a150/tabsint.apk)
- [v2.1.4 (`2018-03-19_orca_7`)](https://gitlab.com/creare-com/tabsint/uploads/7352d6d6c35b9a4ea56d588b08f87a33/tabsint.apk)
- [v2.1.3 (`2018-02-16_orca_6`)](https://gitlab.com/creare-com/tabsint/uploads/602b5ce2842a1a90b85365919be15c98/tabsint.apk)
- [v2.1.2 (`2018-02-12_orca_5`)](https://gitlab.com/creare-com/tabsint/uploads/d434cf36d5376a42062f1c3bff2124d8/tabsint.apk)
- [v2.1.1 (`2018-01-04_orca_4`)](https://gitlab.com/creare-com/tabsint/uploads/41df5b6107a5cca0eda69ee9f648b462/tabsint.apk)
- [v2.1.0 (`2018-01-04_orca_4`)](https://gitlab.com/creare-com/tabsint/uploads/aff4533e49ef4edc3d4fa5becb5dc4bd/tabsint.apk)

#### v2.0

- [v2.0.3 (`2017-12-06_orca_3`)](https://gitlab.com/creare-com/tabsint/uploads/3efb928fb1cc8854fa571f0f8b9c89b7/tabsint.apk)
- [v2.0.2 (`2017-11-01_orca_1`)](https://gitlab.com/creare-com/tabsint/uploads/17836f62ac2168abf07849194ae4390d/tabsint.apk)
- [v2.0.1 (`2017-11-01_orca_1`)](https://gitlab.com/creare-com/tabsint/uploads/3691cc88fd17f0130e27a9ba58c2ae91/tabsint.apk)
- [v2.0.0 (`2017-11-01_orca_1`)](https://gitlab.com/creare-com/tabsint/uploads/d0505c222226f74af2467d51af288a1f/tabsint.apk)
