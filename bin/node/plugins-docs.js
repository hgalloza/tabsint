#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var plugins = require('./util/plugins.js');
var fs = require('fs-extra');
var process = require('process');
var _ = require('lodash');


if (require.main === module) {
  var c = config.load();

  if (c.plugins && _.keys(c.plugins).length > 0) {

    // remove old plugins directory
    fs.removeSync('docs/userguide/src/docs/plugins');
    fs.ensureDirSync('docs/userguide/src/docs/plugins');

    // iterate through each plugin
    _.forEach(c.plugins, function(p, name) {
      p.name = name;

      if (p.docs) {
        log.warn('Plugin Docs are currently unsupported by TaBSINT. This will be fixed in future releases.')

        // log.info('Installing docs for plugin: ' + p.name);
        // var entries = fs.readdirSync(p.docs);

        // // Copy index file
        // if (_.includes(entries, 'index.rst')) {
        //   try {
        //     fs.copySync(p.docs + '/index.rst', 'docs/userguide/src/docs/plugins/plugin-' + p.name + '.rst');
        //   } catch(e) {
        //     log.error('Failed to copy plugin docs index.rst file: ' + p.name, e);
        //     process.exit(2);
        //   }
        // } else if (_.includes(entries, 'index.md')) {
        //   try {
        //     fs.copySync(p.docs + '/index.md', 'docs/userguide/src/docs/plugins/plugin-' + p.name + '.md');
        //   } catch(e) {
        //     log.error('Failed to copy plugins docs index.md file: ' + p.name, e);
        //     process.exit(2);
        //   }
        // } else {
        //   log.error('The plugin docs directory for plugin ' + p.name + ' does not contain and index.rst or index.md');
        //   process.exit(2);
        // }

        // // copy any other files p.name for the plugin
        // if (_.includes(entries, p.name)) {
        //   try {
        //     fs.copySync(p.docs + '/' + p.name, 'docs/userguide/src/docs/plugins/' + p.name);
        //   } catch(e) {
        //     log.error('Failed to copy plugin docs directory: ' + p.name, e);
        //     process.exit(2);
        //   }
        // } 
      }
    });
  }
}
