#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var fs = require('fs-extra');
var xml2js = require('xml2js');
var _ = require('lodash');


if (require.main === module) {
  var c = config.read();

  var parser = new xml2js.Parser();
  parser.parseString(fs.readFileSync('config.xml'), function (err, configxml) {
    if (err) {
      log.error('Error parsing config.xml', err);
    } else {
      afterParsingXml(configxml);
    }
  });

  function afterParsingXml(configxml) {
    log.info('Build Configuration: \n \
      \n\tConfig File: ' + c.filename + '\
      \n\tName: ' + configxml['widget']['name'] + '\
      \n\tVersion: ' + configxml['widget']['$']['version'] + '\
      \n\tId: ' + configxml['widget']['$']['id'] + '\
      \n\tCordova Plugins: ' + _.map(c['cordovaPlugins'], function(obj) {return '\n\t\t "' + obj.package + '": ' + obj.src}) + '\
      \n\tTabSINT Plugins: ' + _.map(c['tabsintPlugins'], function(obj, plugin) {return '\n\t\t' + plugin + ' ' + obj.version + ' ' + (obj.debug ? 'DEBUG':'')}) + '\n');
  }

}
