/* author: Brendan Flynn
 *
 *    cmd window 1:  serve
 *    cmd window 2:  protractor protractor-e2e.js
 *
 *  use the element explorer to find elements when building tests:
 *  cd \node_modules\protractor\bin
 *  node ./elementexplorer.js <your url>
 */

'use strict';

describe('Basic tests', function() {

  it('should have a few things defined.', function() {

    browser.get('/www/index.html');

  });
});

describe('Exam progression', function() {

  function setHeadset(headsetName){
    var deferred = protractor.promise.defer();
    function checkName(elm){
      elm.getText()
        .then(function (t) {
          console.log(t);
          if (t.indexOf(headsetName) > -1){
            elm.click();
            console.log('found: '+t);
            deferred.fulfill();
          }
        });
    }

    var list = element.all(by.repeater('(headsetID, headsetDisplay) in headsets'));
    list.count()
      .then(function(c){
        for (var i = 0; i < c; i++) {
          var option = list.get(i).element(by.css('a'));
          checkName(option);
        }
      });
    return deferred.promise;
  }

  function findProtocol(protocolName){
    var deferred = protractor.promise.defer();
    function checkName(elm){
      elm.getText()
        .then(function (t) {
          console.log(t);
          if (t.indexOf(protocolName) > -1){
            elm.click();
            console.log('found: '+t);
            deferred.fulfill();
          }
        });
    }

    var list = element.all(by.repeater('id in protocol.local'));
    list.count()
      .then(function(c){
        for (var i = 0; i < c; i++) {
          var option = list.get(i).element(by.css('label'));
          checkName(option);
        }
      });
    return deferred.promise;
  }

  beforeEach(function(){
    browser.get('/www/index.html');
    //browser.debugger();

    element(by.partialLinkText('Admin')).click();

    // enter authorization
    element(by.model('pin')).sendKeys(7114);
    element(by.partialButtonText('Ok')).click();

//        browser.manage().logs().get('browser').then(function(browserLog) {
//          console.log('log: ' + require('util').inspect(browserLog));
//        });
  });

  it('should set headset and load mock protocol properly', function(){


    // select first protocol ('mock');
    // hard-coded method:
//        var mock = element.all(by.repeater('protocolId in protocol.listProtocolIds')).get(0);
//        expect(mock.getText()).toEqual('mock'); // make sure it's actually mock
//        mock.element(by.name('protocolId')).click();
//        element(by.buttonText('Reset Exam and Change Protocol')).click();

    setHeadset('VicFirth')
      .then(findProtocol('mock'))
      .then(function(){
        console.log('here');
        browser.manage().logs().get('browser').then(function(browserLog) {
          console.log('log: ' + require('util').inspect(browserLog));
        });
        element(by.partialButtonText('Change Protocol')).click();
        // go back to exam view
        element(by.id('top-right-header')).click();
        element(by.linkText('Exam View')).click();

        browser.waitForAngular();

        expect(element(by.binding('dm.page.title')).getText()).toEqual('Mock Protocol for Unit Testing Purposes Only');

        // begin exam
        element(by.partialButtonText('Begin')).click();
      });
  });

//  it('should run through a few pages properly.', function() {



    //console.log(begin);
    //begin = element(by.id('aaa'));
    //console.log(begin);
    //begin.click();


//        expect(element(by.id('top-center-header')).getText())
//            .toEqual('Standard MRT Exam');
//
//        element(by.id('begin-btn')).click();
//
//        expect(element(by.id('top-center-header')).getText())
//            .toEqual('MRT: Presentation 1');

//        expect(element(by.id('submit-btn')).getAttribute('class')).toMatch('disabled');
//
//        element(by.name('DIG')).click();
//
//        element(by.id('submit-btn')).click();

//        expect(element(by.id('top-center-header')).getText())
//            .toEqual('MRT: Presentation 2');

//  });
});
