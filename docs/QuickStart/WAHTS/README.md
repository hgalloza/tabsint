# Wireless Automated Hearing-Test System (WAHTS) Quick Start Guide

<h3>Note:</h3>
----------------------------------------------------
>**In documentation and in TabSINT you may come across the word *CHA*. *CHA* is the nomenclature for the internal electronics inside the WAHTS and inside some other hearing assessment devices. For the sake of ease, consider the two terms interchangeable.**

# Review Contents
## WAHTS in the Box?
<div style="text-align:center"><img src="wahts-in-the-box.png" width="60%"/></div>

Unless otherwise arranged, your package should contain the following:

- One Wireless Automated Hearing-Test System (WAHTS)
- One micro USB cable with wall charger
- Rechargeable lithium-polymer battery (internal)
- One sturdy travel case to carry the above items

# Basic Operation

The WAHTS have an internal rechargeable lithium-polymer battery that must be charged before use. Once received, WAHTS should be turned *On* and may be left *On*. WAHTS only need to be turned off prior to shipment, due to shipping regulations.

## On/Off Switch

- Each WAHTS has an *On/Off* switch in the right/red ear cup. 
- Slide the switch to the *right* to turn it *On*. Sliding to the *left* disconnects the battery.

<div style="text-align:center"><img src="switch.png" width="60%"/></div>

- Once the WAHTS is turned *On*, switch should remain in the *On* position. The WAHTS only needs to be turned *Off* prior to shipping.


## Charging the WAHTS

- Plug the micro USB charger into the port in the left/blue ear cup and connect to the wall charger. <b>The switch must be in the *On* position for the WAHTS to charge.</b>

<div style="text-align:center"><img src="charging.png" width="60%"/></div>

<h3>Note:</h3>
----------------------------------------------------
>The micro USB chargers have an orientation. It is important to plug them in correctly to not damage the charging port.


<div style="text-align:center"><img src="port.png" width="60%"/></div>

- At this time, there is no light or LED to indicate the WAHTS is charging or to indicate when charging is complete. The amount of time required to charge depends on the current battery level, but charging for two to three hours should be plenty to fully charge the WAHTS. Once charged, battery levels can be checked by connecting to CHAMI or [TabSINT](#connecting-wahts-to-tabsint).




## Sleep and Wake 

The WAHTS will enter a low-power idle state automatically after 15 minutes of inactivity; Bluetooth communication with the WAHTS is impossible in the idle state.  

### To wake WAHTS from idle

- Tap the blue ear cup robustly - this will activate the “shake-to-wake” feature.  

![Shake to Wake](BT.mp4)


Looking closely through the black cloth inside the blue earcup, flashing green and blue lights should be visible after the headset has booted up and is ready for operation.

## Operating

The headset communicates over Bluetooth Low Energy (also known as Bluetooth Smart, or Version 4.0+). 

# Connecting WAHTS to TabSINT

If you haven't yet installed TabSINT, return to the [TabSINT Quick Start](http://tabsint.org/docs/QuickStart/TabSINT/) portion of this document.

- Enter the *Admin View* of TabSINT
- Under the *Setup* tab, scroll down to the *WAHTS* panel
- Make sure the Tablet's Bluetooth is turned on, or you will see the message "Bluetooth radio disabled"

<div style="text-align:center"><img src="BT-enabled.png" width="60%"/></div>

- Make sure the headset is turned *On* with blue lights flashing ![Shake to Wake](BT.mp4)
- Select the *Connect* button and a pop up will appear with the Bluetooth Devices that are active and in range. 
- Select the serial number of the headset you wish to connect

<div style="text-align:center"><img src="pop-up.png" width="60%"/></div>

- Once connected a *Headset with Bluetooth* icon will appear in the upper bar and an addtional *WAHTS* panel will appear. This panel identifies the *Name* or serial number, the *Firmware* installed, and the *Battery Level* of the connected WAHTS (CHA).

<div style="text-align:center"><img src="wahts.png" width="60%"/></div>

**Note:**
>The *Headset with Bluetooth* icon should remain visible as long as the headset is connected. If the headset is turned off or is unused long enough to enter  [sleep mode](#sleep-and-wake), it will no longer be connected, but sometimes due to the idle state of the WAHTS and TabSINT, TabSINT may still show the headset as connected. Attempting to run a test will result in an error message that the headset is not connected. To reconnect, power cycle or [Shake to Wake](to-wake-wahts-from-idle) the headset and reconnect based on the [Connecting WAHTS to TabSINT](connecting-wahts-to-tabsint) instructions. 

## Updating Headset Firmware

Each version of tabSINT requires a specific firmware. If the headset connected to tabSINT does not have the required firmware release, you will see an **Alert**

<div style="text-align:center"><img src="firmware.png" width="60%"/></div>

If the firmware doesn't match, you will need to update the firmware.

1. Plug headset into a power source with a USB cable
2. Select *Show Advanced Settings* in the WAHTS panel. From this panel you can see the Firmware that is on the WAHTS vs the Firmware this version of tabSINT requires to function properly. 

	<div style="text-align:center"><img src="update.png" width="60%"/></div>
	
3. Select *Update* and the firmware will begin it's transfer. You will see the progress under *Active Tasks*

	<div style="text-align:center"><img src="transfer.png" width="60%"/></div>

4. Transferring firmware will take a few minutes and then the "Active Task" will state the headset is 'Rebooting' and attempting to 'Reconnect'.
5. Once complete you should see the transferred version displayed in the WAHTS panel and it should match the version listed under tabSINTS firmware.

For more on using the WAHTS with TabSINT, review the [Getting Started](../../UserGuide/getting-started) and [WAHTS](../../UserGuide/cha) section of the User Guide. 