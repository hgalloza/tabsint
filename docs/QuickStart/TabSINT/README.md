# TabSINT Quick Start Guide

This section briefly describes how to acquire and start the TabSINT software.

## Download TabSINT

- From the browser on your tablet, navigate to `http://tabsint.org` and go to the [Releases Page](http://tabsint.org/releases/)
- Click on <img style="width:125px" src="download-for-android.png" /> icon under the desired release version to start downloading `tabsint.apk`. 

- Once `tabsint.apk` has downloaded, press on the file in the status menu (swipe down from the top) or in the *Downloads* folder to begin installing TabSINT.

- To download older versions of TabSINT, see the [Download Older Versions of TabSINT](#download-older-versions-of-tabsint) section below.
 
## Starting TabSINT

- Once installed on your tablet, TabSINT should automatically open. If not, select the TabSINT icon from the **apps** section of your tablet

 <div style="text-align:center"><img src="tabsint-icon.png" width="60%"/></div>

- To begin configuring TabSINT select **Admin View** and enter the **PIN** "7114". 

<div style="text-align:center"><img src="pin.png" width="60%"/></div>


- This PIN can be changed by  clicking on **Show Advanced Settings** in the TabSINT panel, selecting **Admin Mode** under **Preferences** and clicking on the pencil icon button next to the Admin PIN.

<div style="text-align:center"><img src="pin-change.png" width="60%"/></div>

- For more complete information on Configuring TabSINT, loading Protocols, Administering Exams, Viewing Results and more, visit the [Getting Started](http://tabsint.org/docs/UserGuide/getting-started/) portion of this documentation.

## Download Older Versions of TabSINT

TabSINT was previously released both with and without support for the WAHTS. Older releases **without** support for the WAHTS are hosted in the [Tags/Releases section of the tabsint repository](https://gitlab.com/creare-com/tabsint/tags). 
Older releases for TabSINT **with** support for the WAHTS are hosted in the [Tags/Releases section of the tabsint-cha repository](https://gitlab.com/creare-com/tabsint-cha/tags), which requires an authorized login to access.

- From your tablet, go to Tags/Releases section of the [tabsint](https://gitlab.com/creare-com/tabsint/tags) or [tabsint-cha](https://gitlab.com/creare-com/tabsint-cha/tags)
- Download the desired **tabsint.apk** and install as above

<div style="text-align:center"><img src="apk.png" width="60%"/></div>

