# Tablet Setup

This section describes the process of setting up a new tablet. These basic instructions would apply to any tablet, Android or even Apple devices. However, instructions are modeled off the OS Android 6.0.1. If using a different OS, the order of procedures or location of needed files or applications may differ. 

1. [Tablet Start-Up](#tablet-start-up)
2. [Clean and Clear apps](#clean-and-clear-apps)
3. [Adjust Settings](#adjust-settings)
4. [Download and Install TabSINT](#download-and-install-tabsint)
5. [Set Default Browser and Homepage](#set-default-browser-and-home-page)
6. [Set to Developer Mode](#for-developers)


## Tablet Start-up

When tablet is first turned on, it is required to go through Androids start-up procedures. The following has been customary for setting up a tablet for research or development purposes. Modifying for ones personal use circumstance will not affect it's ability to interact effectively with TabSINT.

- **Start** the Android default procedures on the welcome page. Keep **English** selected as the default language.
- **Connect** to desired wifi account, then click **Next**.
- **Uncheck** "Consent to Provide Diagnostic and Usage Data".
- **Agree** to "Terms and Conditions".
- **Skip** adding, creating or linking to any accounts.
- **Date & Time** adjust if needed to local time.
- **Decline** allowing Google to check device activity. 
- **Name** tablet: for example "SK 500" to "SK 600".
- **Do Not** require a PIN, Pattern or Password to unlock, **Uncheck box** and **Skip** this step.
- **Uncheck** all boxes related to *Google services*.
- **Click** next and **Exit** at AT&T Setup and Transfer.
- **Skip** creating or logging into Samsung account.

## Clean and Clear apps

New tablets arrive with many pre-installed apps. These are unnecessary for our applications and clutter up the screen, slow down tablet, and are burdensome with notifications. Let's clean this up!  

Don't worry about cleaning up too much. Apps can always be re-installed or turned back on.


<div align="middle"><img src="Home-App1.png" width="70%"/></div>


- **Tap and Hold** an icon on the home page and drag to the *Remove* option at the top of the screen. Do this for **ALL** icons on the home screen. The *Apps* icon will be left behind.

<div align="middle"><img src="homePage3.png" width="50%"/></div>

- **Select** the *Apps* icon
- **Select Edit** at the top of the screen to begin clearing this page
- **Remove, Turn off or disable** whatever you can
- **Keep** *My Files, Settings, Tools, Gallery and Google Chrome*. Applications Android will not allow to be deleted, stack in a *Not Using* folder.


<div align="middle"><img src="Apps2.png" width="50%"/></div>


## Adjust Settings


- **Select** the Settings Icon ![Settings](Settings.png)
	- *Bluetooth*: toggle switch to *On*
	- *Notifications*: Turn off all of them
	- *Display*: change "Screen timeout" to 5 minutes or 10 minutes
	- *Lock Screen and Security*: "Unknown sources", toggle *On* to Allow installation of apps from other sources (such as TabSINT)
	- *Privacy*: Turn off location permissions
	- *About Device*: Under "Device Name" name device according to a serial identifier that works for your situation


## Download and Install TabSINT

- From the browser on your tablet, navigate to `http://tabsint.org` and go to the [Releases Page](http://tabsint.org/releases/)
- Click on <img style="width:125px" src="download-for-android.png" /> icon under the desired release version to start downloading `tabsint.apk`. 

- Once `tabsint.apk` has downloaded, press on the file in the status menu (swipe down from the top) or in the *Downloads* folder to begin installing TabSINT.

- To download older versions of TabSINT, see the [Download Older Versions of TabSINT](../TabSINT/#download-older-versions-of-tabsint) section in the TabSINT Quick Start.

Once installed it may automatically open. Return to the home page and find the TabSINT icon under "Apps". Move the shortcut to TabSINT to the home page of the device for easy access.

<div align="middle"><img src="home-page-tabsint.png" width="50%"/></div>
 
To begin configuring and using TabSINT, review the [TabSINT](../TabSINT) section of the Quick Start Guide. 

## Set Default Browser and Homepage

For convenience set the default internet browser to Chrome and the default home page to *tabsint.org*.

    
    
- Launch the **Chrome Browser**. 
    <div align="middle"><img src="chrome.png" width="10%"/></div>
- Tap the **Menu icon** located in the upper-right.
    <div align="middle"><img src="menu.png" width="30%"/></div>

- Tap **Settings**
- From the *Basics section*, tap **Home Page**
- Tap the **Home Page switch** to on 
    <div align="middle"><img src="on.png" width="05%"/></div>

- Tap **Open this page** then enter the preferred URL for the Home page. In this case we recommend *tabsint.org*
- Tap **Save**

## For Developers

For some advanced users, developers or under certain circumstances you may want to put the tablet in developer mode. Specifically in this case we recommend turning *USB debugging* to on. 

- Go to **Settings** and scroll down to **About Device**
- Scroll to the **Build Number** tab on the right hand side
- Tap **Build Number** 7 times. A notice that Developer Mode is being activated will appear
- Once in Developer Mode, select **Developer Options** and toggle **USB debugging** to on

For more on Developing with TabSINT visit the [Developer Guide](../../DeveloperGuide) section of this documentation.




