# TabSINT Plugins

TabSINT has a built in API to accomodate custom plugin hardware and software into the app.

The current API supports certain elements and events within the app.

## Getting Started

TabSINT plugins are specified in the `tabsintPlugins` object of the config file for each build. 
The [config schema](/config/config_schema.json) defines precisely how a plugin should be defined.

The only required fields of a plugin definition is `src` and `version`. 
The `src` field specifies the local path to the source for the plugin, and the version of that plugin.
When `src` field path is a git repository, the version must correspond to the current tag on the git repository unless `debug` is true. 

An example plugin definition in the config file would look like:

```json
...
"tabsintPlugins": {
  "pluginName": {
    "version": "Version tag",
    "debug": false,
    "src": "local path to app plugin source code",
    "params": {
      "...": "open object that may contain any configuration parameters for the plugin"
    }
  }
}
```

Once you have defined a plugin in your tabsint configuration file (i.e. `config/your_config.json`), set the configuration file using:

```bash
$ npm run set-config your-config
```

Once you have set your configuration file defining the external plugins, install the defined plugins by running:

```bash
$ npm run tabsint-plugins
```

This command will both install your tabsint plugin source code, as well as any cordova plugins associated with your TabSINT plugin.

## Writing Plugins

### File Structure

Lets assume you want to write a TabSINT plugin named `widget`:

Plugins should be structered as a core `angularjs` module nested within `requirejs` define block.
This module will link all other plugin dependencies into the TabSINT base.

The `angular` module must be injected into your define block in order to define the angular module.
The name of your angular module must exactly match the name of your plugin in the config file.

The most minimal plugin header would look like:

```js
define(['angular'], function (angular) {
  'use strict';

  angular.module('widget', [])
  
    // your plugin factories, services, controllers, etc here
    
});
```

You are welcome to include your own distributed files and angular modules from within main plugin. 
All `require` modules must be referenced using the prefix `plugins/widget` :

```js
define(['angular', 'plugins/widget/file1', 'plugins/widget/nested/file2'], function (angular) {
  'use strict';

  angular.module('widget', ['widgetAngularModule1', 'widgetAngularModule2'])
  
    // your plugin factories, services, controllers, etc here
    
});
```

### API

#### `plugins` Factory

TabSINT provides an injectable `plugins` factory that can be used to access the plugins API.
In order to load your plugin when the main app is first loaded bootstrapped, you must use the `plugins` factory within an angular **run** block.

```js
define(['angular'], function (angular) {
  'use strict';

  angular.module('widget', [])

    .run(function(plugins) {

        // use the plugins factory methods here
        
    });
    
});
```

#### Methods

The `add` method of the plugins API is used to add elements and events to the TabSINT core.

#### `plugins.add.elem`

The method `plugins.add.elem({})` is used to add ui elements to TabSINT. 
This currently includes admin panels, navbar icons, and built in local protocols.

Usage:

```js
plugins.add.elem('[location]', { ... );
```

`[location]` may be one of:

- `adminPanels`
- `navbar`
- `localProtocols`
- `responseArea`

The definition object is different for each location:

- **Admin Panels** (`adminPanels`)

```js
// definition
plugins.add.elem('adminPanel', {
  title: 'Title to show on Admin Panel',
  templateUrl: 'html template file for admin panel, must be relative to "tabsint_plugins/pluginName/..." '
});

// example
plugins.add.elem('adminPanels', {
  title: 'My Plugins',
  templateUrl: 'tabsint_plugins/myplugin/adminpanel.html'
});
```

- **Navbar Item** (`navbar`)


```js
// definition
plugins.add.elem('navbar', {
  templateUrl: 'html template file for navbar icon, must be relative to "tabsint_plugins/pluginName/..." '
});

// example
plugins.add.elem('navbar', {
  templateUrl: 'tabsint_plugins/myplugin/navbar.html'
});
```

- **Built In Protocols** (`localProtocols`)

```js
// definition
plugins.add.elem('localProtocols', {
  src: 'json protocol file to include with build, must be relative to "tabsint_plugins/pluginName/..." '
});

// example
plugins.add.elem('localProtocols', {
  src: 'tabsint_plugins/myplugin/myprotocol.json '
});
```

- **Response Area** (`responseArea`)

```js
// definition
plugins.add.elem('responseArea', {
  templateUrl: 'html template file for response area, must be relative to "tabsint_plugins/pluginName/..." ',
  loadCallback: 'function to call when protocol is loaded with this response area. This function will run before the exam is started'
});

// example
plugins.add.elem('responseArea', {
  templateUrl: 'tabsint_plugins/myplugin/my-respons-earea.html ',
  loadCallback: loadFunction
});
```


#### `plugins.add.onEvent`

The method `plugins.add.onEvent` is used to add callback functions to events in the app.
TabSINT plugins can run functions an any of the following events:

- Switching to Admin View
- Switching to Exam View
- Exam Reset Start
- Exam Reset End
- Page Start
- Page End
- Protocol Response Area Load

Usage:

```js
plugins.add.onEvent('[event]', function() {
    
    // function to run here
  
  }
});
```
  
`[event]` may be one of:

- `switchToAdminView`: Run each time the user switches to admin view
- `switchToExamView`: Run each time the user switches to exam view
- `resetStart`: Run each time an exam begins to reset
- `resetEnd`: Run each time an exam finishes resetting
- `pageStart`: Run each time a page loads in an exam
- `pageEnd`: Run each time a page finishes in an exam


One function may be added to multiple events by passing a list of events to the first argument of the `onEvent` method.
For example, `plugins.add.onEvent(['resetEnd', 'pageStart'], function() {});`

### Examples

To define a custom navbar template:

```js
define(['angular'], function (angular) {
  'use strict';

  angular.module('widget', [])

    // runs right after the app has loaded  
    .run(function(plugins) {
      plugins.add.elem('navbar', {
        templateUrl: 'tabsint_plugins/widget/templates/navbar.html'
      });
    });
    
});
```

If we wanted our own function `widgetFactory.load()` to run every time an exam is reset:

```js
define(['angular'], function (angular) {
  'use strict';

  angular.module('widget', [])
    .factory('widgetFactory', function() {
      return {
        load: function() { console.log('loaded widget') }
      }
    })
  
    .run(function(plugins, widgetFactory) {
      plugins.add.onEvent('resetEnd', function() {
        widgetFactory.load()
      });
    });
    
});
```
