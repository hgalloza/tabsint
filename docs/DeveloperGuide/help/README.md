# Help

Welcome to the TabSINT help section. 

## Common Issues

* **When building the app on the tablet, the build scripts say `Build Success`, but no app shows up on the tablet.**

The most likely cause of the error is that you have a version of TabSINT on the tablet that won't let you install a debug version over it. 
This error looks like `[FAILED_INCONSISTENT_CERTIFICATES]`. In this case, you should uninstall the version of TabSINT on the tablet, then try running the command again.

Cordova run commands can also fail when the android platform in the working copy is outdated. 
In this case, you should re-install the cordova platform by running the following commands in order in the tabsint root directory (android is used in this example):

```bash
$ npm run cordova -- platform rm android
$ npm run cordova -- platform add android
```


* **Permission errors when copying the userguide to TabSINT**

From time to time, you will receive 'permission denied' while trying to copy the userguide documentation to the TabSINT. 
Try deleting the `www/res/documentation` directory (if it exits) and running the command line tool:

```bash
$ npm run docs --tabverbose
```
This will rebuild the documentation and provide more information about the error.


* **Gitbook Build/Serve results in error `Error: ENOENT: no such file or directory, stat ...`**

While trying to run `npm run doc` or a subcommand of this, you may run into an error that looks like:

```
Error: ENOENT: no such file or directory, stat 'C:\Users\mls\Documents\Repositories\HearingProducts\tabsint\_book\gitbook\...'
npm ERR! code ELIFECYCLE
```

This is currently a bug in the gitbook build system. 
To fix this bug on your system, see the fix in this [issue comment](https://github.com/GitbookIO/gitbook/issues/1309#issuecomment-273584516)