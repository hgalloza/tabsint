
# Protocol Development Tools

The [TabSINT user guide](https://creare-com.gitlab.io/tabsint/docs/UserGuide/protocols/) goes into great depth about writing and developing protocols.
The source code has many features to help you debug and refine your protocols.

This document will describe more advanced tools to help in the protocol development process. 

## Validating a Protocol

The npm scripts have a built in validator to check the your protocol conforms to the master schema. To validate a protocol, run:

``` bash
$ npm run protocol-directory   # shorthand if protocol directory is in `www/protocols`
$ npm run validate path/to/protocol-directory
```

If the input is not a path (i.e. `mockProtocol`), the validator will look in the `www/protocols/` directory.

If the input is a path (i.e. `other-directory/mock`), the validator will look for the protocol inside that directory.
All input paths must be relative to the tabsint root directory.

## Testing a Protocol in the Browser

Serving the app provides a great way to test and iterate protocols quickly.
As described in [Building and Running](../building-running/README.md), you can serve the app in the browser using the command: `npm run serve`
This server will update every time a change is in the source code.

#### Load the Protocol in the App

Create a new directory in the root of your project titled `protocols`. Inside this new directory, make a new directory named for your protocol. Place the protocol files in this directory, including your `protocol.json`. 

Now, in your config file (`/www/tabsintConfig.json`) include a new key named `protocols` and set the value to be an array of all the protocol directory names you want to load. For instance, if your protocol is in a directory `myProtocol`, you would write into your config file:

```
{
  "build": "tabsint",
  "description": "Official tabsint release for android",
  "platform": "android",
  "protocols": [
    "myProtocol"
  ],
  ...
```

Now that your protocol is available to the source code, open TabSINT in the browser and navigate to the *Protocols* section of the **Admin Page**. You should see your protocol listed in the table. Select your protocol in this pane, then click `Load`. You should now be able to switch to Exam View and begin testing your protocol.

