# TabSINT Server

TabSINT was originally built to work with a back-end LAMP (Linux-Apache-MySQL-Php) server.
The original TabSINT server functioned to serve protocols and store results, as well as provide calibrated media for certain devices and headsets. We have since implemented support for the [Gitlab](https://about.gitlab.com/) data interface to provide a similar service without needing to support a seperate server instance.

Currently, the source code for the TabSINT Server is closed source, but we plan to release the API for the server so those with engineering resources could create their own *TabSINT Server*.

Please contact [tabsint@creare.com](mailto:tabsint@creare.com) if you are interested in setting up your own TabSINT Server instance.