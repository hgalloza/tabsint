# Device Storage

Data can be transferred into and out of TabSINT using the SD card of the tablet.
Currently, this feature is only supported for **Android** tablets.

## Deploying Protocols

Protocols can be manually distributed and loaded onto tablets using the SD card.

- Each protocol must consist of one directory with a `protocol.json` file in the top of the directory.
- The protocol file can reference media (images, video, audio) relative to the `protocol.json` file stored anywhere in the directory.

**Device Storage** must be selected as the *Server* in the *Admin View* in order to access this feature. 

<div style="text-align:center"><img src="server-SDcard.png" width="60%"/></div>

### Adding Protocols

#### Prepare the Tablet

Ensure the Tablet is set up to allow the transfer of files on Windows and Mac.

- Go to settings by swiping down near the upper right corner on the tablet
	- Select *Storage*
	- Tap the three dots in the upper right hand corner, then select *USB Computer Connection*

<div style="text-align:center"><img src="USBConnect.png" width="60%"/></div>

- Ensure that the box next to *Media Device (MTP)* is checked
	
<div style="text-align:center"><img src="MediaDevice.png" width="60%"/></div>

#### Add the protocol onto the tablet SD card

- Plug the tablet into the computer using an appropriate USB cable
- Windows:
    - Go to *Computer*, then look for your tablet under *Devices*
    - Click on the tablet name, then click on *Internal Storage*
	- Place the protocol you want to transfer in one of the directories under *Internal Storage*
	
<div style="text-align:center"><img src="InternalStorage.png" width="60%"/></div>

- Mac: 
    - You must have [Android-File-Transfer](https://www.android.com/filetransfer/) installed to move files from a mac to an Android tablet
    - Once the tablet is connected, Android-File-Transfer should automatically open, showing you the directory contents of the tablet
- Drag and drop your protocol directory anywhere onto the SD card of the tablet.

#### Load the Protocol in TabSINT 

- Navigate to the *Protocols* tab
- Below the *Protocols* pane, you should see a pane titled *Add Local Protocols*

<div style="text-align:center"><img src="addProtocols.png" width="60%"/></div>

- Press *Add Protocol*
- Navigate to the directory with your `protocol.json` file, then press *Select Directory* in the bottom of window

<div style="text-align:center"><img src="DirectoryChooser.png" width="60%"/></div>

## Exporting Results

To export results to the Device Storage of the tablet, you must select **Device Storage** under *Server* from the configuration panel on the Admin Page.
You must be in admin mode to change this option.

Once this option is selected, all test results will be automatically exported to the SD card.

<div style="text-align:center"><img src="server-SDcard.png" width="60%"/></div>

### Select Results Output Directory

The output directory for exported results can be selected in the *Results* tab of the Admin Page.
By default, results will be exported into a `tabsint-results` directory created in the root of the SD card.

- With **Device Storage** selected under *Server* from the configuration panel, navigate to the *Results* tab
- Below the *Completed Tests* pane, you should see a pane titled *Local Output*
- Press *Change Results Directory*
- In the pop up window, Navigate to the directory in which you would like to export results, then press *Select Directory* in the bottom of window

<div style="text-align:center"><img src="output-sd-card.png" width="60%"/></div>
