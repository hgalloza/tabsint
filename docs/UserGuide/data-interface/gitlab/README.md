# Gitlab

TabSINT can interface with remote Gitlab repositories to load protocols and export results.

[Gitlab](https://about.gitlab.com/) is a program and web service for managing file repositories based on the **git** version control system.
Users can host their own instance of Gitlab on a local server, or choose to use [Gitlab.com](https://gitlab.com) to host repositories.

The following instructions assume you are using **Gitlab.com** to manage your repositories, but TabSINT will interface with any Gitlab instance. 
Please contact [tabsint@creare.com](mailto:tabsint@creare.com) if you would like more information about configuring a Gitlab instance to run with TabSINT.

## Background

[Gitlab](https://about.gitlab.com/) is a web service for managing file repositories based on the **git** version control system.
**Git** is a powerful program used to version-control files, similar to *tracking-changes* in a Microsoft Word doument, but much more detailed.
We use Gitlab to track the changes to protocols to maintain versions that can be revisited at a later time.

For background on **git** or git based version control management, see the following links:
- [Try Git](https://try.github.io/levels/1/challenges/1)
- [Git Basics](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
- [The Simple Guide to Git](http://rogerdudler.github.io/git-guide/)


## Gitlab.com Setup

Gitlab.com acts as the document store and version tracker for protocol and result files.
Because Gitlab is built as a remote host for **git**-based repositories, you can choose to manage your repositories locally using **git** and push them to a gitlab.com repository.

If you are unfamiliar with **git**, you can fully manage your protocols and results using the [Gitlab Web Interface](https://gitlab.com/users/sign_in).
The instructions below provide a primer on creating and managing repositories from the Gitlab web interface.

### Logging In

- Visit the [Gitlab Sign In Page](https://gitlab.com/users/sign_in) and sign in with an existing gitlab account, or create a user account and then sign-in
- Once logged in, you will see a listing of your **repositories** (a.k.a **Projects**)

<div style="text-align:center" width="100%"><img src="gitlab-home.png"/></div>

### Creating a Repository

- Click on the **+ New Project** button in the top right of the **Projects** page
- Fill out the project path
    - Select a **Project name** for the repository. By default, the repository will be created in your own **group**.  The **group** is sometimes referred to as a **namespace**.
	- You can change the name of the **Project Owner** using the drop down menu.
    - In the example below, we have decided to name the repository **Example-name**
- Type in a description for the repository
- Select the visibility of the repository. Generally, you want to leave this as `Private`

<div style="text-align:center" width="100%"><img src="creating-gitlab2.png"/></div>

- Press **Create Project**, and you will be brought to your new project page

<div style="text-align:center" width="100%"><img src="Create_Project2.png"/></div>

### Initializing a Repository 

- Below where the screen says **The repository for this project is empty**, click on the blue text **README**
    - This will start a new file within your repository titled **README.md**
- Fill in any text that you would like to use to describe your repository.

<div style="text-align:center" width="100%"><img src="ReadMe_Gitlab2.png"/></div>

- Press **Commit Changes** at the bottom of the page when you are done. Don't worry, this file can always be changed later.
- You will be automatically transported to a formatted view of the `README.md` file.  If you are not taken to this page
click the *Project* tab (to refresh the page) and then *Repository* tab and then **Files** at the upper portion of the page.

<div style="text-align:center" width="100%"><img src="gitlab-committed2.png"/></div>

- Now you can add files to your repository using the Gitlab web interface.

### Adding Files to a Repository

- If you are not already at the *Files* interface to your repository, click on the **Files** link on the left side of the page. 

<div style="text-align:center" width="100%"><img src="gitlab-files2.png"/></div>

- Press the **+** button to the right of your protocol name
    - Select `New File`, `Upload File` or `New Directory`, whichever action you would like to perform.

Multiple files can be uploaded (a.k.a **pushed**) to a repository at the same time using the git cli or a git graphical user interface.
Some useful git gui programs include:

- [GitHub Desktop](https://desktop.github.com/) (even though this is made by Github, you can still manage Gitlab repositories)
- [SourceTree](https://www.sourcetreeapp.com/)
- [Git Tower](https://www.git-tower.com/)
- [TortoiseGit](https://tortoisegit.org/)

- Below shows the *Example-name* repository now populated with a protocol and media files, ready to be loaded onto TabSINT.

<div style="text-align:center" width="100%"><img src="gitlab-files3.png"/></div>

### Tagging a Repository

Protocol repositories may be tagged in order to reference specific versions downloaded by TabSINT. 
TabSINT will download files associated with the latest tag in the protocol repository when tabsint is asked to update a protocol.

Tags can be used to **release** a new version of a protocol.

- Enter the project you would like to tag
- Click on **Tags** in left menu
- Click on the **New Tag** button
- Enter a tag name. This will be the saved **version** of the protocol repository when TabSINT downloads or updates a protocol.  
    - Enter a message, and release notes, if you would like

<img src="Gitlab_Tag2.png" style="width: 100%" />

- Press **Create Tag**

## Configuring TabSINT

TabSINT can be configured to communicate with a remote Gitlab repository in the **Configuration** tab of the *Admin view*.

- Select **Gitlab** under the *Protocol* Tab and then select *Show Advanced Settings*
    - A **Gitlab Configuration** pane will appear below.
- If your build of TabSINT has been pre-configured with Gitlab credentials, the fields should already be filled in.  
    - If you have changed the fields and would like to reset to the build configuration, click **Reset to Default**
    - If no credentials are found in the build file, the values will not change.

<div style="text-align:center" width="60%"><img src="gitlab-server-config2.png"/></div>

- **Host**: This is the host url of the gitlab instance you are using to manage repositories 
    - If you use `gitlab.com` to access your repositories, the host should be set to `https://gitlab.com/`
    - If you locally host your own gitlab instance, enter the root url used to access that instance (i.e. `https://myowngitlab.com/`)
- **Token**: The token is a secret key used to access your repositories. This must be entered correctly (case-sensitive) in order to sucessfully download repositories. To find your token on Gitlab.com:
    - Click on your account name in the bottom left of the browser window
    - Click on **Profile Settings**
    - Click on **Account** in the top of the window
    - Look for **Personal Access Tokens** at the top of the page
        - **NOTE**: Tokens allow access to any repositories that are attached to your username. Please keep the token safe and private!
- **Namespace**: This is the group name used to host your repositories. 
    - This will be your username if you have not set up any other groups.



## Deploying Protocols

Gitlab can be used to deploy protocols to remote tablets in real time.
Each protocol must be stored in a single repository.

- Protocol repositories must contain a top level protocol file titled `protocol.json`.
- The protocol file can reference media (images, video, audio) relative to the `protocol.json` file stored anywhere in the repository.

**Gitlab** must be selected as the *Protocol Source* in the *Admin View* in order to access this feature. Refer above to [Configuring TabSINT](#configuring-tabsint) for help.


### Adding Protocols

- Navigate to the **Protocols** tab of the *Admin View*
- In the *Source* panel, make sure Gitlab is selected and the correct *Host*, *Token* and *Group* have been entered into the fields. 
- Type in the name of the protocol repository you want to add (i.e. *Example-name*) The *Version* field can be left blank if it is not relevant. 
- Press **+ Add**
- The protocol repository will be downloaded to the tablet

Below is an example of configuring Gitlab to add the *Example-name* repository (or protocol) to TabSINT. Because there is only one version of the *Example-name* protocol, it is fine to leave that field blank. 

<div style="text-align:center" width="60%"><img src="config-git.png"/></div>

Once the protocol has been added, it will appear as an option in the *protocols* list. Note it list the server identifying the method by which the protocol was added to TabSINT. In this case it was added through Gitlab. 

<div style="text-align:center" width="60%"><img src="add-protocol.png"/></div>

### Updating Protocols

- Select the protocol from the list in the **Protocols** tab of the *Admin View*
- Press **Update**
- If a new repository tag is available, TabSINT will update the local version of the protocol to the latest repository tag.


## Exporting and Uploading Results

If the *Automatically output test results* field is checked under *Admin View* *Setup*, result will be automatically exported or uploaded depending on Server connected to the protocol. For example, the *Example-name* protocol was added using Gitlab, so the results will automatically be uploaded to Gitlab upon completion of an exam. However, in order for results to be uploaded, a *Results* folder must first be created. 

For more on uploading and exporting results, visit the [Results Tab](../../getting-started#results-tab) of the Getting Started section of this documentation.
