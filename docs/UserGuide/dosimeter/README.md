## Dosimeter

The Svantek Dosimeter SV104A has been integrated into TabsINT for recording background noise during TabsINT exams. TabsINT protocol pages can include a `svantek` boolean field to activate the feature, and the device can be connected/disconnected via controls on the Admin page.

# Basic Usage

In order to measure background noise using the Dosimeter, the `protocol.json` file must include the `svantek` property, and the device must be connect from the Admin page prior to the page being loaded. This means the basic usage is to:

1. Add the `svantek: true` propterty to (a) page(s) in the protocol
    - Or use the `svantek-demo` protocol for an example
2. (Optional) To see Dosimeter background noise levels in the audiometry table at the end of an exam, add the `showSvantek` property to the protocol. See the [Demo test protocol](https://gitlab.com/creare-com/tabsint/blob/master/www/res/protocol/svantek-demo/protocol.json) for an example.
3. From the Admin page (with bluetooth turned on), connect to the Dosimeter via the `connect` button contained in the `Dosimeter` section
    - Note the active tasks will indicate the steps during connection, which may include powering on the device. Once done, the Admin page will display `State: 'connected'` in the `Dosimeter` section.
4. Load the protocol containing pages with the `svantek` property.
5. Administer the exam. Each page with the property will automatically start recording when the page begins, and stop recording when it is finished
    - The results are published to the `page.results.svantek` property, which can be viewed during an exam when in admin mode by selecting the `show debug info` option, and navigating to the page results.


A note: if the Dosimeter is not connected when a test attempts to record from it, a notification will appear indicating such, then the test will run. Similarly, if connection is lost for any reason during an exam, the user will be notified and the test will continue running.
