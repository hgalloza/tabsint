# Getting Started #

This brief section gives an overview of how to start, setup, and use the TabSINT software.

## Starting TabSINT ##

TabSINT opens to the welcome screen, showing the options:

<div style="text-align:center"><img src="welcome.png" width="60%"/></div>

- **Exam View**: This option will open the exam view for the current active protocol. If no protocol has been loaded, the page will display *Exam Disabled*.
- **Admin View**: This option will open the administrator console and may require a password to access. The admin console provides the ability to load protocols, view results, and configure the application.
- **Documentation**: This option will open this documentation in a local browser window on the tablet.
  

---

## Configuring TabSINT ##

To access the configuration options in TabSINT:

- From the welcome screen (or the options menu in the top-right), select **Admin View**
- If prompted, type in the **Admin PIN** and click *OK*
- You are now in the admin area, with access to [Setup Tab](#setup-tab), [Protocols Tab](#protocols-tab), [Results Tab](#results-tab) and the *Exam View*.
  

## Setup Tab ##

- Under the ``TabSINT`` header is a choice to **Show Advanced Settings**. Select this to view configuration options.
<div style="text-align:center"><img src="calibration-config2.png" width="60%"/></div>
- ``Headset``: Ensure that this matches the type of headset that you have connected to the tablet (if any). If this value is set incorrectly, then the tablet audio will **not** be calibrated properly.
- ``Admin Mode``: Includes additional configuration options, displays expandable **Debug View** menus at the bottom of exam pages, and suppresses **Admin Pin** prompts. Leave this option *unchecked* unless you are developing protocols or making configuration changes. 
- ``Admin PIN`` can be changed by selecting the pencil icon.
- ``Disable Logs``: Disables automatic software logging in the TabSINT software.
- ``Disable Automatic Volume Control``: This option will disable TabSINT from setting the volume to 100% on every page. Check this box if you would like to set the volume of the app manually using the volume buttons on the outside of the device.
- ``Enable Skip in Exams``: Enables skipping on every page of an Exam.
- ``Tablet Gain``: Apply a special gain in dB to the audio level output through TabSINT. 
- There are also choices regarding the output of [exam results](../analysis).

### Update TabSINT ###

From the **Admin View** -> *Setup* tab,

- Expand the *Software and Hardware* submenu and scroll down to find the *Update TabSINT* subheading
- Click on *View Releases*. This will open your browser to [tabsint.org/releases/](http://tabsint.org/releases/).
- Select and download **Stable** release. This will download a tabsint.apk file to your tablet.
- Navigate to the tabsint.apk file and open to install.
- When installation has completed, open TabSINT and the welcome screen should now show the expected version number at the bottom center.

For more complete information on how to update tabSINT, visit the [FAQ](../../FAQs#how-do-i-update-tabsint) section of this documentation. 

For information on changing to an older version of TabSINT visit the [TabSINT Quick Start](../../QuickStart/tabSINT#download-older-versions-of-tabsint) section of this documentation.


### Update Firmware ###

If using a WAHTS headset, you will need to update the headset firmware to match the firmware released with the latest version of TabSINT. If the headset connected to TabSINT does not have the required firmware release, you will see an **Alert**. Although you can run tabSINT with non-compliant firmware, you may get unexpected results or experience additional error or warning messages. Always update the WAHTS firmware to match the version compatible with the tabSINT release. 

To update firmware visit the [WAHTS Quick Start](../../QuickStart/WAHTS#updating-headset-firmware) section of this documentation. 
  
## Protocols Tab ##

- ``Source``: Select the appropriate location for TabSINT to download or add new protocols and upload results. See the [Data Interface](../data-interface) section for more information.
  
  - ``TabSINT Server``: Use a TabSINT specific server to download protocols and export results based on site names
  - ``Gitlab``: Use your own Gitlab repository to download protocols, export results and for using a common media repository
  - ``Device Storage``: Use your own SD card to download protocols and export results


<div style="text-align:center"><img src="general-config.png" width="60%"/></div>

### Gitlab Configuration ###

See [Gitlab](../data-interface/gitlab) documentation for more information.

- ``Host``: The web address to your gitlab account (i.e. ``https://gitlab.com/``)
- ``Token``: The personal access token to your gitlab account
- ``Group``: The group name of your gitlab repository. This can include subgroups

<div style="text-align:center"><img src="gitlab-server-config.png" width="60%"/></div>

### TabSINT Server Configuration ###

- ``URL``: The web address to your TabSINTs server
- ``Username`` and ``Password``: Your login credentials
- ``Valid``: This will validate your login credentials against the input url
  
<div style="text-align:center"><img src="tabsint-server-config.png" width="60%"/></div>


### Protocols ###

The protocols view lists the protocols already loaded into tabsint.
The currently active protocol will be shaded yellow.

<div style="text-align:center"><img src="protocol-view2.png" width="60%"/></div>


- TabSINT can hold many protocols locally, but only one protocol is active at a time
- Each protocol currently available on the tablet is listed by name, date and server type
  
Protocols are loaded, updated, and deleted by tapping on the Protocols table.

- ``Load``: Load, Validate, and Activate the selected protocol active in the Exam View
- ``Update``: Check for new versions (for gitlab server and TabSINT server only)
- ``Delete``: Remove that protocol from the locally available protocols.
  
  - The protocol information will be removed, and the protocol files will be deleted unless the protocol was loaded via the Device Storage.
    


### Protocol Source View ###

Based on the **Server** seleted in the [Protocols Tab](#protocols-tab), different options will be active under the protocols view.
For more information on sourcing protocols, see the [Data Interface](../data-interface) section.

- **Download Protocol from Gitlab Server** (if ``Protocol Source`` is set to ``Gitlab``)
  
  - ``Name``: Enter the respository (a.k.a. *project*) name for the desired protocol.  This project must be within your configured gitlab namespace.
  - See [Data-Interface/Gitlab](../data-interface/gitlab#deploying-protocols) for more information

- **Add Local Protocols** (if ``Protocol Source`` is set to ``Device Storage``)
  
  - ``Add Protocol``: Opens a window to select a protocol directory from the SD Card on the tablet
  - See [Data-Interface/Device Storage](../data-interface/sd_card#deploying-protocols) for more information

- **Download Protocol from TabSINT Server** (if ``Protocol Source`` is set to ``TabSINT Server``)
  
  - ``Site Name``: Ensure that this site matches siteName configured on the TabSINT server web interface. This value should be provided by the study coordinator, if required.

---

## Administering an Exam ##

While administering an exam may depend on the exact protocol in use, the steps to begin and end an exam are the same:

1. Load a protocol in **Protocols** tab of the **Admin View**.
2. Navigate to the **Exam View**.
3. Be sure that the subject is ready to start the exam, and communicate any necessary oral instructions.
4. Press **Begin** and hand the tablet to the subject.
5. Administer the protocol. When the protocol finishes, the subject should return the tablet to the administrator.
6. Once the exam reaches the final screen, the results will automatically queued for export or upload. The exam results will also get backed up in a local text file on the tablet for safe keeping. At this point, the exam results should be viewable in the **Results** tab of the **Admin View**.
   - If TabSINT is configured to upload results to a remote server and *Automatically output test results* is selected, the exam result will attempt to get uploaded at this point.  If the exam is successfully uploaded, it will be transition to the *Recent Output* section of the **Results** tab. 

Press the ``New Exam`` button if you would like to run another exam on the same protocol.  

## Results Tab ##

<div style="text-align:center"><img src="results2.png" width="60%"/></div>

### Completed Tests ###

This view lists all the test results currently stored in TabSINT's memory.

- Details include protocol name, number of presentations completed, protocol source, and start time
- Tap a specific result to pull up a window showing the details of that result
  
  - Tap any field to expand.  For example, to see responses to specific questions, tap ``testResults``, then
    ``responses``
  - ``Upload``: For Gitlab and TabSINT results servers.  Upload this specific result now.
  - ``Export``: Export results to a local file on the SD Card or Device Storage (The Results Directory can be changed if the results
    location is set to ``Device Storage``)

- ``Export All``: Saves all results to a local file in the specified ``Results Directory``
  
<div style="text-align:center"><img src="results-detail.png" width="60%"/></div>

### Uploading Exam Results ###

Uploading sends exams results to either the Gitlab repository shared with the location of the exam protocol or to the TabSINT Server shared with the exam protocol. 

#### TabSINT server ####

 If the protocol exam was added to TabSINT from a specific TabSINT server, results will be uploaded to this same server. Details for accessing this server would be provided by the study coordinator. 

#### Gitlab ####

- Protocols can be added to TabSINT from a Gitlab repository. More information on adding exams to TabSINT from Gitlab can be found in the [Gitlab](../data-interface/gitlab) section of this documentation. 

- Any protocol identified as being affiliated with the Gitlab server can have results uploaded to that same server. 

<div style="text-align:center"><img src="gitlab-server.png" width="60%"/></div>

- If a *results* folder is not yet set up for that server, you will receive an error message. 
- Set up a *results* folder by going to the group where the protocol repository is housed. Add a *results* folder on the same level as the protocol.

<div style="text-align:center"><img src="results-folder.png" width="60%"/></div>

- Within the *results* folder add a folder for each protocol within this group.  When the protocol results are uploaded they will populate the folder with the same name as the protocol 

<div style="text-align:center"><img src="results-folder2.png" width="60%"/></div>

- Alternatively, if you would like the results uploaded to a different Group or repository, select the *Change Results Location* box and identify the location where you would like the results sent

<div style="text-align:center"><img src="change-results.png" width="60%"/></div>

  

---

## Exporting Exam Results ##

Any results can be Exported onto the tablets *Device Storage*.

<div style="text-align:center"><img src="device-storage.png" width="60%"/></div>

- Select *Device Storage* under *Source* in the Protocols tab. 
- Select *Export all* to save all queued results under *Completed Tests* to the Device Storage or select an individual exam to *Export*
- To change the results directory, in *Admin View* select *Change Directory* under the Local Results Directory. *tabsint-results* is the default directory.

<div style="text-align:center"><img src="change-local.png" width="60%"/></div>

Stored results can be viewed on the **Results** tab of the *Admin View*.
Results are kept in the queue on the tablet until they are uploaded to a configured server or deleted by an administrator.

<div style="text-align:center"><img src="local-server.png" width="60%"/></div>

### Export Single Result ###

- Select the desired *server* for results output in the **Protocols** tab
- **Make sure your selected results output is properly configured**

- Click the result entry in the table found in the *Completed Tests* view of the **Results** tab.
- Select **Export** to export the results to a file on the Device Storage, or **Upload** to send the result to the chosen server.
  

### Exporting all results ###

To export all results, press the **Export All** button below the *Completed Tests* table.


If you have selected a server for your results output, you can press **Upload All** in the *Upload Results* view below *Completed Tests*.

- Uploading results **will** remove results from the *Completed Tests* table.
  

---

## Volume Errors and Alerts When Using Headsets ##

> **Note:** These alerts and errors will most likely only show up when a Headset is plugged in or connected via Bluetooth. 
> Before giving the tablet to a subject to administer an exam, connect headset, reset the exam, and press begin to make sure volume control is functioning properly.


TabSINT attempts to change the volume of the tablet to 100% on many different tablet events.
This controls the output volume of the tablet while playing calibrated media.

Creare has specifically adapted certain tablets to allow TabSINT full control over media volume for calibrated media playback.
On other tablets, TabSINT may have trouble resetting the volume to 100% and will present a pop-up message:
"Listening at a high volume for a long time may damage your hearing.  Tap OK to allow the volume to be increased above safe levels."

### To Enable Full-Scale Volume Control From Within TabSINT ###

- In most cases you have the option to press 'Cancel' or 'OK'.
- Press 'OK' to allow TabSINT to set volume levels in the future.  This setting should persist until the device is rebooted, though some tablets may limit the duration of the setting to 24 hours.
- Press 'OK' for any TabSINT 'ERROR' messages about volume not being properly set.
- Reset the exam to allow the change to take effect.
- The TabSINT 'ERROR' messages should stop appearing once the changes take effect.  If they do not, continue reading.


If you see an error message like:
"ERROR: Volume not properly set..." but you do NOT see the "Listening at high volume..." message, then follow the instructions below to enable full-scale volume control.

### To Enable Full-Scale Volume Control Using Phone Settings ###

- Go to the 'Settings' menu for the phone or tablet.  This may require sliding down from the top right, sliding up from the bottom right, or pressing a home button.
- Go to the volume section.
- Plug in a headset.
- Try to raise the volume to max level.
- If you get a warning about high volume, select the option that allows you to turn the volume up, usually the 'OK' option.
