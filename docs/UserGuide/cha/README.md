# WAHTS

The WAHTS is a hearing assessment device that can be used as a peripheral to TabSINT.

## Basics

To run a protocol with WAHTS exam pages:

1.  Make sure the WAHTS is powered on
2.  Navigate to the admin page in TabSINT, and scroll to the *WAHTS* panel
3.  Select the appropriate Bluetooth Mode
4.  Tap *Connect*
5.  Select the appropriate WAHTS from the list that pops up
6.  The connection is complete when WAHTS info, such as battery level and WAHTS serial number, appear in the panel

## Streaming Audio to the WAHTS

The WAHTS can be used as a standard bluetooth headset to play any audio from the tablet.  To use the WAHTS as a bluetooth
headset, make sure to select "Creare Headset" from the *Headset* drop-down.  With *Headset* set to "Creare Headset", the
audio from any media will play through the WAHTS.

## Loading and Playing Audio Files on the WAHTS

TabSINT can play audio files that are stored on the WAHTS.
These audio files may be located within the WAHTS file system, or they may be audio files loaded onto the WAHTS by the user using CHAMI.

## Loading Audio Files

### Using CHAMI

To load audio files onto the WAHTS using CHAMI, see the `write_file` method in the CHAMI user guide.
Files loaded by the user onto the WAHTS will be placed in the `USER` directory.
When referencing a file in a TabSINT protocol, the path must be prefixed by `C:USER`.

### Using TabSINT

TabSINT can also load audio files onto the WAHTS from any gitlab repository.
Files loaded by the user onto the WAHTS will be placed in the `USER` directory.
When referencing a file in a TabSINT protocol, the path must be prefixed by `C:USER`.

* Create a gitlab repository in your Gitlab Group with the desired audio files
* Tag the repo at the desired commit
* Select `Show Advanced Settings` in the WAHTS settings panel
* Download the repo in the WAHTS admin panel in the `Download Media from Gitlab` section
* Headset and Tablet must be fully charged
* Connect Headset to Tablet using USB OTG (On-The-Go) cable.  A standard micro-to-micro cable will not work - it must be OTG.
* Under `Preferences` in the WAHTS settings panel, for `Type` select `Usb Host` from the drop-down
* Tap `Connect`
* You may get a pop-up `Allow the app TabSINT to access the USB device?`.  Select OK.  Depending on how long it takes you to select `OK`, the connection attempt may fail with a pop-up telling you TabSINT could not connect to the headset.  Just tap `Connect` again.
* Select the media repo to sync, then tap 'Sync to Headset'.  NOTE - do NOT disconnect the headset from the tablet during sync.  This action can lead to corrupted files, and necessitate reformating the headset sdcard and replacing all the media!
* First TabSINT will compare all media files in the repo with the files on the headset.  This can take several minutes for large repos with a lot of files.
* TabSINT will then delete any directories or files found on the headset that are no longer in the repo.
* Next, TabSINT will transfer all new files to the headset.  This step provides progress updates based on number of files and number of kBytes to transfer.

## Playing Audio Files on the WAHTS

Any exam page can play audio files that are stored on the WAHTS.
Playing wav files on the cha is very similar to playing wav files on the tablet.
Instead of using the `wavfiles` key, you would now use the `chaWavFiles` key.
See the `chaWavFiles` key within the `page` field of the protocol schema for the for precise syntax.

Below are two examples showing how to play a wav file on the WAHTS. The first example shows how to reference a wav file that is preloaded on the WAHTS, and the second shows how to reference a wav file that is loaded by the user onto the WAHTS:

```json
{
  "id":"wahtsWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:HINT/LIST1/TIS001.WAV"
    }
  ],
  "responseArea":{
    "type":"multipleChoiceResponseArea",
    "choices":[
      {
        "id":"Choice 1"
      },
      {
        "id":"Choice 2"
      }
    ]
  }
}
```

```json
{
  "id":"chaWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:USER/dir/myfile.wav"
    }
  ]
}
```
