# Example CHA Protocols

## CHA HINT

The HINT exam plays spoken words in noise.

```json
{
  "title":"A Simple CHA HINT Exam",
  "pages":[
    {
      "id": "HINT_1",
      "title": "Audiometry HINT Demo",
      "questionMainText": "HINT Demo",
      "helpText": "Follow instructions",
      "instructionText": "Select Appropriate Words",
      "responseArea": {
        "type": "chaHINT",
        "examProperties": {
          "Type": "military",
          "Direction": "left",
          "NoiseLevel": 50
        }
      }
    }
  ]
}
```
