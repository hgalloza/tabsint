# Protocol Schema #

The complete TabSINT protocol schema is hosted in the TabSINT source code repository on gitlab.

- [TabSINT Protocol Schema](https://gitlab.com/creare-com/tabsint/tree/master/www/res/protocol/schema)

## References

- [http://json-schema.org/](http://json-schema.org/) - information on how json schema files are structured.
