# Developing Protocols

The following section will provide an overview on the methods for developing protocols.

More advanced tools for developing protocols are described in the [Protocol Development Tools](https://creare-com.gitlab.io/tabsint/docs/DeveloperGuide/protocol-dev/) section  of the Developer Guide.

## Protocol Files

At a minimum, protocols **must** contain a file named `protocol.json`. See the [Protocol Design](../protocol_design) section for more details on the protocol architecture.

The protocol may also reference files:

- `.wav`, `.mp3`: audio files
- `.png`, `.jpg`: image files
- `.mp4`: video files
  
Protocol files may be stored in subdirectories, but `protocol.json` must be at the top level. 
All file references must be relative to the root protocol directory (i.e. `images/image1.png`).

## Development Environment

In order to develop protocols most effectively, a user should have:

- A [text editor](../protocol_design#tools-for-writing-json-documents)

- A recent distribution of the [TabSINT source code](https://gitlab.com/creare-com/tabsint)

- A tablet with the [TabSINT release](http://tabsint.org/releases/) installed
      

## Develop Protocols on the Tablet

This method is appropriate for most users. More advanced protocol developers may choose to `develop protocols in a web browser`. 

To load protocols via the Device Storage, see the section on [Device Storage/Deploying Protocols](../../data-interface/sd_card/#deploying-protocols).

To load protocols from a Gitlab Repository, see the section on [Gitlab/Deploying Protocols](../../data-interface/gitlab#deploying-protocols).

### Development Tips ###

- Make sure to validate the syntax of your protocol before loading on to the tablet.
- Select the **Validate Protocols** option within TabSINT to confirm that your protocol validates against the TabSINT protocol schema
  

## Develop Protocols in a Web Browser

This method is appropriate for rapid, iterative protocol development.
To use this method, a user must also install:

- [NodesJS](https://nodejs.org/en/)
- The [Chrome](https://www.google.com/chrome/browser/desktop/) or [Opera](http://www.opera.com) web browser (though Safari and Firefox are also usable)
  

### Steps

- Clone the TabSINT source code from the [TabSINT GitLab Repository](https://gitlab.com/creare-com/tabsint)
- Follow the instructions in [README.md](../../../..) located in the root of the  TabSINT repository to set up the necessary build tools and app dependencies.
  
- Serve TabSINT in a local browser by running the running the following command from a terminal in the root of the repository: 
  
```bash
$ npm run serve
```

- Add a local protocol to TabSINT to test. See [Testing a protocol in the browser](../../../DeveloperGuide/protocol-dev) for the steps to add a local protocol to the application.
- You may also choose to use the protocol `sandbox` that is included with the TabSINT source code. This protocol is already set up to be loaded by the `config/example_config.json` TabSINT configuration file.
- Select the protocol from the Admin View and then press *Load*.
  
### Development Tips

More advanced tools for developing protocols are described in the [Protocol Development Tools](https://creare-com.gitlab.io/tabsint/docs/DeveloperGuide/protocol-dev/) section of the Developer Guide.

- In Chrome, press `F12` (windows) or `Option+Command+J` (mac) to open the developer tools. There, you can :
	- View any errors in the **Console**
	- Set breakpoints in the **Source Code** to explore errors
	- Set the **Emulate** tab to your tablet to see the protocol as it will appear on the tablet
- Use the TabSINT command line tool `npm run validate` to test if your protocol validates against the TabSINT protocol schema. This can also be achieved by selecting the **Validate Protocols** option within TabSINT.

