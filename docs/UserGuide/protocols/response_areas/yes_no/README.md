# Yes-No Response Area #

The Yes-No response area type is used to present the user with a simple yes-no question.

## Options ##

This only options for this response area are the `common_response_area_properties`

## Protocol Example ##
```
{
  "id": "yes-no-id",
  "title": "Yes No Response Area Example",
  "questionMainText":"Is this a yes-no response area example?",
  "questionSubText":"Select yes",
  "responseArea":{
    "type":"yesNoResponseArea"
  }
}
```
## Schema ##
```
"yesNoResponseArea":{
  "description":"A simple yes/no answer. Returns true (Yes) or false (No).",
  "allOf": [
    {"$ref":"#/definitions/commonResponseAreaProperties"},
    {
      "properties": {
        "type": {
          "enum": [
            "yesNoResponseArea"
          ]
        },
        "verticalSpacing": {
          "type": "integer",
          "description": "Vertical spacing between buttons, given in [px]"
        }
      }
    }
  ]
}
```
