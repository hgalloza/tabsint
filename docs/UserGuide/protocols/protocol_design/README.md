# What is a Protocol? #

A protocol is a machine-readable file that defines the content and logic for a set of interactive pages. These pages may be organized to form unique and complex test protocols and questionnaires.

Protocol pages can contain text, audio, images, and videos, as well as [25+ pre-defined response areas](../response_areas).
These response areas include text and integer inputs, as well as many standard speech in noise tests.

## Format ##

Protocols are written in the text-based [JavaScript Object Notation (JSON)](http://www.json.org) format. 
This format is commonly used for transmitting data objects that are both human and machine readable.

```
{
	"key": "value",
	"page": {
		"name": "json example"
	}
}
```

Because this format is machine readable, the syntax is very strict.
Each comma, brace, and quote is significant.

Attention to detail, along with frequent [syntax validation](https://jsonlint.com), will ease the development of the protocol.

## Elements of a Protocol ##

- **Global Properties**: Protocols contain a set of top level properties that define the parameters and context in which a protocol should be administered. The global properties are inhertied by each page and can be used to customize the protocol flow.
- **Pages**: Protocols contain a set of pages that are presented to the user in a specified order. Each page contains a combination of text and media to present to the user and a response-area to collect a user response. 
  

- [Response Areas](../response_areas): The user response can be obtained in many formats, including yes/no, integer, multiple choice, checkbox, Likert, MRT, OMT, and QR codes.
- `Subprotocols`: A protocol can have different sections, referred to as ``subProtocols``, which have their own global properties, including page defaults and page randomization.

- [Dynamic Content](../../advanced-protocols/implementing_logic): In addition to these static attributes, a protocol can be made dynamic using customized logic between pages. This logic includes the capability to skip pages, set test flags, and ask follow on questions. 
  

## Example: Simple Protocol ##

```
{
	"title":"A Very Simple Test",
	"pages":[
		{
			"id":"question1",
			"questionMainText":"How many years of service do you have?",
			"responseArea": {
				"type":"integerResponseArea"
			}
		},
		{
			"id":"question2",
			"questionMainText":"How many times have you been deployed to Iraq or Afghanistan?",
			"responseArea":{
				"type":"integerResponseArea"
			}
		}
	]
}
```

This protocol presents 2 pages, both of which contain question text and an integer response.
The global ``title`` property is inherited by each of the pages, but each page defines its own ``questionMainText`` to present.

The basic JSON structure is apparent :

- double quotes around keys and values, which are paired with a colon (`"key": "value"`
- curly braces to group key/value pairs (`{ "key": "value" }`)
- square braces to denote an array of similar items (`[1, 2, 3]`)
- commas to separate groups (`[{ "key": "value" }, { "key": "value" }]`)
- The line breaks are insignificant. They are an aid for the reader but have no syntactic meaning.

> **Warning**: The JSON structure makes it relatively easy for a human to understand the content of the document at a glance, but writing a JSON document can be tricky since every double quote, colon, comma, curly brace, and square brace has syntactic significance.  
> Misplacing even a single one will result in a syntax error.

## Protocol Schema ##

In addition to writing a syntactically valid JSON document, it must be structured in accord with a **schema**.
The schema defines the properties and property types allowed to be used in a protocol.

See the [Protocol Schema](../protocol_schema) section for more information.

## Tools for Writing JSON Documents ##

There are many tools that can assist in writing syntactically valid protocols.  

**Text Editors**

The following text editors auto indent and highlight matching braces:

- [Notepad++](https://notepad-plus-plus.org)
- [Notepad2](http://www.flos-freeware.ch)
  

**Online Editors**

- [JSON Editor Online](http://www.jsoneditoronline.org): An online editor that presents both a text view and object view. The object view presents a hierarchical representation of the JSON document.
  

**Syntax Validators**

- [JSON Lint](https://jsonlint.com): Copy and paste the protocol text into this editor and it will confirm that JSON syntax is valid
  

## Subprotocols ##

Sets of pages can be grouped together into larger sections referred to as **Subprotocols**.
Subprotocols can define specific attributes to a subset of pages within a larger protocol.

For example, one subprotocol may specify a certain type of randomization, while another does not.
Subprotocols can also be used to repeat or reuse groups of pages in different parts of the protocol.

## Example: Subprotocols ##

```
{
	"title":"A Simple Multiprotocol Exam",
	"pages":[
		{
    			"reference": "no_randomization"
    		},
    		{
    			"reference": "randomized",
    		}
    	],
    	"subProtocols":[
    		{
    			"protocolId": "no_randomization",
    			"pages":[
    				{
    					"id":"question 1-1",
    					"questionMainText":"How many years of service do you have?",
    					"responseArea":{
    						"type":"integerResponseArea"
    					}
    				},
    				{
   	 				"id":"question 1-2",
    					"questionMainText":"How many times have you been deployed to Iraq or Afghanistan?",
    					"responseArea":{
    						"type":"integerResponseArea"
    					}
    				}
    			]
    		},
    		{
    			"protocolId": "randomized",
    			"randomization": "WithoutReplacement",
    			"pages":[
    				{
    					"id": "question 2-1",
    					"questionMainText": "What is your age?",
    					"responseArea":{
    						"type": "integerResponseArea"
    					}
    				},
    				{
    					"id": "question 2-2",
    					"questionMainText": "What is your favorite number?",
    					"responseArea": {
    						"type": "integerResponseArea"
    					}
    				}
    			]
    		}
    	]
}
```
