# Data Encryption #

Tests results that are associated with a protocol containing a RSA public key are stored on the tablet and on Gitlab in .txt base64 format.  

### Encryption Strategy ###

TabSINT uses hybrid encryption to encrypt data that has a RSA public key defined in the associated protocol. If no public key is provided, data is still encrypted within TabSINT, but is saved to the tablet and uploaded to Gitlab in its JSON or CSV unencrypted format. Hybrid encryption is simple, secure and widely used and accepted in cryptography. It combines the high security of RSA public-private asymmetric encryption and the efficiency of symmetric encryption. TabSINT uses the Advanced Encryption Standard (AES) as the symmetric encryption scheme, which was adopted by U.S. NIST in 2001 after thorough testing.

The specific steps TabSINT takes to implement data encryption are:

1. The protocol developer generates public private key pair (`openssl rsa -pubout -in private_key.pem -out public_key.pem`)
<b><u>It is critical that the protocol generator saves the RSA private key and securely shares it with the result analyst. If the private key is lost, the data will be indecipherable. </u></b>
2. As TabSINT prepares result(s) for export on the tablet or upload on Gitlab, it encrypts result(s) with a randomly generated AES key (using the npm package `crypto-js`).
3. Then, TabSINT encrypts the random AES key with the public key in the protocol (using the npm package `js-encrypt`).
4. When retrieving results, the results analyst can use a Matlab tool that decrypts the random AES key with the private key and then decrypts the result(s) with the random AES key.



