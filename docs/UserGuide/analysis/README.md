# Results Analysis #

A variety of resources have been developed for analyzing data collected with the TabSINT app. Below is a brief collection of some of these resources.

## Data Format ##

A primer on the TabSINT results data format can be found [here](data-format/README.md).

## Ambient Noise Data ##

TabSINT supports two sound level meters for measuring background noise during exams:
  * [Sensimetric Sound Level Meter](noise-measurement/slm/README.md)
  * [Svantek Dosimeter](noise-measurement/svantek/README.md)

## Matlab Tool ##

A suite of functions that flatten TabSINT JSON results to support analysis in Matlab. A quick guide to the functions can be found [here](matlab-tool/README.md). The functions can be downloaded from the Gitlab repository [here](https://gitlab.com/creare-com/tabsint/tree/master/tools/matlab).