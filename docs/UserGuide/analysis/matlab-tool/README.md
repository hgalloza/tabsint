# Matlab Analysis Tool #

To facilitate the analysis of TabSINT results in Matlab, a set of Matlab functions to parse JSON results and isolate result fields have been developed. These functions work by taking the nested JSON results and flattening them into hierarchechal data tables that are more easily analyzed.

## Downloading the Matlab Analysis Tool ##

Download the Matlab Analysis Tool files from the [GitLab Repository](https://gitlab.com/creare-com/tabsint/tree/master/tools/matlab), and unzip them into a local folder. You may choose to work directly from this folder by adding result files to this directory, or you may [add this folder to your path (with subfolders)](https://www.mathworks.com/help/matlab/matlab_env/what-is-the-matlab-search-path.html).

## Importing and Parsing Results ##

The Matlab Analysis tool is centered around the TabsintResults class. From within the directory containing the JSON results for analysis, create the TabsintResults object, and load the data.

```Matlab
clear all;
tr = TabsintResults;
tr.loadresults(); % this will load all results indexed by exam

% create list of desired response fields
props = {'subjectId', 'examType', 'Ear', 'ResultType', ...
         'Threshold', 'Units', 'F', 'L', 'RetSPL', ...
         'FalsePositive','examProperties'};

tr.loadresponses(props); % this will load all responses from props for all exams
```
Audiometry results can be loaded directly as follows:

```Matlab
output = tr.loadaudiometry()
```

This will create the struct `output` which will contain just Hughson-Westlake results. Fixed level frequency threshold (FLFT)results can be loaded in a similar way.

```Matlab
output = tr.loadauidometry() % load all flft tests
% output = tr.loadflft('BekesyFrequency');    % load only BekesyFrequency tests (original)
% output = tr.loadflft('BHAFT');              % load only BHAFT tests (adaptive)
```

Other result types can be investigated from the `tr.responses` structure using either the appropriate `presentationId` or `examType` keys.