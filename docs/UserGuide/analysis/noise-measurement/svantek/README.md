# Svantek Dosimeter #

The Svantek Dosimeter SV104A has been integrated into TabsINT for recording background noise during TabsINT exams. Details of the implementation and basic usage can be found [here](../../../dosimeter/README.md).

## Result Fields ##


| Result Field        | Data Type | Description                                                                                    |  Units |
|---------------------|-----------|------------------------------------------------------------------------------------------------|------- |
| time                | String    | Recording start time.                                                                          |        |
| status              | Integer   | Status flag indicating 1/3 or 1/1 Octave band data.                                            |        |
| Leq                 | Array     | Mean level of each frequencies in the "Frequency" array over entire recording.                 |  dB SPL|
| Frequencies         | Array     | Band center frequencies of the reported spectral bands.                                        |  Hz    |
| LeqA                | Float     | A-weighted mean of Leq.                                                                        |  dB SPL|
| LeqC                | Float     | C-weighted mean of Leq.                                                                        |  dB SPL|
| LeqZ                | Float     | Mean of Leq using no spectral weighting.                                                       |  dB SPL|
| overallAmbientNoise | Float     | Same as LeqA                                                                                   |  dB SPL|
| FBand               | Integer   | Center frequency of concurrent Hughson Westlake Exam.                                          |  Hz    |
| bandLevel           | Float     | Mean level of FBand frequency over recording duration.                                         |  dB SPL|
