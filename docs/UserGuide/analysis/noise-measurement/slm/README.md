# Sensimetric Sound Level Meter #

A software plugin to measure sound pressure level and spectra from the Nexus 7 tablet has been integrated into TabSINT to record background noise data.

## Result Fields ##



| Result Field         | Data Type | Description                                                                                    |  Units |
|----------------------|-----------|------------------------------------------------------------------------------------------------|------- |
| recordingStartTime   | String    | Recording start time.                                                                          |        |
| recordingDuration    | String    | Recording duration.                                                                            | Seconds|
| numberOfReports      | String    | Number of reported time points, equal to the number of reported SPL values and number of rows in reported spectra.|      |
| timePoints           | String    | List of time points.                                                                           | Seconds|
| SPL_A_mean           | String    | Time series of A-weighted mean sound pressure level.                                           |  dB SPL|
| meanSpectrum         | String    | A 22-band spectrum (100-12500 Hz) of mean level for each time interval.                        |  dB SPL|
| peakSpectrum         | String    | A 22-band spectrum (100-12500 Hz) of peak level for each time interval.                        |  dB SPL|
| SPL_A_slow           | String    | Returns output of an exponential filter with a 1000-ms time constant at the end of the reporting interval using the A-weighted SPL.| dB SPL|
| bandCenterFrequencies| String    | List of the center frequencies of the reported spectral bands.                                 |  Hz    |
| Frequencies          | Array     | List of center frequencies as an array.                                                        |  Hz    |
| Leq                  | Array     | Mean level of each frequencies in the "Frequencies" array over entire recording.               |  dB SPL|
| LeqA                 | Float     | A-weighted mean of Leq.                                                                        |  dB SPL|
| overallAmbientNoise  | Float     | Same as LeqA.                                                                                  |  dB SPL|
| FBand                | Integer   | Center frequency of concurrent Hughson Westlake Exam.                                          |  Hz    |
| bandLevel            | Float     | Mean level of FBand frequency over recording duration.                                         |  dB SPL|