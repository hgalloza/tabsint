# Advanced Protocols #

Protocols can support many advanced features, including custom navigation menus, dynamically calculated page properties, and complex logic flow.

These sections document the more advanced protocol features in TabSINT.
