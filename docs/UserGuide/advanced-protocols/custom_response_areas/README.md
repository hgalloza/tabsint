# Custom Response Areas #

Advanced users can include custom response areas that extend the standard TabSINT functionality.

These pages can be used for an additional type of response area or to analyze and display results. Below is the list of requirements and an example use case.

> **Warning:** These response areas are difficult to write and debug properly. 
> Creare can develop custom response areas, which can then be extended or  ustomized. 
> This functionality is exposed for *advanced users only*.
> Effective protocol development requires, at a minimum, familiarity with:
> 
> - Angularjs
> - TabSINT
> - The [Underscore Javascript Library](http://underscorejs.org)
> - Bootstrap and AngularUI

## Requirements ##

- The javascript file and html files explained below must be included in the zip file with the protocol.
- Javascript File(s):
	- The javascript file(s) must be defined as a string or an array in the field "js", at the top level of the protocol
	- The module must be wrapped in a ```(function() { // content })()``` statement
	- The module can contain one or more angular controllers used in the htmls files attached to custom response areas. These controllers are registered using the `tabsint.controller` service, and must be referenced in the custom html files using ng-controller="controllerName"
	- Note the available variables and methods attached to $scope, commented below.  Only variables on $scope (i.e. $scope.score) are available in the html of custom response areas!
	- Sample javascript file ```customPageControllers.js```

```
(function() {

  tabsint.controller('SummaryResultsCtrl', function ($scope) {

    // Read-Only $scope variables:
    //  $scope.responses :
    //      description : results
    //      fields: response, correct, eachCorrect, numberCorrect, numberIncorrect
    //  $scope.tabletLocation :
    //      description : gps coords
    //      fields : latitude, longitude
    //  $scope.site :
    //      description : tablet info
    //      fields : siteName, siteId, protocolId, protocolHash, protocolName, protocolCreationDate, protocolOwner

    // Read/Write $scope variables:
    //  $scope.page : 
    //      description : points you to examLogic.dm.page;
    //      you can edit properties of the page here, including the presented title, question test, submittable logic, etc
    //  $scope.flags :
    //      description : points to examLogic.dm.state.flags;
    //      you can set or check these, but be careful - Changing flags mid-exam may lead to unpredicted behavior!

    // custom functionality (example)
    $scope.score = ($scope.nCorrect - $scope.nIncorrect)/$scope.nResponses;
  });

  tabsint.controller('CustomPageCtrl', function ($scope) {

    // Another controller, tied to a different custom page and html, could go here.
    $scope.displayText = 'This is a Custom Page';

  });

})();
```

- HTML Files:
	- One HTML file is required for each custom page type. Start the HTML by assigning a custom controller from the custom javascript file.
	- Sample HTML files `summaryResults.html` and `customPage.html`

{% raw %}
```
	<div ng-controller="SummaryResultsCtrl">
		<row>Protocol Name : {{site.protocolName}}</row>
		<row>Score : {{score}}</row>
	</div>
```
{% endraw %}

{% raw %}
```
	<div ng-controller="CustomPageCtrl">
        	<row>Protocol Name : {{site.protocolName}}</row>
        	<row>{{displayText}}</row>
	</div>
```
{% endraw %}

- Protocol Block:
	- The syntax for adding multiple ```customResponseArea``` pages in the ```protocol.json``` is shown below.

```
 	{
            "js": "customPageControllers.js", // the same javascript file for ALL custom page controllers
            "pages":[
                ...,
                {
                    "id":"summaryResults1",
                    "title":"Summary Results",
                    "questionMainText":"The Results of the Last Five Questions",
                    "responseArea": {
                        "type": "customResponseArea",
                        "html": "summaryResults.html"    // the html file specific to this custom page type
                    },
                    "submitText": "Custom Submit Button Text"
                },
                {
                    "id":"customPage1",
                    "title":"Custom Page",
                    "questionMainText":"This is a Second Custom Page",
                    "responseArea": {
                        "type": "customResponseArea",
                        "html": "customPage.html"    // the html file specific to this custom page type
                    },
                    "submitText": "Custom Submit Button Text"
                },
                ...
             ]
         }
```

## Additional Functionality - Save To File ##

TabSINT custom response area functionality can be expanded to include the ability to save intermediate data to a file.

### Implementation ###

1. Dependency-inject the library `$cordovaFile` into your `customResponseArea` controller. Notice the addition of file after $scope in the controller function arguments below. See [ngCordova](http://ngcordova.com/docs/plugins/file) for more information.
2. Call ```$cordovaFile.writeFile(cordova.file.externalRootDirectory, fileName, textToWriteToFile)```. 

### Example of Saving Data to File ###

```
(function() {

  tabsint.controller('SummaryResultsCtrl', function ($scope, $cordovaFile) {
      var score = ($scope.nCorrect - $scope.nIncorrect)/$scope.nResponses;

      var path = 'testData',
      fileName = 'summaryResultsSection1.txt',
      text = 'The results from section 1 were: '+score;

      $cordovaFile.writeFile(cordova.file.externalRootDirectory, fileName, text);

  });

})();
```

> **Note:** While the file will write immediately, the filesystem may not update to show the new file from a computer until you restart the tablet.  
> The file will show up immediately on the tablet and using more advanced file browsers on the computer side.
