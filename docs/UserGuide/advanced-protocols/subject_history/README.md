# Using Subject history #

Tabsint can keep track of individual subjects taking an exam on a site. A subject's results from a previous exam can be used to inform the content of that subject's exam at a later date.

Utilizing subject history requires three individual parts:

- A Subject ID Response Area to attach a subject id to exam results

- A server side exam results processing function

- Protocol-level logic to use the subject history
  
## Subject ID Response Area ##

The subject ID response area will attach a unique subject id to exam results for a specific site.   

The subject ID response area is defined in the json schema as:

```
"subjectIdResponseArea":{
        "description":"A response area to record a subject id in the exam results",
        "properties":{
                "type":{
                        "enum":[
                                "subjectIdResponseArea"
                        ]
                },
                "skip": {
                        "type": "boolean",
                        "description": "Allow user to skip entering the subject id"
                }
        }
}
```

## Using Subject History in a protocol ##

If subject history information is available for a site, it will be put on the flags object of any protocol running on that site. See [Implementing Logic](../implementing_logic/) for details on how to access these objects and use them in a protocol.

An example protocol using subject history is shown in [Subject History Example](../example_protocols/subject_history/)
