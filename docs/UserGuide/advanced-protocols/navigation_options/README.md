# Navigation Options #

Protocol flow can be further customized using navigation menus and back buttons.

## Back Button ##

Set the field ``enableBackButton`` to ``true`` at the page level to replace the **Help** button in the bottom left of the page with a **Back** button. This option can also be set globally by setting ``enableBackButton`` to ``true`` at the protocol level.

Note that the back button currently only allows going back *within* a single subprotocol. 
If the user is currently on the first page of a subprotocol, they cannot go back, and the button will be disabled. 
The back button will also be disabled inside ``followOns`` and after a skipped question.

## Navigation Menu ##

The navigation drop-down menu in the top right of the tablet can be customized during exams using the ``navMenu`` object in a protocol.
```
"navMenu":[
  {
    "text": "Back to Main Menu",
    "target": {
      "reference": "MainMenu"
    },
    "returnHereAfterward": false
  }
]
```
All three fields of the ``navMenu`` object are required. 

The link text and target are set using ``text`` and ``target``, respectively.
The field ``returnHereAfterward`` allows the link to behave in two different ways:

- ``false``:  Replace all currently queued pages with the target, finish exam when target is complete.
- ``true``:  Add the linked section to the current protocol stack.  The page displayed when the link is pressed will be shown after the target is complete.

