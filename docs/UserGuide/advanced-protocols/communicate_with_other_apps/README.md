# Communication with other apps #

For additional feature flexibility, TabSINT protocol pages can interface with other apps installed on the tablet or smart phone.

In an externalApp responseArea, TabSINT switches focus to another specified app, optionally sending data as well.  Then TabSINT waits for the other app to return focus to TabSINT.

The other app can perform any desired function, then pass focus and data back to TabSINT.  That data is stored as the response for the externalApp page, and the protocol continues.

## External App Requirements

For the externalApp feature to work properly, the other app must be able to properly interface with TabSINT.

See [tabsint-crosstalk](https://gitlab.com/creare-com/tabsint-crosstalk) app for a workng example.

The example app depends on the [TabSINT Receiver plugin](https://gitlab.com/creare-com/cordova-plugin-creare-tabsintreceiver).

## Options ##

* `appName` - must match the package name of another installed
* `data` - object that can be passed to external app on initialization

## Protocol Example ##
```
{
  "id": "externalApp1",
  "title": "External App",
  "questionMainText":"This page will launch another app to perform tasks external to TabSINT.",
  "questionSubText":"When the external app is done performing these tasks, focus will return to TabSINT.",
  "responseArea":{
    "type":"externalAppResponseArea",
    "appName": "com.creare.crosstalk",
    "dataOut": {
      "message": "Run-Exam-1",
      "data": 10
    }
  }
}
```
