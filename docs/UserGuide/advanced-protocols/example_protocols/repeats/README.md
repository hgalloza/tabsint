# Repeats Example #

## Description ##

See [Repeats](../../implementing_logic/#repeats) for detailed description of feature.

## Protocol ##

```json
{
  "title":"A Simple Exam With Repeated Question",
  "pages":[
    {
      "id":"repeat01",
      "title":"Multiple Choice 1 With Repeats",
      "instructionText":"The question will repeat (up to 2 repeats) if you choose A.",
      "responseArea":{
        "type":"multipleChoiceResponseArea",
        "choices":[
          {
            "id":"A",
            "text":"Choice A"
          },
          {
            "id":"B",
            "text":"Choice B"
          }
        ]
      },
      "repeatPage":{
        "nRepeats":2,
        "repeatIf":"result.response !== 'B'"
      }
    }
  ]
}
```
