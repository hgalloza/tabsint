# Feedback Example #

## Description ##

See [Feedback](../../implementing_logic/#feedback) for detailed description of feature.

## Protocol ##

```json
{
  "title":"A Simple OMT Exam With Feedback",
  "pages":[
    {
      "id":"omt001",
      "title":"OMT",
      "wavfiles":[
        {
          "path":"Ta_ll3cm.wav",
          "targetSPL":"65.0"
        }
      ],
      "responseArea":{
        "type":"omtResponseArea",
        "correct":"Lucy likes three cheap mugs.",
        "feedback":"gradeResponse"
      }
    }
  ]
}
```
