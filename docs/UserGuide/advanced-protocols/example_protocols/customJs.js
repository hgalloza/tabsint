(function() {

  tabsint.controller('SummaryResultsCtrl', function ($scope) {

    // Read-Only $scope variables:
    //  $scope.responses :
    //      description : results
    //      fields: response, correct, eachCorrect, numberCorrect, numberIncorrect
    //  $scope.tabletLocation :
    //      description : gps coords
    //      fields : latitude, longitude
    //  $scope.site :
    //      description : tablet info
    //      fields : siteName, siteId, protocolId, protocolHash, protocolName, protocolCreationDate, protocolOwner

    // Read/Write $scope variables:
    //  $scope.page : 
    //      description : points you to examLogic.dm.page;
    //      you can edit properties of the page here, including the presented title, question test, submittable logic, etc
    //  $scope.flags :
    //      description : points to examLogic.dm.state.flags;
    //      you can set or check these, but be careful - Changing flags mid-exam may lead to unpredicted behavior!

    // custom functionality (example)
    $scope.score = ($scope.nCorrect - $scope.nIncorrect)/$scope.nResponses;
  });

  tabsint.controller('CustomPageCtrl', function ($scope) {

    // Another controller, tied to a different custom page and html, could go here.
    $scope.displayText = 'This is a Custom Page';

  });

})();