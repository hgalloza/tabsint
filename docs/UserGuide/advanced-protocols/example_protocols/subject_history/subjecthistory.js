(function() {

  tabsint.register('retrieveHAF', function(api) {

    var Freq;
    var history = api.flags.subjectHistory;
    var subject = api.examResults.subjectId;

    // Try to retrieve HAF for either left of right ear
    try {
      Freq = retrieveF();
    } catch (e) {
      console.log('WARNING: Failed to retrieve HAF data from history. Error: ' + angular.toJson(e));
      Freq = undefined;   // This will throw an error in the exam to alert us
    }

    // Find Frequency in subject history or from Input Response Area
    function retrieveF() {
      var F;

      // if there is no history entry for this subject, try get input from previous integer response area
      if (_.isUndefined(history[subject])) {

        // look through all previous responses, find the one with the presentationId = 'inputF'
        _.each(api.examResults.testResults.responses, function(response) {
          if (response.presentationId === 'inputF'){
            F = parseInt(response.response);   // make an integer out the response
          }
        });
      }

      // otherwise, get F from subject history
      else {
        F = _.last(history[subject]).F
      }

      return F;
    }

    return {
      responseArea: {
        examProperties: {
          F: Freq
        }
      }
    };
  });


})();