# FAQs

----------

1. [How do I make sure the WAHTS is charging?](#how-do-i-make-sure-the-wahts-is-charging)
2. [How do I reboot the WAHTS?](#how-do-i-reboot-the-WAHTS)
3. [The connection is dropping between the WAHTS and TabSINT. What should I do?](#the-connection-is-dropping-between-the-wahts-and-tabsint-what-should-I-do)
4. [I keep seeing the word "CHA", what is that?](#i-keep-seeing-the-word-cha-what-is-that)
5. [How do I retrieve the log file on the tablet?](#how-do-i-retrieve-tablet-logs)
6. [How do I find my Tablet or Device UUID?](#how-do-i-find-my-tablet-or-device-uuid)
7. [TabSINT shows my headset is connected, but I receive an error notice that it is not connected when I try to run an exam. How do I fix this?](#how-do-i-know-the-headset-is-connected-to-tabsint)
8. [How do I update TabSINT to the latest version?](#how-do-i-update-tabsint)
9. [I received an alert that the firmware in the WAHTS doesn't match the tabSINT release. Do I have to update it? How do I do this?](#how-do-i-update-the-wahts-firmware)


### How do I make sure the WAHTS is charging?

Make sure the WAHTS is turned *On* while charging, by sliding the switch in the red ear cup to the right. 

<div style="text-align:center"><img src="switch.png" width="60%"/></div>

At this time, there is no light or LED to indicate the WAHTS is charging or to indicate when charging is complete. The amount of time required to charge depends on the current battery level, but charging for two to three hours should be plenty to fully charge the WAHTS. Once charged, battery levels can be checked by connecting to CHAMI or [TabSINT](../QuickStart/WAHTS#connecting-wahts-to-tabsint).
  
### How do I reboot the WAHTS?

The headset can be rebooted two different ways:

1. Power Cycle the headset. This means to simply turn the headset "Off" then back "On" using the [switch](#how-do-i-make-sure-the-wahts-is-charging) in the right ear cup.
2. Press "Reboot" in the TabSINT program. Older versions of TabSINT have a "Reboot" button. When updating firmware, the headset should reboot automatically. If the headset does not reboot after firmware update, and/or the TabSINT version does not have a reboot button option, simply Power Cycle the headset.


  
### The connection is dropping between the WAHTS and TabSINT. What should I do?

1. Make sure the headset is fully [charged](../QuickStart/WAHTS#charging-the-wahts). You can check battery levels by [connecting the WAHTS to TabSINT](../QuickStart/wahts#connecting-wahts-to-tabsint). Make sure the headset is not plugged into a wall outlet for an accurate review of the headsets battery levels.
2. [Power Cycle](#how-do-I-reboot-the-wahts) the headset, by turning it "Off" and then back "On". 

### I keep seeing the word "CHA", what is that?

In documentation and in TabSINT you may come across the word *CHA*. *CHA* is the nomenclature for the internal electronics inside the WAHTS and inside some other hearing assessment devices. For the sake of ease, consider the two terms interchangeable.

### How do I retrieve tablet logs?

1. Go to "Admin View" in TabSINT
2. Scroll down to the section "Application Log" and select "Export" <div style="text-align:center"><img src="logs.png" width="60%"/></div>

3. The logs are saved on directory My files ==> Device Storage==> .tabsint-logs

<div style="text-align:center"><img src="devicestorage.png" width="60%"/></div>

<h3>Note:</h3>
----------------------------------------------------
>**If .tabsint-logs is not visible in "Device Storage" selcect "More" at the upper right corner to activate a *drop down menu*, then select "Show Hidden Files"**

<div style="text-align:center"><img src="more.png" width="60%"/></div>

### How do I find my Tablet or Device UUID?

The Device UUID can be seen in two places. On the bottom of the home screen of TabSINT, the last 5 digits of the UUID is displayed. For the full UUID, go to "Admin View" and scroll down to "Software and Hardware". The full ID is displayed under "Device UUID"

<div style="text-align:center"><img src="uuid.png" width="60%"/></div>

### How do I know the headset is connected to TabSINT?

There are three primary indicators the headset is connected:

<div style="text-align:center"><img src="connection.png" width="60%"/></div>

1. The *Headset with Bluetooth* icon will appear in the upper bar.
2. The WAHTS Panel will show the headset as connected, identifying by serial number which headset is connected.
3. The WAHTS Panel will show additional identifying information for the headset such as *Serial Number*, *Firmware*, and *Battery Level*. 

The headset should stay connected throughout the duration of any examination. There are a few reasons the headset might become disconnected, though still showing as connected:

1. The headset has been idle for greater than 15 minutes and has entered [Sleep Mode](../QuickStart/WAHTS/#sleep-and-wake). Due to the idle state of the WAHTS and thus lack of communication with TabSINT, the headset may show as still connected when in fact it is not. Attempting to run an exam will result in an error message. 
2. The headset has been turned off. If the [Power Switch](#how-do-i-make-sure-the-wahts-is-charging) in the red ear cup has been moved to the left, the headset has been turned off. If it is turned off while connected to TabSINT, TabSINT may show the headset as still connected when it is not. Turn the [Power Switch](#how-do-i-make-sure-the-wahts-is-charging) back on by moving the switch to the right. Then [Shake to Wake](../QuickStart/WAHTS/#to-wake-the-wahts-from-idle) the headset until blue lights are flashing in the left ear cup. 
3. The headset needs to be charged. If the battery level falls below an acceptable range, the headset will no longer be able to communicate with TabSINT. If the above two methods do not remedy the problem, the headset may need to be [Charged](#how-do-i-make-sure-the-wahts-is-charging).

Once it is known the headset is fully charged and *On* with blue lights flashing, reconnect to TabSINT as directed in the [QuickStart-WAHTS](../QuickStart/WAHTS/#connecting-wahts-to-tabsint) guide of this documentation. 

If the headset is not connecting after any of the above procedures and has not yet been *Power Cycled* (turned off and back on) then try *Power Cycling* the headset before re-connecting to TabSINT. 

### How do I update TabSINT?

1. Under *Admin View*, go to the *Setup* tab. Scroll down and expand the **Software and Hardware** field. Scroll down and select **View Releases**

	<div style="text-align:center"><img src="view-releases.png" width="60%"/></div>

	This will open the browser on the tablet to [http://tabsint.org/releases/](http://tabsint.org/releases/). 

2. Select and download the latest **Stable** release available. This will download the tabsint.apk file to your tablet


	<div style="text-align:center"><img src="tabsint-apk.png" width="60%"/></div>

3.  On a tablet you should be able to swipe down in the upper left corner, (you will see the arrows indicating it's downloading). Once downloaded open the tabsint.apk file to install. 


	> NOTE: If tabSINT has been downloaded prior, and you look for the tabsint.apk file in the *Device Storage* of the tablet, there may be multiple tabsint.apk files to choose from. If this occurs and is confusing, deleted all the tabsint.apk files, then download the file with the release you want again. 

4. Confirm "Install," it will show "Installing ..." and then when complete click "Open"

	<div style="text-align:center"><img src="install.png" width="60%"/></div>

5. The bottom of the welcome screen should show the version of tabSINT you just installed. 

	<div style="text-align:center"><img src="version.png" width="60%"/></div>

### How do I update the WAHTS firmware?

Each version of tabSINT requires a specific firmware. If the headset connected to tabSINT does not have the required firmware release, you will see an **Alert**. Although you can run tabSINT with non-compliant firmware, you may get unexpected results or experience additional error or warning messages. Always update the WAHTS firmware to match the version compatible with the tabSINT release. 

For complete details on how to update firmware visit the [WAHTS Quick Start](../QuickStart/WAHTS#updating-headset-firmwaer) section of this documentation. 