
define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.cha.indicator', [])
    .directive('chaIndicator', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/cha-indicator/cha-indicator.html',
        controller: 'ChaIndicatorCtrl',
        scope: {}
      };
    })
    .controller('ChaIndicatorCtrl', function ($scope, cha) {
      var self = this;
      self.cha = cha;
      $scope.cha = cha;
      $scope.chaInfoURL = 'scripts/components/cha-info/cha-info.template.html';
    });
});
