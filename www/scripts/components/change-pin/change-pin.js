/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.change-pin', [])

    .factory('changePin', function ($uibModal, logger, disk, notifications) {
      var api = {
        editAdminPin: undefined
      };

      /**
       * Check that user is authorized using a modal dialog, and if so, call targetFn.
       * @param  {function} targetFn - callback function upon success
       * @param  {boolean} skipAuthorize - override the authorization (i.e. admin mode)
       */
      api.editAdminPin = function() {

        var ModalInstanceCtrl = function ($scope, $uibModalInstance) {
          $scope.save = function (pin) {
            $uibModalInstance.close(pin);
          };
          $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
          };
        };

        var modalInstance = $uibModal.open({
          templateUrl: 'scripts/components/change-pin/change-pin.html',
          controller: ModalInstanceCtrl,
          backdrop: 'static'
        });

        modalInstance.result.then(function (pin) {
          if (pin) {
            disk.pin = pin;
            logger.info('User updated admin PIN to: ' + disk.pin);
          } else {
            logger.warn('User tried to update admin PIN, entered pin: ' + pin);
          }
        });
      };

      return api;
    });

});
