define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.svantek.admin', [])

    .directive('svantekAdmin', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/svantek-admin/svantek-admin.html',
        controller: 'SvantekAdminCtrl',
        scope: {}
      };
    })
    .controller('SvantekAdminCtrl', function ($scope, svantek, bluetoothStatus, disk) {
      $scope.svantek = svantek;
      $scope.disk = disk;
      $scope.bluetoothStatus = bluetoothStatus;
    });
});
