/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.header', [])

    .directive('header', function () {
      return {
        templateUrl: 'scripts/components/header/header.html',
        transclude: true,
        scope: {
          isexam: "="
        },
        controller: function($scope, app, disk, authorize, logger, notifications, examLogic, 
                              page, router, tasks, adminLogic, gettextCatalog) {
          
          $scope.router = router;
          $scope.dm = examLogic.dm;
          $scope.page = page;
          $scope.tasks = tasks;
          $scope.disk = disk;
          $scope.app = app;

          $scope.resetExam = function () {
            notifications.confirm(
              gettextCatalog.getString('Reset Exam and Discard Partial Results?'),
              function (buttonIndex){
                if (buttonIndex === 1){
                  authorize.modalAuthorize(function () {
                    examLogic.reset();
                    disk.currentResults = undefined; // can reset this now - we have generated a proper result
                  }, disk.debugMode);
                } else if (buttonIndex === 2){
                  logger.info('Reset Canceled');
                }
              },
              gettextCatalog.getString('Confirm Selection'),
              [gettextCatalog.getString('OK'), gettextCatalog.getString('Cancel')]
            );

          };

          $scope.submitPartialExam = function () {
            notifications.confirm(
                gettextCatalog.getString('End Exam and Submit Partial Results?'),
              function (buttonIndex){
                if (buttonIndex === 1){
                  authorize.modalAuthorize(function () {
                    examLogic.submitPartial();
                  }, disk.debugMode);
                } else if (buttonIndex === 2){
                  logger.info('submitPartial Canceled');
                }
              },
                gettextCatalog.getString('Confirm Selection'),
              [gettextCatalog.getString('OK'), gettextCatalog.getString('Cancel')]
            );
          };

          $scope.navigateToTarget = function (navOption) {
            var confirmText = navOption.text+'?  ';
            if (!!navOption.returnHereAfterward){
              confirmText += gettextCatalog.getString('TabSINT will navigate to the selected sub-protocol, then return to this page and resume the current series of questions after that sub-protocol is complete.');
            } else {
              confirmText += gettextCatalog.getString('Results from this page will be lost and the current series of questions will be aborted.');
            }

            notifications.confirm(
              confirmText,
              function (buttonIndex){
                if (buttonIndex === 1){
                  authorize.modalAuthorize(function () {
                    examLogic.navigateToTarget(navOption);
                  }, disk.debugMode);
                } else if (buttonIndex === 2){
                  logger.info('navigateToTarget Canceled');
                }
              },
                gettextCatalog.getString('Confirm Selection'),
              [gettextCatalog.getString('OK'), gettextCatalog.getString('Cancel')]
            );
          };

          $scope.switchToExamView = function() {
            examLogic.switchToExamView();
          };

          $scope.switchToAdminView = function() {
            adminLogic.switchToAdminView();
          };

        }
      };
    });

});
