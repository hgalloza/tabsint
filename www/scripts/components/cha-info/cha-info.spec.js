define(['test-dep', 'app'], function () {
  'use strict';

  describe('cha-info test', function(){

    // Load the module that contains the `chaInfo` component before each test
    beforeEach(module('tabsint.components.cha.info'));

    describe('chaInfo component test', function() {
      //TODO: this should inject mock cha...
      var ctrl;
      var cha = {
        name: 'A cha',
        info: {id: {serialNumber: '232352f2f', description: 'description', buildDateTime: '03/22/2017'},
              vBattery: 3.7,
              batteryIndicatorWidth: 92}
      };

      beforeEach(inject(function($componentController) {
        ctrl = $componentController('chaInfo');
        //TODO: need to connect to cha here, I guess, or inject a mock cha?
      }));

      it('should have cha injected properly', function() {
        expect(true).toBeTruthy();
        expect(cha).toBeDefined();
        // expect(ctrl.cha.info.name).toBeDefined();
        expect(cha.info.id.serialNumber).toBeDefined();
        expect(cha.info.id.description).toEqual('description');
        // expect(ctrl.cha.info.name).toEqual('FAKE CHA');

      });
    });
  });
});
