define(['angular'], function (angular) {
  'use strict';

      angular.module('tabsint.components.cha.info',[]).
        component('chaInfo', {
          templateUrl: 'scripts/components/cha-info/cha-info.template.html',
          controller: ['$scope','cha',function ChaInfoController($scope,cha) {
            var self = this;
            self.cha = cha;
            $scope.cha = cha;
            }
          ]
        });

});
