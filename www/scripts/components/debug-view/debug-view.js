/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.debug-view', [])

    .directive('debugView', function () {
      return {
        templateUrl: 'scripts/components/debug-view/debug-view.html',
        controller: function($scope, disk, examLogic, page, results) {
          $scope.disk = disk;
          $scope.dm = examLogic.dm;
          $scope.page = page;
          $scope.results = results;
          $scope.isCollapsed = true;
        },
        scope: {}
      };
    });

});