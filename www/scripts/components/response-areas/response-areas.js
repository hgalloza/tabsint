/**
 * Created by RXC on 12/5/13.
 */

define(['angular',
  './cha-response-areas',
  './basicResponseAreas',
  './textbox/textbox',
  './omt/omt',
  './multiple-choice/multiple-choice',
  './button-grid/button-grid',
  './crm/crm',
  './custom/custom',
  './checkbox/checkbox',
  './integer/integer',
  './mrt/mrt',
  './likert/likert',
  './seesaw/seesaw',
  './qr/qr',
  './image-map/image-map',
  './tdt/tdt',
  './multiple-input/multiple-input',
  './subject-id/subject-id',
  './audiometry-input/audiometry-input',
  './nato/nato',
  './external-app/external-app',
  './audiometry/audiometry',
  './dpoae/dpoae',
  './hint/hint',
  './third-octave-bands/third-octave-bands',
  './three-digit/three-digit',
  './mld/mld',
  './tone-generation/tone-generation',
  './tat/tat',
  './cha-complex-response-areas/complex-response-areas'
  ], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas', [
    'tabsint.components.response-areas.cha-response-areas',
    'tabsint.components.response-areas.basic-response-areas',
    'tabsint.components.response-areas.textbox',
    'tabsint.components.response-areas.omt',
    'tabsint.components.response-areas.multiple-choice',
    'tabsint.components.response-areas.button-grid',
    'tabsint.components.response-areas.custom',
    'tabsint.components.response-areas.checkbox',
    'tabsint.components.response-areas.integer',
    'tabsint.components.response-areas.mrt',
    'tabsint.components.response-areas.likert',
    'tabsint.components.response-areas.seesaw',
    'tabsint.components.response-areas.qr',
    'tabsint.components.response-areas.image-map',
    'tabsint.components.response-areas.tdt',
    'tabsint.components.response-areas.multiple-input',
    'tabsint.components.response-areas.subject-id',
    'tabsint.components.response-areas.audiometry-input',
    'tabsint.components.response-areas.nato',
    'tabsint.components.response-areas.external-app',
    'tabsint.components.response-areas.cha.audiometry',
    'tabsint.components.response-areas.cha.crm',
    'tabsint.components.response-areas.cha.dpoae',
    'tabsint.components.response-areas.cha.hint',
    'tabsint.components.response-areas.cha.third-octave-bands',
    'tabsint.components.response-areas.cha.three-digit',
    'tabsint.components.response-areas.cha.mld',
    'tabsint.components.response-areas.cha.tone-generation',
    'tabsint.components.response-areas.cha.tat',
    'tabsint.components.response-areas.cha.complex-response-areas'
  ])

    .factory('responseAreas', function () {
      var api = {
        list: undefined,
        native: {},
        plugins: {}
      };

      api.native = {
        textboxResponseArea: {
          templateUrl: 'scripts/components/response-areas/textbox/textbox.html'
        },
        omtResponseArea: {
          templateUrl: 'scripts/components/response-areas/omt/omt.html'
        },
        multipleChoiceSelectionResponseArea: {
          templateUrl: 'scripts/components/response-areas/omt/omt.html'
        },
        multipleChoiceResponseArea: {
          templateUrl: 'scripts/components/response-areas/multiple-choice/multiple-choice.html'
        },
        buttonGridResponseArea: {
          templateUrl: 'scripts/components/response-areas/button-grid/button-grid.html'
        },
        crmResponseArea: {
          templateUrl: 'scripts/components/response-areas/crm/crm.html'
        },
        checkboxResponseArea: {
          templateUrl: 'scripts/components/response-areas/checkbox/checkbox.html'
        },
        integerResponseArea: {
          templateUrl: 'scripts/components/response-areas/integer/integer.html'
        },
        mrtResponseArea: {
          templateUrl: 'scripts/components/response-areas/mrt/mrt.html'
        },
        likertResponseArea: {
          templateUrl: 'scripts/components/response-areas/likert/likert.html'
        },
        seeSawResponseArea: {
          templateUrl: 'scripts/components/response-areas/seesaw/seesaw.html'
        },
        qrResponseArea: { // may want to test qr code on a tablet
          templateUrl: 'scripts/components/response-areas/qr/qr.html'
        },
        imageMapResponseArea: {
          templateUrl: 'scripts/components/response-areas/image-map/image-map.html'
        },
        threeDigitTestResponseArea: { // need to find protocol to test this
          templateUrl: 'scripts/components/response-areas/tdt/tdt.html'
        },
        multipleInputResponseArea: {
          templateUrl: 'scripts/components/response-areas/multiple-input/multiple-input.html'
        },
        customResponseArea: { // where's the controller? (make a folder with only view)
          templateUrl: 'scripts/components/response-areas/custom/custom.html'
        },
        subjectIdResponseArea: { // What to do with directive? (delete directive, copy controller)
          templateUrl: 'scripts/components/response-areas/subject-id/subject-id.html'
        },
        yesNoResponseArea: { // Just use multiple-choice for this? (just change template to be same as multiple choice)
          templateUrl: 'scripts/components/response-areas/multiple-choice/multiple-choice.html'
        },
        audiometryInputResponseArea: {
          templateUrl: 'scripts/components/response-areas/audiometry-input/audiometry-input.html'
        },
        externalAppResponseArea: {
          templateUrl: 'scripts/components/response-areas/external-app/external-app.html'
        },
        natoResponseArea: {
          templateUrl: 'scripts/components/response-areas/nato/nato.html'
        },
        chaBekesyLike: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaBekesyFrequency: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaBHAFT: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaCRM: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaThreeDigit: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaMLD: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaHughsonWestlake: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaHughsonWestlakeFrequency: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaThirdOctaveBands: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaToneGeneration: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaDPOAE: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaHINT: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaTAT: {
          templateUrl: 'scripts/components/response-areas/cha-response-areas.html'
        },
        chaManualAudiometry: {
          templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html'
        },
        chaPlaysoundArray: {
          templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html'
        },
        chaManualToneGeneration: {
          templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html'
        },
        chaAudiometryList: {
          templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html'
        },
        chaSoundRecognition: {
          templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/complex-response-areas.html'
        },
        chaAudiometryResultsPlot: {
          templateUrl:'scripts/components/response-areas/audiometry/audiogram/audiogram.html'
        },
        chaAudiometryResultsTable: {
          templateUrl:'scripts/components/response-areas/audiometry/audiometry-table/audiometry-table.html'
        }
      };


      api.all = function() {
        return _.extend(api.plugins, api.native);
      };

      return api;
    })

    .directive('responseArea', function() {
      return{
        restrict: 'E',
        scope:{},
      };
    })


    .controller('ResponseAreaCtrl', function($scope, responseAreas) {
      this.responseAreas = responseAreas.all();
      //this.exists = function(type) {
      //  return _.includes(_.keys(responseAreas.all()), type);
      //};

    });


});
