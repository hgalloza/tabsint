define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.seesaw', [])

    .controller('SeeSawResponseAreaCtrl', function ($scope, page) {
      $scope.$watch('page.result.response', function () {
          if (!page.dm.responseArea.responseRequired || (page.result.response !== '')) {
              page.dm.isSubmittable = true;
          } else {
              page.dm.isSubmittable = false;
          }
      });

      page.result.response = '';
      $scope.page = page;

      $scope.chosen = function (side) {
        if (side === 'L') {
          return ($scope.i === page.result.response);
        } else if (side === 'R') {
          return ((4-$scope.i) === page.result.response);
        }
      };

      $scope.btnSrc = function (side) {
        var btnSrc;
        if ($scope.chosen(side)) {
          btnSrc = 'img/radioSeeSaw_selected_24_24.png';
        } else {
          btnSrc = 'img/radioSeeSaw_unselected_24_24.png';
        }
        return btnSrc;
      };

      $scope.choose = function (side) {
        if (side === 'L') {
          page.result.response = $scope.i;
        } else if (side === 'R') {
          page.result.response = (4 - $scope.i);
        }
      };
    });
});