
define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.textbox', [])

  .controller('TextboxResponseAreaCtrl', function ($scope, page, examLogic) {
    $scope.page = page;

    // prevent the user from pressing enter if the number of rows i
    $('textarea').keydown(function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
      }
    });

    $scope.$watch('page.result.response', function () {
        if (page.dm.responseArea.responseRequired === true && (page.result.response === undefined || page.result.response === '')) {
            $scope.page.dm.isSubmittable = false;
        } else {
            $scope.page.dm.isSubmittable = true;
        }
    });

    // Clear textbox when the question ID is changed
    function update() {
      $scope.page.result.response = undefined;

      if (!!(page.dm.responseArea.rows)) {
        $scope.rows = page.dm.responseArea.rows;
      } else {
        $scope.rows = 1;
      }

      if (page.dm.responseArea.submitEmpty) {
        $scope.page.result.response = '';
      }
    }

    update();

    //$scope.$watch('page.dm.id', update);
  });

});