/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.likert', [])

    .controller('LikertResponseAreaCtrl', function ($scope, page) {
      $scope.page = page;

      // defaults
      var labelFontSize = 20;      // px
      $scope.$watch('page.result.response', function () {
          if (page.dm.responseArea.responseRequired === true && (page.result.response === undefined || page.result.response[0] === undefined || page.result.response === '')) {
              $scope.page.dm.isSubmittable = false;
          } else {
              $scope.page.dm.isSubmittable = true;
          }
      });
      // update view when page first loads
      function update () {

        // Note - we treat ALL likert response areas as "multiple likert response areas".
        // If the user specifies a standard single likert, we create an array of length one
        // $scope.questions holds the array of all likert presentations
  		  $scope.questions = [];
  		  page.result.response = []; // create array of responses
        
  		  if (page.dm.responseArea.questions === undefined) {
    			page.dm.responseArea.questions = [];
    			page.dm.responseArea.questions.push(angular.copy(page.dm.responseArea));
  		  }

    	  _.forEach(page.dm.responseArea.questions, function (question, index) {

    			page.result.response.push(undefined);
    			$scope.questions.push({});
    			var nLevels = question.levels;
    			$scope.questions[index].levels = _.map(_.range(nLevels), function(n) {return (n).toString();});
    			$scope.questions[index].topLabels = _.map(_.range(nLevels), function() {return '';});
    			$scope.questions[index].bottomLabels = _.map(_.range(nLevels), function() {return '';});
          $scope.questions[index].text = question.questionMainText;
    			$scope.questions[index].labelFontSize = question.labelFontSize || labelFontSize;

    			if (question.specifiers ) {
    			  _.forEach(question.specifiers, function (specifier) {
      				if (specifier.position ==='below') {
      				  $scope.questions[index].bottomLabels[specifier.level] = specifier.label;
      				} else if ((specifier.position ==='above') || (specifier.position === undefined)) {
      				  $scope.questions[index].topLabels[specifier.level] = specifier.label;
      				}
    			  });
    			}

    			// Can we remove the 'odd' labels to make the layout more spacious?
    			function isOdd(num) { return num % 2; }

    			if (isOdd(nLevels)) {
    			  var allIdxs = _.range(nLevels);
    			  var oddIdxs = _.filter(allIdxs, function(num){ return isOdd(num); });
    			  var evenIdxs = _.filter(allIdxs, function(num){ return !isOdd(num); });

    			  if ( _.every(oddIdxs, function (idx) { return $scope.questions[index].topLabels[idx] === ''; } ) ) {
    				  $scope.questions[index].topLabelsAreSpacious = true;
    				  $scope.questions[index].topLabels = _.map(evenIdxs, function (idx) { return $scope.questions[index].topLabels[idx]; });
    			  }

    			  if ( _.every(oddIdxs, function (idx) { return $scope.questions[index].bottomLabels[idx] === ''; } ) ) {
    				  $scope.questions[index].bottomLabelsAreSpacious = true;
    				  $scope.questions[index].bottomLabels = _.map(evenIdxs, function (idx) { return $scope.questions[index].bottomLabels[idx]; });
    			  }
    			}
    		});
      }

      update();

    })


    .controller('LikertChoiceController', function ($scope, examLogic, page) {
      update();

      function update() {
        $scope.chosen = function () {
          return (($scope.level === page.result.response[$scope.parentIndex]));
        };

        var emoticons;
        if (page.dm.responseArea.useEmoticons) {
          emoticons = [
            {
              src: 'img/emoticons/strongly_disagree.png'
            },
            {
              src: 'img/emoticons/disagree.png'
            },
            {
              src: 'img/emoticons/no_opinion.png'
            },
            {
              src: 'img/emoticons/agree.png'
            },
            {
              src: 'img/emoticons/strongly_agree.png'
            }
          ];
        }

        $scope.btnSrc = function (ind) {
          var btnSrc;
          if (page.dm.responseArea.useEmoticons) {
            btnSrc = emoticons[ind].src;
          } else {
            if ($scope.chosen()) {
              btnSrc = 'img/radio_selected_64_64.png';
            } else {
              btnSrc = 'img/radio_unselected_64_64.png';
            }
          }

          return btnSrc;
        };
        
        // function to run when selection is made
        $scope.choose = function (index) {
          
          // toggle chosen/unchosen.
  		  $scope.currentIndex = index;
          if (page.result.response[index] === $scope.level) {
            page.result.response[index] = undefined; //toggle off.
          } else {
            page.result.response[index] = $scope.level; // choose.
          }
          if (page.result.response.indexOf(undefined) < 0) {
            page.dm.isSubmittable = true;  // submittable when both the responseText and responseNumber are defined
          } else {
            page.dm.isSubmittable = false;
          }
  			  
          // AUTO-SUBMIT:
          if (page.dm.responseArea.autoSubmit){ //to allow auto-submit, HG 07/12/16
            var checkAllResponse = true;		//so autosubmit works with multi-likeRT
            for (var i=0;i<page.result.response.length;i++){
              if (page.result.response[i] === undefined){
                checkAllResponse = false;
                break;
              }
            }
            if (checkAllResponse){
              examLogic.submit();
            }
          }
  
        };
      }
    });

});
