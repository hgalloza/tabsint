define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.checkbox', [])

    .controller('CheckboxResponseAreaCtrl', function ($scope, page, $location, $anchorScroll, examLogic) {
      $scope.page = page;
      page.dm.isSubmittable = examLogic.getSubmittableLogic();

      // For checkbox response areas and omt response areas (to be included in future)
      // page.result.response will be a list converted to a json string. 'responses' is converted back to a list of strings
      examLogic.gradeResponse = function(){
        var responses = angular.fromJson(page.result.response);

        page.result.correct = undefined;
        page.result.eachCorrect = undefined;

        var eachCorrect = [];   // set an empty array to begin - will coorrespond to each 'choice'
        var choices  = page.dm.responseArea.choices;
        var questionContainsCorrect = false; // Flag for if any choice is marked as true.  If not choice is marked as true, result.correct will be undefined

        for (var i = 0; i < choices.length; i++) {
          var choice = choices[i];
          // If we find our choice id, and it's correct, mark it as such.
          if (choice.correct === true || choice.correct === 'true') {
            questionContainsCorrect = true;   // the question has one or mor choices labeled correct
            if (_.includes(responses, choice.id )) {
              eachCorrect[i] = true;
            } else {
              eachCorrect[i] = false;
            }
          } else if (choice.correct === false || choice.correct === 'false'){  // if the choice is not a correct one
            if (_.includes(responses, choice.id )) {  // but the user has selected it
              eachCorrect[i] = false;
            } else {
              eachCorrect[i] = true;
            }
          } else {
            eachCorrect[i] = null;
          }
        }
        if ( questionContainsCorrect ) {
          page.result.eachCorrect = eachCorrect;
          page.result.numberCorrect = 0;
          _.forEach(eachCorrect, function(current) {if (current){page.result.numberCorrect++;}});
          page.result.numberIncorrect = 0;
          _.forEach(eachCorrect, function(current) {if (!current){page.result.numberIncorrect++;}});
          page.result.correct = _.every(eachCorrect);   // if all individual answers correct, mark the whole question correct
        } else {
          page.result.correct = undefined;  // otherwise leave the correct field undefined
          page.result.numberIncorrect = undefined;
          page.result.numberCorrect = undefined;
        }
      };

      $scope.enableOther = false;
      $scope.responseAsList = function () {
        return angular.fromJson(page.result.response);
      };
      $scope.saveListToResponse = function (list) {
        page.result.response = angular.toJson(list);
      };

      function update() {
        $scope.choices = angular.copy(page.dm.responseArea.choices);
        if (page.dm.responseArea.other) {
          $scope.choices.push(
            {
              "id": "Other",
              "text": page.dm.responseArea.other
            });
        }
      }

      update();

      $scope.$watch('page.result.response', function () {

        page.dm.isSubmittable = examLogic.getSubmittableLogic();

        if (page.dm.responseArea.other && ( _.includes($scope.responseAsList(), 'Other'))) {
          $scope.enableOtherText = true;
          $location.hash('otherInput');
          $anchorScroll();
        } else {
          $scope.enableOtherText = false;
          page.result.otherResponse = undefined;
        }
      });
    });
});
