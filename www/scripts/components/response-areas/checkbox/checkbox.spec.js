/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['test-dep', 'app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  var page;

  describe('Response Area: Checkbox', function () {

    var ctrl, scope, page, myChoice, exam;

    beforeEach(inject(function($controller, $rootScope, _page_, _examLogic_) {
      _page_.dm = {
        // isSubmittable: function() {return false},
        responseArea:
        {
          "type": "checkboxResponseArea",
          "buttonScheme": "markIncorrect",
          "choices": [
            {
              "id": "WHITE"
            },
            {
              "id": "SILK"
            },
            {
              "id": "JACKET"
            },
            {
              "id": "ANY"
            },
            {
              "id": "SHOES"
            }
          ],
          "verticalSpacing": 20
        }
      };
      _page_.result = {
        response: undefined,
        other: false
      };

      page = _page_;

      exam = _examLogic_;

      scope = $rootScope.$new();  // start a new scope

      myChoice = page.dm.responseArea.choices[1];

      scope.choice = myChoice;

      ctrl = $controller('CheckboxResponseAreaCtrl', {
        $scope: scope
      });

    }));

    it('should handle submittable logic', function(){

      expect(page.dm.isSubmittable).toBeTruthy();

      page.dm.responseArea.responseRequired = true;

      page.dm.isSubmittable = exam.getSubmittableLogic(page.dm.responseArea);

      expect(page.dm.isSubmittable).toBeFalsy();

      page.result.response = '[]';
      
      page.dm.isSubmittable = exam.getSubmittableLogic(page.dm.responseArea);

      expect(page.dm.isSubmittable).toBeFalsy();

      page.result.response = myChoice;

      page.dm.isSubmittable = exam.getSubmittableLogic(page.dm.responseArea);

      expect(page.dm.isSubmittable).toBeTruthy();
    });

  });

});
