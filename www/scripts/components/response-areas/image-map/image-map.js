/*...*/

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.image-map', [])

    .directive('coordTranslate', function() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          element.on('load', function () {
            //get image width and height, determined by the percentage in protocol
            var w = $(element).width(),
              h = $(element).height();

            var map = attrs.usemap.replace('#', ''),
              c = 'coords';

            angular.element('map[name="' + map + '"]').find('area').each(function () {
              var $this = $(this);

              if (!$this.data(c)) {
                $this.data(c, $this.attr(c));
              }

              var coords = $this.data(c).split(','),
                  coordsPercent = new Array(coords.length),
                  shape = $this.attr('shape');

              // Determine the size in image pixels
              for (var i = 0; i < coordsPercent.length; ++i) {
                if (i % 2 === 0) {
                  coordsPercent[i] = parseInt(Math.round((coords[i] / 100)*w));
                } else {
                  coordsPercent[i] = parseInt(Math.round((coords[i] / 100)*h));
                }
              }
              $this.attr(c, coordsPercent.toString());
            });
          });
        }
      };
    })

    .controller('ImageMapResponseAreaCtrl', function ($scope, examLogic, page, $location, $anchorScroll) {
      $scope.page = page;

      function update () {
        $scope.hotspots = $scope.page.dm.responseArea.hotspots;
        for (var i = 0; i < $scope.hotspots.length; i++) {
          if ($scope.hotspots[i].other) {
            $scope.hotspots[i].id = 'Other';                      // set ID to other to trigger 'Other' response
          }
        }
      }

      $scope.$watch('page.dm.responseArea.hotspots', update());
      update();


      // Function when a hotspot is chosen
      $scope.choose = function (id) {
        // AUTO-SUBMIT:
        page.result.response = id;
        if (page.result.response !== 'Other') {
          examLogic.submit();
        }
      };

      $scope.$watch('page.result.response', function () {
        if ( page.result.response === 'Other' ) {
          $scope.enableOtherText = true;
         // $location.hash('otherInput');
          $anchorScroll();
        } else {
          $scope.enableOtherText = false;
          $scope.page.result.otherResponse = undefined;
        }
      });


    });

});