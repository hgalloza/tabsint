/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.cha.three-digit', [])
    .directive('threeDigitExam', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/three-digit/three-digit.html',
        controller: 'ThreeDigitExamCtrl'
      };
    })
    .controller('ThreeDigitExamCtrl', function ($q, $scope, $timeout, cha, chaExams, examLogic, logger, notifications, page, results) {

      // Function to run at exam initiation - happens when the responseArea changes
      function newThreeDigitExam() {
        $scope.digitsDisabled = true;
        $scope.readyToProcess = false;      // activated when the presentation info is set in "presentationHandler"
        $scope.presentationCount = 0;         // counted number of presentation displayed
        if (_.isUndefined(chaExams.examProperties.nPresentations)) {chaExams.examProperties.nPresentations = 50;}
        if (_.isUndefined(chaExams.examProperties.maxSNR)) {chaExams.examProperties.maxSNR = 25;}

        page.dm.isSubmittable = false;   // make the page not submittable

        resetResults();
        getPresentationInfo();
      }

      function getPresentationInfo() {
        return cha.requestResults()
          .then(function (result) {presentationHandler(result);})
          .catch(function(err) {cha.errorHandler.main(err);});
      }

      // This is less a results handler in this case, more an exam handler
      function presentationHandler(resultFromCha){
        if (resultFromCha.State === 0 || resultFromCha.State === 1) { // State 0 = exam still running, // State 1 =  waiting for result    
          
          // below handle a bug that should get handled in the firmware, but it isn't currently.
          if (resultFromCha.currentSNR > chaExams.examProperties.maxSNR) {
            logger.error('Current SNR > MaxSNR on 3D test');
            notifications.alert('The SNR of the current presentation is greater than the max SNR set in the protcol. Please hand the device to an administrator.');

            page.dm.isSubmittable = true;   // make the page submittable

            page.result = $.extend({}, results.default(page.dm), page.result, {
              response: 'Exam Results',
              digitScore: resultFromCha.digitScore,
              presentationScore: resultFromCha.presentationScore,
              chaInfo: chaExams.getChaInfo()
            });

            examLogic.submit();
          } else {
            // Put presentation properties in the result
            page.result = $.extend({}, page.result, resultFromCha);
            page.result.currentDigits = page.result.currentDigits.toString().split('');  // converts integer into list of strings
            $scope.readyToProcess = true;
          }
        } else if (resultFromCha.State === 2) {    // State 2 = End of exam
          page.dm.isSubmittable = true;   // make the page submittable

          page.result = $.extend({}, results.default(page.dm), page.result, {
            response: 'Exam Results',
            digitScore: resultFromCha.digitScore,
            presentationScore: resultFromCha.presentationScore,
            chaInfo: chaExams.getChaInfo()
          });
          examLogic.submit();
        }
      }

      // grade the user inputs and show correct answers
      function processDigits() {
        if ($scope.readyToProcess) {
          cha.requestResults()
            .then(function (result) {
              if (result.State === 0 ) { // State 0 = exam still running, ask again in 200 ms
                $timeout(processDigits, 500);
              } else if (result.State === 1 ) { // State 1 =  waiting for result  
                logger.info('CHA processing entered digits: ' + $scope.response);
                $scope.digitsDisabled = true;
                $scope.readyToProcess = false;
                $scope.feedback = angular.isDefined(page.dm.responseArea.feedback) ? page.dm.responseArea.feedback : true;
                _.forEach($scope.response, function (digit, index) {
                  if (digit === page.result.currentDigits[index]) {
                    $scope.digitCorrect[index] = true;
                  }
                });
                var feedbackDelay = page.dm.responseArea.feedbackDelay || 1000;
                $timeout(submitDigits, feedbackDelay);    //  Allow the correct answers to show for ~1 second before submitting results.
              }
            })
            .catch(function(err) {cha.errorHandler.main(err);});
        } else {
          $timeout(processDigits, 500);
        }
      }

      // Send digits back to the CHA and push presentation score onto the exam results stack
      function submitDigits() {
        // Grade Presentation Response
        page.result = $.extend({}, page.result, {
          response: $scope.response,
          numberCorrect: _.filter($scope.digitCorrect, function(digit) {return digit === true;}).length,
          numberIncorrect: _.filter($scope.digitCorrect, function(digit) {return digit === false;}).length,
          eachCorrect: $scope.digitCorrect,
          correct: _.every($scope.digitCorrect)
        });

        // push results
        examLogic.pushResults();

        // send results to cha and start new presentation
        var submission = {nCorrect: page.result.numberCorrect};
        logger.debug('Submitting digits to the CHAs: ' + JSON.stringify(submission));
        return cha.examSubmission('ThreeDigit$Submission', {nCorrect: page.result.numberCorrect})
//          .then(chaExams.runTOBifCalledFor)
          .then(resetResults)
          .then(getPresentationInfo)
          .catch(function(err) {cha.errorHandler.main(err);});
      }

      function resetResults() {
        $scope.resetKeypad();   // reset keypad

        // activate keypad
        var keypadDelay = page.dm.responseArea.keypadDelay || 5;
        $timeout(activateKeypad, keypadDelay);    // Activate keypad after N second

        var presentationId = page.result.presentationId;
        // TODO: this should be replaced by a "page.newResult()" or "results.new()"
        // this will require a change to examlogic, so skipping for now
        page.result = {};
        page.result = {
          examType: chaExams.examType,
          presentationId: presentationId,
          responseStartTime: new Date()
        };
      }

      function activateKeypad() {
        $scope.digitsDisabled = false;
        $scope.presentationCount += 1;         
      }

      // Clear keypad at the start and after each response - also fires on 'Clear' keypress
      $scope.resetKeypad = function () {
        $scope.response = [];
        $scope.digitCorrect = [false, false, false];
      };

      // User input from HTML
      $scope.addDigit = function (digit) {
        if ($scope.response.length < 3) {
          $scope.response.push(digit.toString());   // store as list of strings
          if ($scope.response.length === 3) {
            processDigits();
          }
        }
      };

      // start new Three Digit Exam
      newThreeDigitExam();

    });

});
