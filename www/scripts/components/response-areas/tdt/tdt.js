/**
 * Created by bpf on 3/18/2015.
 */

/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
/*globals _*/

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.tdt', [])

  // added $timeout parameter, HG 12/7/15
    .controller('ThreeDigitTestResponseAreaCtrl', function ($scope, examLogic, page, $timeout) {
      // Clear keypad at the start and after each response - also fires on 'Clear' keypress
      $scope.resetKeypad = function () {
        $scope.response = [];
        $scope.digitCorrect = [false, false, false];
        $scope.digitsDisabled = false;
      };

      //$scope.$watch('page.dm',update);

      function update(){
        // Function to run at exam initiation - happens when the responseArea changes
    
        //set to true, delay specifies when it will be enable, HG 12/7/15
        $scope.digitsDisabled = true;
            
        $scope.resetKeypad();
        page.result.response = undefined;
      }

      // User input from HTML
      $scope.addDigit = function (digit) {
        if ($scope.response.length < 3) {
          $scope.response.push(digit.toString());   // store as list of strings
          if ($scope.response.length === 3) {
            processDigits();
          }
        }
      };

      // set the button disbled for x msecs where x is specified by page.dm.delayEnable, HG 12/7/15
      $timeout(function(){
        $scope.digitsDisabled = false;
      }, page.dm.responseArea.delayEnable);

      update();

      // grade the user inputs and show correct answers
      function processDigits() {
        $scope.digitsDisabled = true;

        // default correct digits
        var responseAreaCorrect = page.dm.responseArea.correct;

        // if tdt page created through custom response Area feedback, numberCorrect/Incorrect still works, HG 7/3/18
        if (page.dm.changedFields.responseArea.correct) {
          responseAreaCorrect = page.dm.changedFields.responseArea.correct;
        }

        // set digit correct values
        if (responseAreaCorrect && responseAreaCorrect.length === 3) {
          _.forEach($scope.response, function (digit, index) {
            if (digit === responseAreaCorrect[index]) {
              $scope.digitCorrect[index] = true;
            }
          });
        } else {
          $scope.digitCorrect = [null, null, null];
        }

        setTimeout(submitDigits, 700);    //  Allow the correct answers to show for ~1 second before submitting results.
      }

      // append results and submit
      function submitDigits() {
        // Grade Presentation Response
        page.result = $.extend({}, page.result, {
          response: $scope.response,
          numberCorrect: _.filter($scope.digitCorrect, function(digit) {return digit === true}).length,
          numberIncorrect: _.filter($scope.digitCorrect, function(digit) {return digit === false}).length,
          eachCorrect: $scope.digitCorrect,
          correct: _.every($scope.digitCorrect)
        });
        examLogic.submit();
      }
    });

});
