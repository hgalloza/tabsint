/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.sound-recognition', [])
    .directive('soundRecognitionExam', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/sound-recognition/sound-recognition.html',
        controller: 'SoundRecognitionExamCtrl'
      };
    })
    .controller('SoundRecognitionExamCtrl', function ($interval, $q, $scope, $timeout, $uibModal, cha, chaExams, disk,
                                                      examLogic, logger, page, results) {
      var CHA_FLAGS_NOISE_ON = 0x00000080;

      // defaults
      var defaultCategories = [
        {
          name: 'AIRCRAFT',
          soundClasses: [
            {
              name: 'FIXED-WING',
              imgPath: 'img/fixed-wing.jpg',
              wavfiles: [
                {path:'C:USER/SRIN/Aircraft/Jet/A-J-0001.wav',playbackLevelAdjustment:11.1},
                {path:'C:USER/SRIN/Aircraft/Jet/A-J-0002.wav',playbackLevelAdjustment:10.81},
                {path:'C:USER/SRIN/Aircraft/Jet/A-J-0003.wav',playbackLevelAdjustment:5.65},
                {path:'C:USER/SRIN/Aircraft/Jet/A-J-0004.wav',playbackLevelAdjustment:10.51},
                {path:'C:USER/SRIN/Aircraft/Jet/A-J-0005.wav',playbackLevelAdjustment:11.8}
              ]
            },
            {
              name: 'ROTARY-WING',
              imgPath: 'img/rotary-wing.gif',
              wavfiles: [
                {path:'C:USER/SRIN/Aircraft/Rotor/A-R-0001.wav',playbackLevelAdjustment:17.25},
                {path:'C:USER/SRIN/Aircraft/Rotor/A-R-0002.wav',playbackLevelAdjustment:12.22},
                {path:'C:USER/SRIN/Aircraft/Rotor/A-R-0003.wav',playbackLevelAdjustment:17.13},
                {path:'C:USER/SRIN/Aircraft/Rotor/A-R-0004.wav',playbackLevelAdjustment:14.25},
                {path:'C:USER/SRIN/Aircraft/Rotor/A-R-0005.wav',playbackLevelAdjustment:18.29}
              ]
            }
          ]
        },
        {
          name: 'LANGUAGE',
          soundClasses: [
            {
              name: 'ENGLISH',
              imgPath: 'img/language_english.png',
              wavfiles: [
                {path:'C:USER/SRIN/Language/English/L-E-0001.wav',playbackLevelAdjustment:7.83},
                {path:'C:USER/SRIN/Language/English/L-E-0002.wav',playbackLevelAdjustment:10.06},
                {path:'C:USER/SRIN/Language/English/L-E-0003.wav',playbackLevelAdjustment:12.92},
                // {path:'C:USER/SRIN/Language/English/L-E-0004.wav',playbackLevelAdjustment:11},
                {path:'C:USER/SRIN/Language/English/L-E-0005.wav',playbackLevelAdjustment:12.99},
                {path:'C:USER/SRIN/Language/English/L-E-0006.wav',playbackLevelAdjustment:13.01},
                {path:'C:USER/SRIN/Language/English/L-E-0007.wav',playbackLevelAdjustment:12.7},
                {path:'C:USER/SRIN/Language/English/L-E-0008.wav',playbackLevelAdjustment:8.95},
                {path:'C:USER/SRIN/Language/English/L-E-0009.wav',playbackLevelAdjustment:8.14},
                {path:'C:USER/SRIN/Language/English/L-E-0010.wav',playbackLevelAdjustment:8.02},
                {path:'C:USER/SRIN/Language/English/L-E-0011.wav',playbackLevelAdjustment:8.64}
              ]
            },
            {
              name: 'OTHER',
              imgPath: 'img/language_other.png',
              wavfiles: [
                {path:'C:USER/SRIN/Language/Other/L-O-0001.wav',playbackLevelAdjustment:13.83},
                {path:'C:USER/SRIN/Language/Other/L-O-0002.wav',playbackLevelAdjustment:13.53},
                {path:'C:USER/SRIN/Language/Other/L-O-0003.wav',playbackLevelAdjustment:10.43},
                {path:'C:USER/SRIN/Language/Other/L-O-0004.wav',playbackLevelAdjustment:11.28},
                {path:'C:USER/SRIN/Language/Other/L-O-0005.wav',playbackLevelAdjustment:10.64},
                {path:'C:USER/SRIN/Language/Other/L-O-0006.wav',playbackLevelAdjustment:15.93},
                {path:'C:USER/SRIN/Language/Other/L-O-0007.wav',playbackLevelAdjustment:11.43},
//                {path:'C:USER/SRIN/Language/Other/L-O-0008.wav',playbackLevelAdjustment:5.77},
                {path:'C:USER/SRIN/Language/Other/L-O-0009.wav',playbackLevelAdjustment:8.76},
                {path:'C:USER/SRIN/Language/Other/L-O-0010.wav',playbackLevelAdjustment:11.5},
                {path:'C:USER/SRIN/Language/Other/L-O-0011.wav',playbackLevelAdjustment:10.58},
                {path:'C:USER/SRIN/Language/Other/L-O-0012.wav',playbackLevelAdjustment:9.6},
                {path:'C:USER/SRIN/Language/Other/L-O-0013.wav',playbackLevelAdjustment:13.8},
                {path:'C:USER/SRIN/Language/Other/L-O-0014.wav',playbackLevelAdjustment:13.77},
                {path:'C:USER/SRIN/Language/Other/L-O-0015.wav',playbackLevelAdjustment:12.71},
                {path:'C:USER/SRIN/Language/Other/L-O-0016.wav',playbackLevelAdjustment:7.79},
                {path:'C:USER/SRIN/Language/Other/L-O-0017.wav',playbackLevelAdjustment:7.59},
                {path:'C:USER/SRIN/Language/Other/L-O-0018.wav',playbackLevelAdjustment:7.94}
              ]
            }
          ]
        },
        {
          name: 'FOOTSTEPS',
          soundClasses: [
            {
              name: 'RUNNING',
              imgPath: 'img/running.png',
              wavfiles: [
                {path:'C:USER/SRIN/Footsteps/Running/F-R-0001.wav',playbackLevelAdjustment:22.19},
                {path:'C:USER/SRIN/Footsteps/Running/F-R-0002.wav',playbackLevelAdjustment:19.08},
                {path:'C:USER/SRIN/Footsteps/Running/F-R-0003.wav',playbackLevelAdjustment:20.21},
                {path:'C:USER/SRIN/Footsteps/Running/F-R-0004.wav',playbackLevelAdjustment:16.4}
              ]
            },
            {
              name: 'WALKING',
              imgPath: 'img/walking.png',
              wavfiles: [
                {path:'C:USER/SRIN/Footsteps/Walking/F-W-0001.wav',playbackLevelAdjustment:15.79},
                {path:'C:USER/SRIN/Footsteps/Walking/F-W-0002.wav',playbackLevelAdjustment:20.05},
                {path:'C:USER/SRIN/Footsteps/Walking/F-W-0003.wav',playbackLevelAdjustment:20.29},
                {path:'C:USER/SRIN/Footsteps/Walking/F-W-0004.wav',playbackLevelAdjustment:17.1},
                {path:'C:USER/SRIN/Footsteps/Walking/F-W-0005.wav',playbackLevelAdjustment:20.7}
              ]
            }
          ]
        },
        {
          name: 'GUNFIRE',
          soundClasses: [
            {
              name: 'INDOOR',
              imgPath: 'img/indoor1.png',
              wavfiles: [
                {path:'C:USER/SRIN/Gunfire/Indoor/G-I-0001.wav',playbackLevelAdjustment:5.53},
                {path:'C:USER/SRIN/Gunfire/Indoor/G-I-0002.wav',playbackLevelAdjustment:5.68},
                {path:'C:USER/SRIN/Gunfire/Indoor/G-I-0003.wav',playbackLevelAdjustment:6.1}
              ]
            },
            {
              name: 'OUTDOOR',
              imgPath: 'img/outdoor.png',
              wavfiles: [
                {path:'C:USER/SRIN/Gunfire/Outdoor/G-O-0001.wav',playbackLevelAdjustment:12.16},
                {path:'C:USER/SRIN/Gunfire/Outdoor/G-O-0002.wav',playbackLevelAdjustment:9.17},
//                {path:'C:USER/SRIN/Gunfire/Outdoor/G-O-0003.wav',playbackLevelAdjustment:8.29},
                {path:'C:USER/SRIN/Gunfire/Outdoor/G-O-0004.wav',playbackLevelAdjustment:14.1}
              ]
            }
          ]
        }
      ];
      var defaultPresentationMax = 50;
      var defaultIncorrectPresentationMax = 50;
      var defaultPointsGoal = 20;
      var defaultPointsAwardedForCorrectAnswer = 1;
      var defaultPointsAwardedForCorrectCategoryWrongSubcategory = 0;
      var defaultPointsAwardedForWrongCategory = -1;
      var defaultPointsAwardedForMaxedOutTrial = 1;
      var defaultStartSNR = -15;
      var defaultmaxSNR = 20;
      var defaultStepSizeSNR = 1;
      var defaultNoiseType = 'pink';
      var defaultNoiseLevel = 55;
      var defaultNoiseIdleLevel = 40;
      var defaultHidePointsTotalAndGoal = false;
      var defaultHideButtonPressTimer = false;
      var defaultTrainingMode = false;
      var defaultPresentAllTokensOnce = false;
      var defaultTrainingLevel = 70;
      var defaultTrainingGoal = 2;
      var defaultTrainingMaxExemplarRepeats = 10;
      var defaultResponseDelay = 1000;
      var defaultPause = false;
      var defaultPauseIfNoResponse = true;
      var defaultNTrialsWithoutResponsePause = 3;
      var defaultNoResponseMessage = 'It looks like you have not selected any sounds in a while.  Please see an administrator if you have any questions.';
      var defaultPauseIfIncorrect = true;
      var defaultNTrialsIncorrectPause = 2;
      var defaultIncorrectMessageInitial = 'It looks like you are choosing some incorrect answers.  Remember, only choose an answer if you are sure.';
      var defaultIncorrectMessageRepeat = 'It looks like you are still choosing some incorrect answers.  See the test administrator for help.';

      // if protocol-level categories, we have to handle the image paths.
      // TODO - this should not be here, it should be in the protocol load section.
      if (angular.isDefined(page.dm.responseArea.categories)) {
        page.dm.responseArea.categories.forEach(function(category) {
          category.soundClasses.forEach(function(soundClass) {
            // make sure it's not a hard-coded assets path
            if (soundClass.imgPath.indexOf(disk.protocol.path) < 0) {
              soundClass.imgPath = disk.protocol.path + soundClass.imgPath;
            }
          });
        });
      }

      function convertCategories(c) {
        var list = [];
        c.forEach(function(category){
          category.soundClasses.forEach(function(soundClass) {
            soundClass.wavfiles.forEach(function(wavfile){
              var adjustment = checkDefined(wavfile.playbackLevelAdjustment, 0);
              list.push({wavfilePath: wavfile.path, wavfileAdjustment: adjustment, category: category.name, soundClass: soundClass.name});
            });
          });
        });
        return list;
      }

      function checkDefined(param, defaultParam) {
        if (angular.isDefined(param)) {
          return param;
        } else {
          return defaultParam;
        }
      }

      // set up exam properties - these can be set from the protocol
      var ra = page.dm.responseArea;
      // Training properties
      var trainingMode = checkDefined(ra.trainingMode, defaultTrainingMode);
      var trainingLevel = checkDefined(ra.trainingLevel, defaultTrainingLevel);
      var trainingGoal = checkDefined(ra.trainingGoal, defaultTrainingGoal);
      var trainingMaxExemplarRepeats = checkDefined(ra.trainingMaxExemplarRepeats, defaultTrainingMaxExemplarRepeats);
      var responseDelay = checkDefined(ra.responseDelay, defaultResponseDelay);
      // non-training/shared properties
      var presentAllTokens = checkDefined(ra.presentAllTokens, defaultPresentAllTokensOnce);
      var hidePointsTotalAndGoal = checkDefined(ra.hidePointsTotalAndGoal, defaultHidePointsTotalAndGoal);
      var hideButtonPressTimer = checkDefined(ra.hideButtonPressTimer, defaultHideButtonPressTimer);
      var pauseIfNoResponse = checkDefined(ra.pauseIfNoResponse, defaultPauseIfNoResponse);
      var nTrialsWithoutResponsePause = checkDefined(ra.nTrialsWithoutResponsePause, defaultNTrialsWithoutResponsePause);
      var pauseIfIncorrect = checkDefined(ra.pauseIfIncorrect, defaultPauseIfIncorrect);
      var nTrialsIncorrectPause = checkDefined(ra.nTrialsIncorrectPause, defaultNTrialsIncorrectPause);
      var categories = checkDefined(ra.categories, defaultCategories);
      var exemplars = convertCategories(categories);
      var startSNR = checkDefined(ra.startSNR, defaultStartSNR);  // dB
      var stepSizeSNR = checkDefined(ra.stepSizeSNR, defaultStepSizeSNR);
      var maxSNR = checkDefined(ra.maxSNR, defaultmaxSNR);  // dB
      var pointsGoal = checkDefined(ra.pointsGoal, defaultPointsGoal);
      var pointsAwardedForCorrectAnswer = checkDefined(ra.pointsAwardedForCorrectAnswer, defaultPointsAwardedForCorrectAnswer);
      var pointsAwardedForRightCategoryWrongSubcategory = checkDefined(ra.pointsAwardedForRightCategoryWrongSubcategory, defaultPointsAwardedForCorrectCategoryWrongSubcategory);
      var pointsAwardedForWrongCategory = checkDefined(ra.pointsAwardedForWrongCategory, defaultPointsAwardedForWrongCategory);
      var pointsAwardedForMaxedOutTrial = checkDefined(ra.pointsAwardedForMaxedOutTrial, defaultPointsAwardedForMaxedOutTrial);
      var presentationMax = checkDefined(ra.presentationMax, defaultPresentationMax); // The exam is complete when this number of exemplars have been presented, regardless of number correct
      var incorrectPresentationMax = checkDefined(ra.incorrectPresentationMax, defaultIncorrectPresentationMax);
      var backgroundNoiseType = checkDefined(ra.backgroundNoiseType, defaultNoiseType); // white/pink/red...
      var backgroundNoiseLevel = trainingMode? 0 : checkDefined(ra.backgroundNoiseLevel, defaultNoiseLevel); // This controls the noise level AND the target level, using SNR
      backgroundNoiseLevel = [backgroundNoiseLevel, backgroundNoiseLevel]; // Making an array of left/right for the CHA.  Not exposing left/right to user yet
      var backgroundNoiseIdleLevel = checkDefined(ra.backgroundNoiseIdleLevel, defaultNoiseIdleLevel);
      backgroundNoiseIdleLevel = [backgroundNoiseIdleLevel, backgroundNoiseIdleLevel]; // Making an array of left/right for the CHA. Currently hardcoded to be the same for each ear

      // properties that are hard-coded
      var minOccurrences = checkDefined(ra.minOccurrences, 2);

      var maxLevel = maxSNR + backgroundNoiseLevel[0];
      var currentMaxLevel = maxLevel;
      var backgroundNoise = {
        Type: backgroundNoiseType,
        Level: backgroundNoiseLevel
      };
      //backgroundNoiseEar = checkDefined(ra.backgroundNoiseEar) ?  ra. || 2; // both ears - currently using only default of 2

      // control variables
      var currentLevel, currentPresentation, exemplarPlayCount, userAttempts;
      var timerInterval, popupTimeout, modal, playNextTimeout; // timing - this interval gets set and cleared to control exam flow
      var active = true;

      /***************************** Setup *************************************
      Instead of figuring out which exmplar to play next on the fly, all lists
      are generated ahead of time.

      For the full exam mode (non-training) all requirement rules for presentation
      order can be satisfied during list generation

      For the training exam mode, the requirement: once a user has 2 correct
      answers on the FIRST press for a given sound CLASS, that sound class will
      be presented at most one more time.  This requires adaptive list changes,
      which are performed by the updateTrainingSuccesses function

      */

      // for training purposes, the presentation list rule is:
      // No sound exemplars within a given sound class will be repeated until every one in that sound class has been presented.
      // The simplest way to confirm that is to concat as many Shuffled exmeplar lists as necessary to generate a list of the desired length.
      function generateTrainingList() {
        var list = [];

        for (var i = 0; i < presentationMax; i++) {
          list = list.concat(_.shuffle(exemplars));
        }

        var numOfExemplars = exemplars.length;
        list = list.slice(0, trainingMaxExemplarRepeats*numOfExemplars); // if we slice this here, we could potentially run low of presentationMax due to slices based on user responses

        return list;
      }

      // For full exam (non-training) to quickly acquire lots of data on each of the sound tokens, the rule is:
      // Present all tokens once to each subject, in randomized order.
      function generateAllSoundsList() {
          var list = [];
          var testCondition;
          // If exam is just starting, create a random list from all tokens. Otherwise, regenerate a random list and remove tokens that have been answered correctly.
          list = list.concat(_.shuffle(exemplars));
          if (chaExams.storage.soundRecognition.points > 0) {
              list = list.filter(function(item) {
                  testCondition = true;
                  _.forEach(chaExams.storage.soundRecognition.presentedExemplars, function(element) {
                      if (item.wavfilePath === element.presentedWavfile ) {
                        if (element.correct === true) {
                            testCondition = false;
                        }
                      }
                  });
                  if (testCondition === true) {
                      return item;
                  }
              });
          }
          return list;
      }

      // For full exam (non-training) the presentation list rule is:
      // grab sounds from overall list at random, until a soundClass has been presented 3x.  Then don't present that soundClass
      // again until all other soundClasses have been presented at least twice.  Then reset.
      // Note:  There will be more than one wavfile for each soundClass
      function generateExamList() {
        var list = []; // the output
        var tmpExemplarList = exemplars.slice(0); // make a temporary copy

        function checkClassOccurrences(fromList, exemplar, sublist) {
          // count number of occurrences of the most recently added exemplar's soundClass - if >= minOccurrences, remove all of that soundClass from the drawing pool
          var occurrences = sublist.filter(function(item){return item.soundClass === exemplar.soundClass;}).length;
          if (occurrences > minOccurrences) {
            fromList = fromList.filter(function(item) {return item.soundClass !== exemplar.soundClass});
          }
          return fromList;
        }

        function checkListStatus(fromList, sublist) {
          // count number of occurrences of each soundClass - if all >= minOccurrences, done with this sublist
          var counts = fromList.map(function(listItem) { return sublist.filter(function(listItem2) {return listItem.soundClass === listItem2.soundClass;}).length; });
          var complete = counts.every(function(item) { return item >= minOccurrences;});
          return complete;
        }

        function getRandomExemplar(fromList) {
          // build list of remaining categories, select one at random, filter list to just the selected category
          var categoryNameList = _.uniq(fromList.map(function(item){return item.category}));
          var categoryName = categoryNameList[Math.floor(Math.random()*categoryNameList.length)];
          var categoryList = fromList.filter(function(listItem) {return listItem.category === categoryName});
          // build list of remaining soundClassses (soundClasses get removed after playing more than minOccurrences), select one at random, filter list to just the selected soundCLass
          var soundClassNameList = _.uniq(categoryList.map(function(item){return item.soundClass}));
          var soundClassName = soundClassNameList[Math.floor(Math.random()*soundClassNameList.length)];
          var soundClassList = categoryList.filter(function(listItem) {return listItem.soundClass === soundClassName});
          // grab a random exemplar from the filtered soundClass list.
          var newExemplar = soundClassList[Math.floor(Math.random()*soundClassList.length)];  // get a random exemplar
          return newExemplar;
        }

        // Generate a sub list, long enough to satisfy the presentation rules
        // and get to the point of 'reset' after presenting all sound classes
        // at least minOccurrences times
        function generateSublist(fromList) {
          var sublist = [];
          var newExemplar;

          var maxIter = (minOccurrences + 1) * fromList.length; // overkill, but will always work

          for (var i = 0; i < maxIter; i++) {
            // get random exemplar, by first choosing a random category, then random soundClass, then random exemplar within that soundClass.
            // this ensures even distribution of categories even when the number of exemplars in each category is drastically different!
            newExemplar = getRandomExemplar(fromList);
            sublist.push(newExemplar);

            // count occurrences of newExemplar's soundClass, if > min, remove all exemplars with same soundClass from list
            // This ensures it won't present again for this sub list
            fromList = checkClassOccurrences(fromList, newExemplar, sublist);

            // get count for each exemplar, done if all meet or exceed the min
            if (checkListStatus(fromList, sublist)) {break;}
          }

          return sublist;
        }

        // build the list in chunks, based on the presentation rules.
        for (var i = 0; i < presentationMax; i++) {
          // building the list
          tmpExemplarList = exemplars.slice(0); // reset the temporary copy
          var tmpList = generateSublist(tmpExemplarList);
          list = list.concat(tmpList);
          if (list.length >= presentationMax) {
            // list of sufficient length - breaking
            break;
          }
        }

        // cut the extra items out from the combined chunks
        list = list.slice(0, presentationMax);
        return list;
      }

      function generateList() {
        if (trainingMode) {
          return generateTrainingList();
        } else if (presentAllTokens) {
              return generateAllSoundsList();
        } else {
          return generateExamList();
        }
      }

      function resetExam() {
        chaExams.storage.soundRecognition = {
          list: [],
          points: 0,
          trainingSuccesses: [],
          presentedExemplars: [],
          nTrialsWithoutResponse: 0,
          nTrialsIncorrect: 0,
          nIncorrectPauses: 0,
          nTotalIncorrect: 0
        };
        chaExams.storage.soundRecognition.list = generateList();
      }

      /* If chaExams.storage contains a soundRecognition list, we were in the
      middle of an exam when the user/admin went to the admin panel, perhaps to
      reset CHA connection, and the exam will pick up where it was left using
      the stored list.
      Otherwise generate a new list. */
      if (angular.isUndefined(chaExams.storage.soundRecognition) || angular.isUndefined(chaExams.storage.soundRecognition.list)) {
        resetExam();
        // logger.debug('CHA sound-recognition generated list: ' + JSON.stringify(chaExams.storage.soundRecognition.list));
      }

      // View variables
      $scope.categories = categories;
      $scope.timeCounter = 0;
      $scope.pointsGoal = pointsGoal;
      $scope.responseDisabled = true;
      $scope.trainingMode = trainingMode;
      $scope.hidePointsTotalAndGoal = hidePointsTotalAndGoal;
      $scope.hideButtonPressTimer = hideButtonPressTimer;
      $scope.srintState = 'exam';
      $scope.pause = checkDefined(ra.pause, defaultPause);
      $scope.noResponseMessage = checkDefined(ra.noResponseMessage, defaultNoResponseMessage);
      $scope.incorrectMessageInitial = checkDefined(ra.incorrectMessageInitial, defaultIncorrectMessageInitial);
      $scope.incorrectMessageRepeat = checkDefined(ra.incorrectMessageRepeat, defaultIncorrectMessageRepeat);
      $scope.soundDetectionTime = undefined;
      $scope.soundCategoryChosen = false;

      // onExit function, to make sure popups and timers do not remain
      $scope.$on("$destroy", function() {
        active = false; // ensure the exam will not continue looping
        stopTimer();
      });

      // disable submit function until complete
      page.dm.isSubmittable = false;

      // set callback for dropped connections
      cha.connectionDropCB = function() {
        logger.debug('CHA - Running cha connectionDropCB to reset Exam');
        cha.abortExams()
          .then(startExam);
      };

      // when the controller is loaded, start the exam
      startExam();

      // start the sound recognition exam
      function startExam() {

        // turn on the noise if not training
        if (!trainingMode) {

          // Start noise depending on the current state of the cha flags (CHA_FLAGS_NOISE_ON)
          cha.requestStatus()
            .then(function(status) {
              if (status.Flags & CHA_FLAGS_NOISE_ON) {
                return cha.changeNoiseFeatureLevel(backgroundNoise.Level);
              } else {
                return cha.startNoiseFeature(backgroundNoise);
              }
            })
            .then(setNextPresentation)
            .catch(function(e){
              logger.error('Could not start CHA Sound Detection exam: ' + JSON.stringify(e));
            });

        // if in training mode, go to the first presentation without turning on noise
        } else {
          setNextPresentation();
        }
      }

      // Keeps track of time to response for display/results purposes
      function startTimer() {
        timerInterval = $interval(function() {
          $scope.timeCounter += 0.1;
        }, 100);
      }

      // Clean up popups and timers on responseArea exit OR on user sound selection
      function stopTimer() {
        $interval.cancel(timerInterval);
        if (popupTimeout) {$timeout.cancel(popupTimeout);}
        if (modal && modal.close) {modal.close();}
        if (playNextTimeout) {$timeout.cancel(playNextTimeout);}
      }


      // Grab the first examplar in the presentation list and setup start level,
      // timer, and attempt counter for presentaiton.  Exemplars are unshifted
      // from the list after user responses.
      function setNextPresentation() {
        // reset results
        resetResults();

        // set the state of the exam
        $scope.srintState = 'exam';

        // grab the current presentation definition
        currentPresentation = chaExams.storage.soundRecognition.list[0];
        $scope.currentPresentation = currentPresentation; // TODO remove this once demo phase is over

        // process current properties
        currentLevel = trainingMode? trainingLevel : (backgroundNoise.Level[0] + startSNR);
        currentPresentation.wavfileAdjustment = currentPresentation.wavfileAdjustment; // defaults to 0, set above during list generation
        currentLevel += currentPresentation.wavfileAdjustment;
        currentLevel = (Math.round(currentLevel*100))/100;
        currentMaxLevel = maxLevel + currentPresentation.wavfileAdjustment;
        currentMaxLevel = (Math.round(currentMaxLevel*100))/100;

        // reset the counts
        $scope.timeCounter = 0;
        exemplarPlayCount = 0; // keep track of playCount for this exempalr for results analysis AND training max
        userAttempts = []; // array of attempts for analysis AND training FIRST attempt logic

        // set the current exam properties
        currentPresentation.examProperties = {
          Leq: [currentLevel, currentLevel, 0, 0], // currently hardcoded to play to both ears
          SoundFileName: currentPresentation.wavfilePath,
          UseMetaRMS: true
        };

        // start playing the sound
        startPlaying();
      }

      // begin the loop for the current presentation exemplar
      function startPlaying() {
        active = true; // enable looping
        exemplarPlayCount += 1;

        startTimer();
        return playCurrentSound();
      }

      // play the sound at currentPresentation.examProperties and wait for it to be over
      function playCurrentSound() {

        return cha.requestStatusBeforeExam()
          .then(checkActive)
          .then(function() {return cha.queueExam('PlaySound', currentPresentation.examProperties) })
          .then(cha.requestResults)   // TODO: remove when `wait.forReadyState` uses `requestResults`
          .then(function() { return chaExams.wait.forReadyState(1000, 400) })
          .then(loopSound)
          .catch(function(e) {
            logger.warn('playCurrentSound chain rejected: ' + JSON.stringify(e));
          });
      }

      // convience function to check the active boolean and return a promise
      function checkActive() {
        if (active) {
          return $q.resolve();
        } else {
          logger.log('Sound Recognition not active');
          return $q.reject({msg: 'Sound Recognition not active'});
        }
      }

      // loop function.  Queue exam at current desired level, playLoop waits for
      // queued exam (playing the sound) to complete, then resolves to
      // loopSound, process starts again.  Broken by reaching maxSNR in full
      // exam, or reaching maxRepeats in trainingMode.
      function loopSound() {

        if ( (!trainingMode && currentLevel <= (currentMaxLevel - stepSizeSNR)) || (trainingMode && exemplarPlayCount < trainingMaxExemplarRepeats)) {

          // increase counts and level
          exemplarPlayCount += 1;
          currentLevel = trainingMode? currentLevel : (Math.min(currentLevel + stepSizeSNR, currentMaxLevel));
          currentLevel = (Math.round(currentLevel*100))/100;

          // set to current presentation
          currentPresentation.examProperties.Leq = [currentLevel, currentLevel, 0, 0];

          // play this sound after `responseDelay` (playCurrentSound() checks if we're still active)
          $timeout(function(){
            playCurrentSound();
          }, responseDelay);

        } else {
          $timeout(function(){
            $scope.selectSoundClass();
          }, responseDelay);
        }

      }


      // Check ability to continue.  If done for one of serveral reasons, finalize
      // otherwise, push results, remove first exemplar from presentation list
      // and continue presenting.
      function submitResultsAndGoToNextInList() {
        var presentationCount = chaExams.storage.soundRecognition.presentedExemplars.length; // shortening the conditional statement
        var remainingExams = chaExams.storage.soundRecognition.list.length; // shortening the conditional statement
        var points = chaExams.storage.soundRecognition.points; // shortening the conditional statement

        // Priority order:
        // 1 - if the exam has not reached presentationMax, continue
        // 2 - if there are more sounds to play, continue
        // 3 - if training mode OR exam mode and goal not yet reached, continue
        if (presentAllTokens === true && remainingExams <= 1 && points < pointsGoal)   {
            chaExams.storage.soundRecognition.list = chaExams.storage.soundRecognition.list.concat(generateAllSoundsList());
            remainingExams = chaExams.storage.soundRecognition.list.length; // resetting remaining exams
        }

        if ( (presentationCount >= presentationMax) ||
             (angular.isDefined(incorrectPresentationMax) && chaExams.storage.soundRecognition.nTotalIncorrect >= incorrectPresentationMax) ||
             (remainingExams <= 1 && presentAllTokens === false) ||
             (remainingExams <= 1 && presentAllTokens === true) ||
             (!trainingMode && points >= pointsGoal) ||
             (trainingMode && checkTrainingComplete())
           ) {
          // we are done - don't push results - the normal submit method will handle that.
          finalize();
        } else {
          // we still have presentations to go

          // results have been properly assembled in the submitClass function
          examLogic.pushResults();

          if (!trainingMode) {
            // REGULAR EXAM - restore noise to exam level, continue
            cha.changeNoiseFeatureLevel(backgroundNoise.Level);
          }

          // if subject has not responded for several trials, pause the test
          if (pauseIfNoResponse && chaExams.storage.soundRecognition.nTrialsWithoutResponse >= nTrialsWithoutResponsePause) {
            $scope.pauseExam('noResponse');
          } else if (pauseIfIncorrect && chaExams.storage.soundRecognition.nTrialsIncorrect >= nTrialsIncorrectPause) {
            chaExams.storage.soundRecognition.nTrialsIncorrect = 0;
            chaExams.storage.soundRecognition.nIncorrectPauses += 1;
            if (chaExams.storage.soundRecognition.nIncorrectPauses === 1) {
              $scope.pauseExam('incorrectResponseInitial');
            } else {
              $scope.pauseExam('incorrectResponseRepeat');
            }
          } else {
            $timeout(
              function(){
                // grab next presentation, reset exams
                chaExams.storage.soundRecognition.list.shift(); // remove first entry

                setNextPresentation();
              }, 100);
          }
        }
      }

      $scope.pauseExam = function(pauseTrigger) {
        stopTimer(); // but don't clear - we want to display the time to the user and save it to results
        active = false;

        chaExams.cancelPolling();
        cha.abortExams();

        // generate a result so analysts know the exam was paused
        page.result = {
          examType: chaExams.examType,
          presentationId: page.dm.id,
          responseStartTime: new Date(),
          response: 'paused',
          pauseTrigger: pauseTrigger
        };
        examLogic.pushResults();

        $scope.pauseTrigger = pauseTrigger;
        $scope.srintState = 'pause';
        if (!trainingMode) {
          cha.changeNoiseFeatureLevel(backgroundNoiseIdleLevel);
        }
      };

      $scope.resumeExam = function() {
        if (!trainingMode) {
          // restore noise to exam level, continue
          cha.changeNoiseFeatureLevel(backgroundNoise.Level);
        }

        // generate a result so analysts know the exam was paused/resumed
        page.result = {
          examType: chaExams.examType,
          presentationId: page.dm.id,
          responseStartTime: new Date(),
          response: 'resumed'
        };
        examLogic.pushResults();

        chaExams.storage.soundRecognition.nTrialsWithoutResponse = 0;
        setNextPresentation();
      };

      $scope.restartExam = function() {
        // generate a result so analysts know the exam was paused/restarted
        page.result = {
          examType: chaExams.examType,
          presentationId: page.dm.id,
          responseStartTime: new Date(),
          response: 'restarted'
        };
        examLogic.pushResults();

        chaExams.storage.soundRecognition.nTrialsWithoutResponse = 0;
        resetExam();
        startExam();
      };

    // for now, only record time when category is chosen, nothing else in the logic changes
      $scope.selectSoundCategory = function(category) {

          // stop all sounds
          if (!active) {
              logger.warn('CHA Sound Recognition exam.  User selected a sound category while not in state "active"');
              return;
          }
          logger.info('CHA processing sound-detection response: ' + JSON.stringify(category));

          if (category === currentPresentation.category) {
              $scope.soundDetectionTime = $scope.timeCounter.toFixed(1);
              logger.info('Seconds To Detect Category: '+ $scope.soundDetectionTime);
          } else {
              // for now, do nothing
          }

          $scope.soundCategoryChosen = true;
      };

      // get user input, show feedback,
      $scope.selectSoundClass = function(category, soundClass) {

        // stop all sounds
        if (!active) {
          logger.warn('CHA Sound Recognition exam.  User selected a sound class while not in state "active"');
          return;
        }

        // If full exam, drop noise to idle level
        if (!trainingMode) {
          cha.changeNoiseFeatureLevel(backgroundNoiseIdleLevel);
        }

        stopTimer(); // but don't clear - we want to display the time to the user and save it to results
        active = false;
        cha.abortExams();

        logger.info('CHA processing sound-recognition response: ' + JSON.stringify(soundClass));
        $scope.responseDisabled = true;
        var msg = '';

        // Handle checking for unresponsive listener
        if (angular.isUndefined(category)) {
          // reached a limit on this presentation without a user press - submit empty
          chaExams.storage.soundRecognition.nTrialsWithoutResponse += 1;
        } else {
          // user selected something - reset this counter
          chaExams.storage.soundRecognition.nTrialsWithoutResponse = 0;
        }

        // grade response, tally points
        var correct = false;
        if (angular.isUndefined(category)) {
          if (!trainingMode) {
            msg = 'NO BUTTON PRESSED';
          } else if (trainingMode){
            msg = 'INCORRECT.  REACHED THE MAXIMUM NUMBER OF TRIES FOR THIS SOUND.';
          }
          chaExams.storage.soundRecognition.points = Math.max(0, chaExams.storage.soundRecognition.points + pointsAwardedForMaxedOutTrial); // tally points for Trial Max Out
        } else if (soundClass === currentPresentation.soundClass && category === currentPresentation.category) {
          msg = 'CORRECT';
          correct = true;
          chaExams.storage.soundRecognition.points = Math.max(0, chaExams.storage.soundRecognition.points + pointsAwardedForCorrectAnswer); // tally points for Correct Response
        } else if (soundClass !== currentPresentation.soundClass && category === currentPresentation.category) {
          msg = 'INCORRECT'; // still incorrect, just different points than completely wrong
          chaExams.storage.soundRecognition.points = Math.max(0, chaExams.storage.soundRecognition.points + pointsAwardedForRightCategoryWrongSubcategory); // tally points for right category wrong class
        } else if (soundClass !== currentPresentation.soundClass && category !== currentPresentation.category) {
          msg = 'INCORRECT';
          chaExams.storage.soundRecognition.points = Math.max(0, chaExams.storage.soundRecognition.points + pointsAwardedForWrongCategory); // tally points for completely wrong response
        }

        // if listener picked something but it was wrong, tally it in case the protocol calls for showing listener a message after n incorrect choices.
        if (angular.isDefined(category) && !correct) {
          chaExams.storage.soundRecognition.nTrialsIncorrect += 1;
          chaExams.storage.soundRecognition.nTotalIncorrect += 1;
        } else if (angular.isUndefined(category)) {
          chaExams.storage.soundRecognition.nTotalIncorrect += 1;
        }
//
//        if (angular.isDefined(category) && !trainingMode) {
//          msg += '.  Seconds To Button Press: '+$scope.timeCounter.toFixed(1);
//        }

        // show feedback message
        modal = $uibModal.open({
          template: '<h3 style="text-align:center; padding:10px;">'+msg+'</h3>',
          size: 'sm'
        });

        // wait 3 seconds, then close feedback popup
        popupTimeout = $timeout(finish, 3000);


        function finish() {
          modal.close();
          if (!trainingMode) {
            // REGULAR EXAM - restore noise to exam level, continue
            // cha.changeNoiseFeatureLevel(backgroundNoise.Level);

            setResultsAndGoToNext();
          } else {
            // TRAINING MODE - handle logic based on user attempts
            userAttempts.push({category: category, soundClass: soundClass});
            if (correct || exemplarPlayCount >= trainingMaxExemplarRepeats) {
              if (correct && userAttempts.length === 1) {
                // User got it on first try - apply rule (if user gets soundClass on FIRST try 2x, present that sound class at most once more.)
                updateTrainingSuccesses(soundClass);
              }
              // User got it right.  store results and move on to next sound.
              setResultsAndGoToNext();
            } else {
              // User got it wrong - keep playing it until they get it right.
              startPlaying();
            }

          }

          // common method to add relevant results to the result object and go on to the next presentation
          function setResultsAndGoToNext() {
            var currentLevel = currentPresentation.examProperties.Leq[0];
            // NOTE this is a big line - we bring the level back to un-adjusted value.
            currentLevel = currentLevel - currentPresentation.wavfileAdjustment;
            currentLevel = (Math.round(currentLevel*100))/100; // avoid getting a float number here, like 34.99999999999927....

            // prepare for bulk data processing here by appropriately storing
            var bulk = {
              chosenCategory: category,
              chosenSoundClass: soundClass,
              response: {category: category, soundClass: soundClass}, // filling in the generic tabsint 'response' field  TODO - could also put correct here
              correct: correct, // did the user get it right?
              points: chaExams.storage.soundRecognition.points,
              presentedCategory: currentPresentation.category, // object holding the details of the current presentation, (category, soundClass, wavfile, playSound spec (wavfile, Leq) )
              presentedSoundClass: currentPresentation.soundClass,
              presentedWavfile: currentPresentation.wavfilePath,
              presentedLevel: currentLevel, // level desired by test
              presentedLevelOffset: currentPresentation.wavfileAdjustment,  // level adjustment to get specific wavefile to level desired by test
              presentedSNR: trainingMode? currentLevel : currentLevel - backgroundNoiseLevel[0],
              levelChangedB: trainingMode? 0 : currentLevel - (startSNR + backgroundNoiseLevel[0]),
              timeToButtonPress: Math.round($scope.timeCounter*10)/10,
              soundDetectionTime: Math.round($scope.soundDetectionTime*10)/10,
              trainingAttempts: userAttempts, // each user input for the current presentation (1 for non-training mode, >=1 for trainingMode)
              trainingAttemptCount: userAttempts.length,
              exemplarPlayCount: exemplarPlayCount, // number of times the current presentation played (before user press for non-trainingMode, before user got it right for trainingMode)
              trainingMode: trainingMode
            };

            chaExams.storage.soundRecognition.presentedExemplars.push(bulk);

            page.result = $.extend({}, page.result, results.default(page.dm), {chaInfo: chaExams.getChaInfo()}, bulk);

            submitResultsAndGoToNextInList();
          }

        }
      };

      // The training rule states that training will continue until user has
      // 2 correct responses on the *first* button press for each sound CLASS
      function checkTrainingComplete() {
        // get list of all soundClasses
        var soundClasses = exemplars.map(function(item){return item.soundClass});
        // reduce to unique entries
        soundClasses = soundClasses.filter(function(item,ind){return soundClasses.indexOf(item) === ind});
        // filter through soundClasses to see if any are not unrepresented or underrepresented in trainingSuccesses
        var incomplete = soundClasses.filter(function(soundClass) {
          return angular.isUndefined(chaExams.storage.soundRecognition.trainingSuccesses[soundClass]) ||
            chaExams.storage.soundRecognition.trainingSuccesses[soundClass] < trainingGoal;
        });

        return incomplete.length <= 0;
      }

      // handle the adaptive logic in the training version of this examProperties
      // If the listener has already successfully identified targets in a soundClass x times, don't present any sounds from that soundClass again
      function updateTrainingSuccesses(soundClass) {
        if (!!chaExams.storage.soundRecognition.trainingSuccesses[soundClass]) {
          chaExams.storage.soundRecognition.trainingSuccesses[soundClass] += 1;
        } else {
          chaExams.storage.soundRecognition.trainingSuccesses[soundClass] = 1;
        }

        // Once this soundClass has been correctly identified X times on FIRST button press
        // this soundClass should present at MOST one more time.  Remove all but the next
        if (chaExams.storage.soundRecognition.trainingSuccesses[soundClass] >= trainingGoal) {
          var keepInd = chaExams.storage.soundRecognition.list.findIndex(function(item) {return item.soundClass === soundClass}); // get first index
          chaExams.storage.soundRecognition.list = chaExams.storage.soundRecognition.list.filter(function(item, ind) {
            return (item.soundClass !== soundClass) || (ind === keepInd); // return list after removing all that match this soundClass except the first that does
          });
        }
      }

      function resetResults() {
        page.result = {
          examType: chaExams.examType,
          presentationId: page.dm.id,
          responseStartTime: new Date()
        };
        $scope.soundDetectionTime = undefined;
        $scope.soundCategoryChosen = false;
      }

      function finalize(){
        // perform any bulk data processing

        // store the data as a final result
        resetResults();
        page.result.presentationId += '_Aggregated';
        page.result = $.extend({}, page.result, {
          chaInfo: chaExams.getChaInfo(), // details about the CHA
          presentedExemplars: chaExams.storage.soundRecognition.presentedExemplars // bulk data for easier analysis
        });


        // reset storage
        delete chaExams.storage.soundRecognition;

        page.dm.isSubmittable = true;

        // Auto submit
        examLogic.submit();
      }

    });

});
