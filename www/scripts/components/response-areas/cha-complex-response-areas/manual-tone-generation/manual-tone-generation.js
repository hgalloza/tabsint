/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.manual-tone-generation', [])
    .directive('manualToneGenerationExam', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/manual-tone-generation/manual-tone-generation.html',
        controller: 'ManualToneGenerationExamCtrl'
      };
    })
    .controller('ManualToneGenerationExamCtrl', function ($scope, $q, $timeout, cha, page, disk, chaExams, chaResults, logger) {
      // setup
      var examType = 'ToneGeneration';         // remove 'cha' from prefix
      $scope.btnState = 0;
      var startTime = new Date();
      page.dm.isSubmittable = true;

      // prepare presentationList and examProperties
      $scope.presentationList = {};
      $scope.presentationList.Left = angular.copy(page.dm.responseArea.presentationList);
      $scope.presentationList.Right = angular.copy(page.dm.responseArea.presentationList);

      var defaultExamProperties = page.dm.responseArea.commonPresentationProperties || {};
      defaultExamProperties.Level = defaultExamProperties.Level || 65; // standard default
      defaultExamProperties.ToneDuration = defaultExamProperties.ToneDuration || 10000; // 10 seconds

      // build examProps from universal defaults and from pres-specific, build for left and right
      //console.log('DEBUG: presentationList: '+angular.toJson($scope.presentationList));
      //console.log('DEBUG: defaultExamProps: '+angular.toJson(defaultExamProperties));
      _.forEach($scope.presentationList.Left,function(pres){
        var examProps = _.extend({}, defaultExamProperties, pres);
        examProps.OutputChannel = 'HPL0';
        pres.examProperties = examProps;
      });
      _.forEach($scope.presentationList.Right,function(pres){
        var examProps = _.extend({}, defaultExamProperties, pres);
        examProps.OutputChannel = 'HPR0';
        pres.examProperties = examProps;
      });
      //console.log('DEBUG: presentationList Left: '+angular.toJson($scope.presentationList['Left']));

      // view fields
      $scope.data = {
        presIndex: 0,
        channel: undefined,
        levelUnits: 'dB SPL',
        minLevel: undefined,
        maxLevel: undefined
      };

      $scope.frequencyButtonClass = function(index){
        if (index === $scope.data.presIndex){
          return 'btn btn-default btn-success';
        } else {
          return 'btn btn-default';
        }
      };

      // set min and max levels, handling undefined and 0 cases
      if (angular.isDefined(page.dm.responseArea.minLevel)){
        $scope.data.minLevel = page.dm.responseArea.minLevel;
      } else {
        $scope.data.minLevel = -80;
      }
      if (angular.isDefined(page.dm.responseArea.maxLevel)){
        $scope.data.maxLevel = page.dm.responseArea.maxLevel;
      } else {
        $scope.data.maxLevel = 100;
      }

      // Starting channel.  Start with channel defined in exame props, or default left
      var channel = defaultExamProperties.OutputChannel || 'HPL0';

      $scope.btnText = ['Play Tones', 'Stop Tones'];
      if (channel.indexOf('R') > -1) {
        $scope.data.channel = 'Right';
      } else {
        $scope.data.channel = 'Left';
      }

      window.presentationList = $scope.presentationList;

      $scope.playTonesButtonClass = function(){
        var presentButtonClass;
        if ($scope.data.channel === 'Right'){
          presentButtonClass = ['btn btn-danger', 'btn btn-danger active'];
        } else {
          presentButtonClass = ['btn btn-primary','btn btn-primary active'];
        }
        return presentButtonClass[$scope.btnState];
      };

      $scope.increase = function(val){
        $scope.presentationList[$scope.data.channel][$scope.data.presIndex].examProperties.Level += val;
      };

      $scope.decrease = function(val){
        $scope.presentationList[$scope.data.channel][$scope.data.presIndex].examProperties.Level -= val;
      };

      function setFrequencyAndLevel() {
        $scope.data.currentFrequency = $scope.presentationList[$scope.data.channel][$scope.data.presIndex].F;
      }

      $scope.chooseFrequency = function(index) {
        //if ($scope.data.presIndex !== index){
        $scope.data.presIndex = index;
        setFrequencyAndLevel();
        //}
      };

      $scope.switchEar = function(){
        if ($scope.data.channel === 'Left'){
          $scope.data.channel = 'Right';
        } else if($scope.data.channel === 'Right'){
          $scope.data.channel = 'Left';
        }
      };

      function stopPresentingFrequency(){
        cha.abortExams();
        $scope.btnState = 0;
      }

      function presentFrequency(){
        //console.log('present');
        $scope.btnState = 1;

        var waitFunction = function() {
          chaExams.wait.forReadyState()
            .then(function () {
              if ($scope.btnState === 1) {
                $scope.btnState = 0;
              }
            })
            .catch(function (err) {cha.errorHandler.main(err);});
        };

        cha.requestStatusBeforeExam()
          .then(function () {return cha.queueExam(examType, $scope.presentationList[$scope.data.channel][$scope.data.presIndex].examProperties);})
          .then(waitFunction)
          .catch(function (err) {cha.errorHandler.main(err);});
      }

      $scope.pressSoftwareButtonStart = presentFrequency;
      $scope.pressSoftwareButtonEnd = stopPresentingFrequency;
      $scope.toggleToneButton = function(){
        //console.log('toggletonebutton '+$scope.btnState);
        if ($scope.btnState === 1){
          stopPresentingFrequency();
        } else {
          presentFrequency();
        }
      };

      // start off in the first frequency
      $scope.chooseFrequency($scope.data.presIndex);

      cha.errorHandler.responseArea = function(err) {
        // Display error to user
        $scope.chaError = true;

        if (err.msg) {
          $scope.errorMessage = 'Error: ' + angular.toJson(err.msg);
        } else {
          $scope.errorMessage = 'Internal error, please restart the exam';
        }

        // make page submittable
        $scope.page.dm.isSubmittable = true;
      };

    });

});
