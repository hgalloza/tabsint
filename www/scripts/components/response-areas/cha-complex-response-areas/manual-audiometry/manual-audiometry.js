/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.manual-audiometry', [])
    .directive('manualAudiometryExam', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/cha-complex-response-areas/manual-audiometry/manual-audiometry.html',
        controller: 'ManualAudiometryExamCtrl'
      };
    })
    .controller('ManualAudiometryExamCtrl', function ($q, $scope, $timeout, cha, chaExams, chaResults, disk, examLogic,
                                                      logger, notifications, page, results) {

      var examType = page.dm.audiometryType || 'HughsonWestlake';         // remove 'cha' from prefix
      chaExams.setup(examType, page.dm.responseArea.examProperties); // sets chaExams.examProperties

      $scope.showLevelProgression = false;
      $scope.btnState = 0;
      var presentationIndex = 0;
      var defaultExamProperties = chaExams.examProperties || {};
      var defaultPresentationId = page.dm.id;
      var startTime = new Date();

      examLogic.submit = function(){ submitResults(); };
      page.dm.isSubmittable = true;

      // build presentation list
      if (angular.isUndefined(chaExams.storage.manualAudiometry) || angular.isUndefined(chaExams.storage.manualAudiometry.list)) {
        chaExams.storage.manualAudiometry = {
          list: {}
        };
        chaExams.storage.manualAudiometry.list.Left = angular.copy(page.dm.responseArea.presentationList);
        chaExams.storage.manualAudiometry.list.Right = angular.copy(page.dm.responseArea.presentationList);
        // build examProps from universal defaults and from pres-specific, build for left and right
        //console.log('DEBUG: presentationList: '+angular.toJson(chaExams.storage.manualAudiometry.list));
        //console.log('DEBUG: defaultExamProps: '+angular.toJson(defaultExamProperties));
        _.forEach(chaExams.storage.manualAudiometry.list.Left,function(pres){
          var examProps = _.extend({}, defaultExamProperties, pres, {PresentationMax: 1});
          examProps.OutputChannel = 'HPL0';
          if (examProps.id){delete examProps.id;}
          pres.examProperties = examProps;
        });
        _.forEach(chaExams.storage.manualAudiometry.list.Right,function(pres){
          var examProps = _.extend({}, defaultExamProperties, pres, {PresentationMax: 1});
          examProps.OutputChannel = 'HPR0';
          if (examProps.id){delete examProps.id;}
          pres.examProperties = examProps;
        });
      }
      //console.log('DEBUG: presentationList Left: '+angular.toJson(chaExams.storage.manualAudiometry.list['Left']));

      // view fields
      $scope.data = {
        currentFrequency: undefined,
        currentLevel: undefined,
        channel: undefined,
        levelUnits: undefined,
        minLevel: undefined,
        maxLevel: undefined
      };

      $scope.frequencyButtonClass = function(index){
        if (index === presentationIndex){
          return 'btn btn-default btn-success';
        } else {
          if (chaExams.storage.manualAudiometry.list[$scope.data.channel][index].Threshold !== undefined){
            return 'btn btn-default active';
          } else {
            return 'btn btn-default';
          }

        }
      };

      // Determine if it's a threshold or pass-fail exam
      if (angular.isDefined(page.dm.responseArea.responseType)){
          $scope.data.responseType = page.dm.responseArea.responseType;
      } else {
          $scope.data.responseType = 'threshold';
      }

      // set min and max levels, handling undefined and 0 cases
      if (angular.isDefined(page.dm.responseArea.minLevel)){
        $scope.data.minLevel = page.dm.responseArea.minLevel;
      } else {
        $scope.data.minLevel = -80;
      }
      if (angular.isDefined(page.dm.responseArea.maxLevel)){
        $scope.data.maxLevel = page.dm.responseArea.maxLevel;
      } else {
        $scope.data.maxLevel = 100;
      }

      // Starting channel.  Start with channel defined in exame props, or default left
      var channel = defaultExamProperties.OutputChannel || 'HPL0';

      $scope.btnText = ['Play Tones', 'Stop Tones'];
      if (channel.indexOf('R') > -1) {
        $scope.data.channel = 'Right';
      } else {
        $scope.data.channel = 'Left';
      }

      //window.presentationList = chaExams.storage.manualAudiometry.list;

      // set level units
      var levelUnits = defaultExamProperties.LevelUnits || chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.LevelUnits;
      if (angular.isUndefined(levelUnits)){
        logger.warn('no levelUnits defined for chaManualAudiometry, defaulting to dB HL');
        $scope.data.levelUnits = 'dB HL';
      } else {
        $scope.data.levelUnits = levelUnits;
      }

      $scope.playTonesButtonClass = function(){
        //console.log('playTonesButtonClass called');
        var presentButtonClass;
        if ($scope.data.channel === 'Right'){
          presentButtonClass = ['btn btn-danger', 'btn btn-danger active'];
        } else {
          presentButtonClass = ['btn btn-primary','btn btn-primary active'];
        }
        return presentButtonClass[$scope.btnState];
      };

      //Check that data.currentLevel falls within the data.minLevel and
      //data.maxLevel bounds - this isn't a thorough check, as the headset may
      //be calibrated with tighter bounds at particular frequencies
      function levelBoundsCheck(){
        if ($scope.data.currentLevel > $scope.data.maxLevel) {
          $scope.data.currentLevel = $scope.data.maxLevel;
        }
        else if($scope.data.currentLevel < $scope.data.minLevel) {
          $scope.data.currentLevel = $scope.data.minLevel;
        }
      }

      $scope.increase = function(val){
        $scope.data.currentLevel += val;
        levelBoundsCheck();
      };

      $scope.decrease = function(val){
        $scope.data.currentLevel -= val;
        levelBoundsCheck();
      };

      function setFrequencyAndLevel() {
        $scope.data.currentFrequency = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].F;
        // switching to a new frequency.  If we already used this frequency, then there should be a threshold, or a levelprogression (use the last used level)
        if ($scope.data.responseType === 'threshold' && angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold)) {
          $scope.data.currentLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold;
        } else if (angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L && chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.length > 0)){
          $scope.data.currentLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.slice(-1)[0];
        } else {
          // no previous use, look for Lstart, otherwise default to 40
          $scope.data.currentLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Lstart || defaultExamProperties.Lstart || 40; // default is 40
          levelBoundsCheck();
          //only check bounds if not setting based on a previously set threshold.
          //Not sure how you could have gotten a threshold outside of the bounds,
          //but if you did, don't want to force it to the bounds
        }
      }

      $scope.chooseFrequency = function(index) {
        //if (presentationIndex !== index){
        presentationIndex = index;
        setFrequencyAndLevel();
        if (!angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L)){
          $scope.showLevelProgression = false;
        } else {
          updateLevelProgressionPlot();
        }
        //}
      };

      function nextFrequency(){
        if (presentationIndex < (chaExams.storage.manualAudiometry.list[$scope.data.channel].length - 1) ) {
          $scope.chooseFrequency(presentationIndex + 1);
        }
      }

      $scope.setThreshold = function() {
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = $scope.data.currentLevel;
        nextFrequency();
      };

      $scope.setPass = function(){
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = 'P';
        nextFrequency();
      };

      $scope.setFail = function(){
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = 'F';
        nextFrequency();
      };

      $scope.switchEar = function(){
        if ($scope.data.channel === 'Left'){
          $scope.data.channel = 'Right';
        } else if($scope.data.channel === 'Right'){
          $scope.data.channel = 'Left';
        }
        presentationIndex = 0;
        updateLevelProgressionPlot();
      };

      function stopPresentingFrequency(){
        //console.log('stop');
        cha.abortExams();
        updateLevelProgressionPlot();
        $scope.btnState = 0;
      }

      function presentFrequency(){
        //console.log('present');
        $scope.btnState = 1;

        if (!angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L)){
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L = [];
        }
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.push($scope.data.currentLevel);
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.Lstart = $scope.data.currentLevel;

        var waitFunction = function() {
          chaExams.wait.forReadyState()
            .then(cha.requestResults()) //try to add a step where we check if things worked out.
            .then(function () {
              if ($scope.btnState === 1) {
                $scope.btnState = 0;
                updateLevelProgressionPlot();
              }
            })
            .catch(function (err) {cha.errorHandler.main(err);});
        };

        cha.requestStatusBeforeExam()
          .then(function () {return cha.queueExam('HughsonWestlake', chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties);})
          .then(waitFunction)
          .catch(function (err) {cha.errorHandler.main(err);});
      }

      $scope.pressSoftwareButtonStart = presentFrequency;
      $scope.pressSoftwareButtonEnd = stopPresentingFrequency;
      $scope.toggleToneButton = function(){
        //console.log('toggletonebutton '+$scope.btnState);
        if ($scope.btnState === 1){
          stopPresentingFrequency();
        } else {
          presentFrequency();
        }
      };

      function submitResults(){
        var earSide = '';

        function submitSingleResult(pres, index) {
          var presId;
          if (angular.isDefined(pres.id)){
            presId = earSide.toLowerCase() + '_' + pres.id;
          } else {
            presId = defaultPresentationId+earSide+'_HW'+pres.examProperties.F;
          }

          if (page.dm.responseArea.onlySubmitFrequenciesTested){
            if (angular.isUndefined(pres.Threshold)){
              return;
            }
          }

          page.result = results.default(page.dm);
          page.result = $.extend({}, page.result, {
            examType: examType,
            presentationId: presId,
            responseStartTime: startTime,
            chaInfo: chaExams.getChaInfo(),
            ResponseType: $scope.data.responseType,
            presentationIndex: index,
            ResultType: (angular.isDefined(pres.Threshold))?$scope.data.responseType:'Skipped',
            Threshold: (angular.isDefined(pres.Threshold))? pres.Threshold : Number.NaN,
            RetSPL: 0,
            Units: defaultExamProperties.LevelUnits || 'dB HL',
            L: pres.L,
            response: (angular.isDefined(pres.Threshold))? pres.Threshold : Number.NaN,
            examProperties: pres.examProperties
          } );
          logger.debug('CHA pushing maunal audiometry presentation result onto stack: ' +  angular.toJson(page.result));
          examLogic.pushResults();
        }

        earSide = 'Left';
        _.forEach(chaExams.storage.manualAudiometry.list.Left, submitSingleResult);
        earSide = 'Right';
        _.forEach(chaExams.storage.manualAudiometry.list.Right, submitSingleResult);

        page.result = {
          presentationId: 'ManualAudiometry',
          responseStartTime: startTime,
          response: 'complete'
        };

        // clear storage
        delete chaExams.storage.manualAudiometry;

        // push results
        examLogic.submit = examLogic.submitDefault;
        examLogic.submit();

      }

      function updateLevelProgressionPlot() {
        // update plot
        $scope.showLevelProgression = false;

        if (angular.isUndefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L)  ||
                chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.length <= 0){return;}

        var tmpResult = {
          L: chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L,
          Units: defaultExamProperties.LevelUnits || 'dB HL'
        } ;
        var plotData = chaResults.createLevelProgressionData(tmpResult);
        plotData.maxY = 120;
        plotData.minY = -10;
        plotData.minX = 10;
        plotData.plotHeight = 300;
        plotData.plotWidth = 300;
        $scope.levelProgressionData = plotData;
        //console.log('level progressionData: '+angular.toJson($scope.levelProgressionData));
        $timeout(function(){$scope.showLevelProgression = true;},20);
      }

      // start off in the first frequency
      $scope.chooseFrequency(presentationIndex);

      cha.errorHandler.responseArea = function(err) {
        // Display error to user
        $scope.chaError = true;

        if (err.msg) {
          $scope.errorMessage = 'Error: ' + angular.toJson(err.msg);
        } else {
          $scope.errorMessage = 'Internal error, please restart the exam';
        }

        // make page submittable
        $scope.page.dm.isSubmittable = true;
      };

      cha.errorHandler.responseArea = function(err) {
        // Display error to user with popup notifications
        console.log("manual-audiometry error; popping up notification to user.");
        $scope.chaError = true;

        if (err.msg) {
          //special error handling for exceeding maximum calibrated level.
          if(err.code === 10){
            notifications.alert("The target amplitude at this frequency is outside the calibrated bounds for this headset.");
            //reset button state
            $scope.btnState = 0;
            //reset the last entry on the list. No need to store something out of range.
            chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.pop();
          } else if(err.code !== 1){
            //ignore code 1, which is an error from trying to ask the WAHTS to do something while it's busy
            notifications.alert(err.msg);
          }
        } else {
          notifications.alert('Internal error, please restart the exam');
        }
      };

    });

});
