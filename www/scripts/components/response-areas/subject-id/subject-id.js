define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.subject-id', [])

    .controller('SubjectIdResponseAreaCtrl', function ($scope, adminLogic, results, page) {
      $scope.page = page;

      $scope.$watch('page.result.response', function () {
          //submission logic
          if (page.dm.responseArea.responseRequired === true && (page.result.response === undefined || page.result.response === '')) {
              $scope.page.dm.isSubmittable = false;
          } else {
              $scope.page.dm.isSubmittable = true;
          }
      });

      $scope.storeResponse = function() {
        results.current.subjectId = page.result.response;  // store in the top level exam subject-id field
      };

      $scope.generate = function() {
        // Generate a random n-digit ascii/letter-number
        var len = 5;
        var text = '';
        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        page.result.response = text;       // store in the current response
        $scope.storeResponse();
      };

    });
});
