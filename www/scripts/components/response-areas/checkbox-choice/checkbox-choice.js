define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.checkbox-choice', [])   

  .controller('CheckboxChoiceController', function ($scope, page) {

    $scope.label = $scope.choice.text || $scope.choice.id;

    // container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {'padding-bottom':page.dm.responseArea.verticalSpacing});
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        'padding-right':page.dm.responseArea.horizontalSpacing/2,
        'padding-left':page.dm.responseArea.horizontalSpacing/2
      });
    }

    $scope.saveListToResponse([]);

    $scope.chosen = function () {
      return (_.includes($scope.responseAsList(), $scope.choice.id));
    };
    $scope.btnClass = function () {
      var btnClass = 'btn btn-block ';
      if (page.dm.responseArea.buttonScheme === "markIncorrect") {
        if ($scope.chosen()) {
          btnClass += 'btn-danger';
        } else {
          btnClass += 'btn-success';
        }
      }
      else if (page.dm.responseArea.buttonScheme === "markCorrect") {
        if ($scope.chosen()) {
          btnClass += 'btn-success';
        } else {
          btnClass += 'btn-danger';
        }
      }
      else {
        if ($scope.chosen()) {
          btnClass += 'btn-default active ';
        } else {
          btnClass += 'btn-default ';
        }
      }
      return btnClass;
    };
    $scope.choose = function () {
      // toggle chosen/unchosen.
      if ($scope.chosen()) {
        $scope.saveListToResponse(_.without($scope.responseAsList(), $scope.choice.id)); //toggle off.
      } else {
        $scope.saveListToResponse(_.union($scope.responseAsList(), [$scope.choice.id])); //toggle off.
      }
    };
  });
});