define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.external-app', [])

    .controller('ExternalAppResponseAreaCtrl', function ($scope, disk, gettextCatalog, logger, notifications, page, tabsintNative) {
      $scope.externalApp = {
        dataOut: page.dm.responseArea.dataOut,
        dataIn: 'waiting',
        appName: page.dm.responseArea.appName
      };

      // set the submittable logic to always be true
      page.dm.isSubmittable = true;

      function sendExternalData() {
        if (page.dm.responseArea.appName) {
          var d = {
            message: page.dm.responseArea.dataOut.message || 'Test InterApp Message',
            data: page.dm.responseArea.dataOut.data || 'Test InterApp Data'
          };
          page.result.sentToExternalApp = d;
          page.result.externalAppName = page.dm.responseArea.appName;
          tabsintNative.sendExternalData(
            function(){logger.info('Successfully sent data ' + JSON.stringify(d) + ' to external app ' + page.dm.responseArea.appName + '.')},
            function(e){
              logger.error('Sending data ' + JSON.stringify(d) + ' to external app ' + page.dm.responseArea.appName + '.  Error: ' + JSON.stringify(e));
              var strmsg = JSON.stringify(e);
              if (strmsg.indexOf('No Activity found to handle Intent')) {
                notifications.alert(gettextCatalog.getString('Warning:  Error sending data to external app ') + disk.interApp.appName + '.  '+gettextCatalog.getString('No app by that name can be found on this device.  Please alert administrator.'));
              }
            },
            page.dm.responseArea.appName,
            d
          );
        }
      }

      function handleExternalAppData(d) {
        logger.info('Received data from app ' + page.dm.responseArea.appName + '.  Data: ' + JSON.stringify(d));
        $scope.externalApp.dataIn = d;
        page.result.response = d;
        page.result.receivedFromExternalApp = d;
      }

      tabsintNative.setExternalDataHandler(
        handleExternalAppData,
        function(e) {logger.error('in interApp data handler: ' + JSON.stringify(e))}
      );

      sendExternalData();

    });
 });
