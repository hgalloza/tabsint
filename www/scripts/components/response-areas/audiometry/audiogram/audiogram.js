/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.audiogram', [])
    .directive('audiogramPlot', function () {
      return {
        restrict: 'E',
        template: '<div id="audiogramPlotWindow"></div>',
        controller: 'AudiogramPlotCtrl',
        scope: {
          data: '='
        }
      };
    })

    .controller('AudiogramPlotCtrl', function($scope, d3Services) {
      d3Services.audiogramPlot('#audiogramPlotWindow', $scope.data);
    })
    .controller('AudiogramCtrl', function ($scope, page, chaResults) {

        page.dm.hideProgressbar = true;
        page.dm.isSubmittable = true;

        // Update page view, get rid of instructions
        $scope.page.dm.title = 'Results';
        $scope.page.dm.questionMainText = '';
        $scope.page.dm.instructionText = '';

        $scope.audiogramData = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[1];

        if ($scope.audiogramData){
          $scope.showAudiogram = true;
        }
      });

});
