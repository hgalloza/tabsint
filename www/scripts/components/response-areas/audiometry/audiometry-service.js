/* jshint bitwise: false */
/* globals ChaWrap: false */

/**
 * Created by mls on 7/11/2014.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.audiometry-service', [])
    .factory('chaAudiometryService', function ($timeout, $q, page, cha, chaExams, logger) {

      var api = {
        reset: undefined,
        logButtonPress: undefined,
        buttonPressCount: 0,
        buttonPressTimes: [],
        simulateTimer: undefined,
        examDone: false
      };

      api.reset = function(){
        api.buttonPressCount = 0;
        api.examDone = false;
        api.startTime = new Date();
        api.buttonPressTimes = [];
        if (api.simulateTimer){
          $timeout.cancel(api.simulateTimer);
        }
      };

      api.logButtonPress = function() {
        api.buttonPressCount ++;
        api.buttonPressTimes.push((new Date()) - api.startTime);
      };

      // Results structure:
      var audiometryResults = {
        ResultTypeThreshold : ['Threshold', 'Hearing potentially outside measurable range', 'Did not converge'],
        ResultTypeScreener : ['Pass', 'Unused', 'Fail'],
        Units: ['dB SPL', 'dB HL', 'Hz']
      };

      // Results processing - results will be available aftwarwards on page.result
      api.processResults = function(results){
        if (api.simulateTimer){
          $timeout.cancel(api.simulateTimer);
        }

        // make page unsubmittable while processing
        page.dm.isSubmittable = false;

        // convert integers to string results, save exam results to structure
        results.chaInfo = chaExams.getChaInfo();
        results.examProperties = chaExams.examProperties;
        results.examType = chaExams.examType;

        results.Units = audiometryResults.Units[results.Units];
        results.ResultTypeCode = results.ResultType;

        if (results.ResultType >= 0 && results.ResultType <= 2) {
          if (results.examProperties.Screener){
            results.ResultType = audiometryResults.ResultTypeScreener[results.ResultType];
          } else {
            results.ResultType = audiometryResults.ResultTypeThreshold[results.ResultType];
          }
        } else {
          results.ResultType = 'Unknown result type: '+results.ResultType;
        }

        results.buttonPressTimes = api.buttonPressTimes;

        page.result = $.extend({}, page.result, results);

        // set 'response' field to Threshold, or if no threshold, display the result Type
        if (results.Threshold !== angular.undefined){
          page.result.response = results.Threshold;
        } else {
          page.result.response = results.ResultType;
        }

      };

      return api;
    });


});
