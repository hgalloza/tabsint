/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.software-button', [])
    .directive('softwareButton', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/audiometry/software-button/software-button.html',
        controller: 'SoftwareButtonCtrl'
      };
    })

    .controller('SoftwareButtonCtrl', function ($scope, $timeout, cha, chaExams, chaCheck, chaAudiometryService, gettextCatalog) {
      var btnClass = ['btn btn-cha', 'btn btn-cha active'];  // defaults
      $scope.btnClass = btnClass[0];
      var simulateDelay;

      function simulateResponses(){
        if (chaExams.state === 'exam' ) {
          $scope.pressSoftwareButtonStart();
          chaAudiometryService.simulateTimer = $timeout(simulateResponses, simulateDelay);
        }
      }

      function tapSoftwareButton() {
        chaAudiometryService.logButtonPress();
        if (!chaAudiometryService.examDone) {
          $scope.btnClass = btnClass[1];
          $scope.btnState = 1;
          cha.setSoftwareButtonState(1);
        }

        $timeout(function () {
          $scope.btnClass = btnClass[0];
          $scope.btnState = 0;
        }, 150);

        $timeout(function () {
          if (!chaAudiometryService.examDone) {
            cha.setSoftwareButtonState(0);
          }
        }, 20);
      }

      function toggleSoftwareButton () {
        chaAudiometryService.logButtonPress();
        $scope.btnState = $scope.btnState ^ 1;      // ^ is the XOR operator - will always return the opposite
        $scope.btnClass = btnClass[$scope.btnState];
        cha.setSoftwareButtonState($scope.btnState);
      }

      // set up the software button
      if (chaExams.examProperties.UseSoftwareButton) {
        $scope.btnState = 0;    // software button set to false initially in all cases;

        // Set Exam specific software button properties here:
        if (chaCheck.isHughsonWestlakeExam(chaExams.examType)) {
          $scope.btnText = [gettextCatalog.getString('Tap this button when you hear a set of sounds'), gettextCatalog.getString('Tap this button when you hear a set of sounds')];
          $scope.pressSoftwareButtonStart = tapSoftwareButton;
          $scope.pressSoftwareButtonEnd = function(){};
        }

        if (chaCheck.isBekesyExam(chaExams.examType)) {
          $scope.btnText = ['Press and Hold', 'Press and Hold'];
          $scope.pressSoftwareButtonStart = toggleSoftwareButton;
          $scope.pressSoftwareButtonEnd = toggleSoftwareButton;
        }

        if (cha.simulateResponses){
          var TonePulseNumber = chaExams.examProperties.TonePulseNumber || 3;
          var ToneRepetitionInterval = chaExams.examProperties.ToneRepetitionInterval || 450;
          var MinISI = chaExams.examProperties.MinISI || 600;
          var MaxISI = chaExams.examProperties.MaxISI || 1000;
          simulateDelay = TonePulseNumber*(ToneRepetitionInterval+ 1/2 *(MinISI + MaxISI));
          simulateDelay = 1500;
          chaAudiometryService.simulateTimer = $timeout(simulateResponses, simulateDelay);
        }
      }

    });
});
