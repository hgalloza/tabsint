define(['angular'], function (angular) {
    'use strict';

    angular.module('tabsint.components.response-areas.cha.crm', [])
        .directive('crmExam', function () {
            return {
                restrict: 'E',
                templateUrl: 'scripts/components/response-areas/crm/crm.html',
                controller: 'CrmExamCtrl'
            };
        })
        .controller('CrmExamCtrl', function ($interval, $q, $scope, $timeout, cha, chaExams, examLogic, logger, page,  results) {
            var timerInterval;
            var maxResponseTime;
            if (angular.isDefined(page.dm.responseArea.examProperties)) {
                maxResponseTime = angular.isDefined(page.dm.responseArea.examProperties.MaxResponseTime) ? page.dm.responseArea.examProperties.MaxResponseTime : 8;
            } else {
                maxResponseTime = 8;
            }
            var conditionPresentations;
            if (angular.isDefined(page.dm.responseArea.examProperties)) {
                conditionPresentations = angular.isDefined(page.dm.responseArea.examProperties.ConditionPresentations) ? page.dm.responseArea.examProperties.ConditionPresentations : [1,5];
            } else {
                conditionPresentations = [1,5];
            }
            var feedback = angular.isDefined(page.dm.responseArea.feedback)? page.dm.responseArea.feedback : true;
//            var feedbackDelay = angular.isDefined(page.dm.responseArea.feedbackDelay)? page.dm.responseArea.feedbackDelay : 1000;
            var colorSubmissionMap = {Incorrect: 0, Blue: 1, Red: 2, White: 3, Green: 4};
            var i; // 0 or 1 integer to keep track of whether a pause in the exam has been requested

            $scope.n = [1, 2, 3, 4, 5, 6, 7, 8];
            $scope.color = ['Blue', 'Red', 'White', 'Green'];
            $scope.space = [' '];
            $scope.btnStyle = {
                Blue: {background: 'rgb(119, 179, 255)'},
                Red: {background: 'rgb(248, 132, 132)'},
                White: {background: 'white'},
                Green: {background: 'rgb(122, 202, 122)'}
            };
            $scope.wahtsState = 'Not Ready to Submit Results';

            // Container spacing
            $scope.spacing = {};
            if (page.dm.responseArea.verticalSpacing) {
                _.extend($scope.spacing, {'padding-bottom':page.dm.responseArea.verticalSpacing});
            }
            if (page.dm.responseArea.horizontalSpacing) {
                _.extend($scope.spacing, {
                    'padding-right':page.dm.responseArea.horizontalSpacing/2,
                    'padding-left':page.dm.responseArea.horizontalSpacing/2
                });
            }

            /**
             * When user selects response:
             * Grade response, display correct response for desired duration,
             * submit result to the WHATS, reset results and move on to
             * next presentation.
             */
            $scope.crmchoose = function (btncolor, number) {
                // Temporarily disable buttons to make sure we don't call this function twice in a row
                $scope.btnsDisabled = true;

                stopTimer();
                if (feedback === true) {
                    $scope.btnShowCorrect = true; // Show correct answer
                }
                $scope.response = btncolor + ' ' + number;
                // If WAHTS is already waiting for results, submit them now.
                // Otherwise, store the response and submit when the WAHTS is ready.
                if ($scope.wahtsState === 'Ready to Submit Results') {
                    submitResponse($scope.response.slice(0, -2), parseInt($scope.response.slice(-1)));
                    $scope.wahtsState = 'Not Ready to Submit Results';
                } else {
                    $scope.wahtsState = 'Response Ready to Submit to WAHTS';
                }
            };

            /**
             * Utility function to pause exam upon user request.
             * Exam only pauses after the current presentation has been submitted.
             * @returns {*}
             */
            $scope.pause = function() {
                $scope.state = 'paused'; // Pause exam after this presentation
            };

            /**
             * Utility function to resume exam upon user request
             * @returns {*}
             */
            $scope.play = function() {
                if ($scope.state === 'paused') {
                    $scope.state = 'playing'; // Resume exam
                } else if ($scope.state === 'paused - waiting for resume request') {
                    $scope.state = 'playing'; // Resume exam
                    return cha.examSubmission('CRM$Submission', {Color: 1, Number: 1, Pause: 0})
                        .then(getPresentationInfo)
                        .catch(function (err) {
                            cha.errorHandler.main(err);
                        });
                }
            };

            /**
             * Function to run at exam initiation - happens when the responseArea changes
             */
            function newCrmExam() {
                var j;
                $scope.btnsDisabled = true;           // Only accept answer when the WHATS is expecting one
                $scope.state = 'playing';             //Is exam playing or paused?
                $scope.presentationCount = 0;         // Count number of presentation displayed
                $scope.nPresentations = 0;            // Total number of presentations
                for (j=1; j<= conditionPresentations.length; j+=2) {
                    $scope.nPresentations += conditionPresentations[j];
                }
                page.dm.isSubmittable = false;   // Make the page not submittable
                page.result = {};
                getPresentationInfo();
            }

            /**
             * Request results from WHATS and handle what to do next
             * @returns {*}
             */
            function getPresentationInfo() {
                if ($scope.state !== 'paused - waiting for resume request') {
                    return cha.requestResults()
                        .then(function (result) {
                            presentationHandler(result);
                        });
                }
            }
            /**
             * Wait function to try and reduce delay between end of sound and WAHTS state ready.
             * Didn't work.
             * @returns promise
             */
//            function waitForReadyState() {
//                var deferredExam = $q.defer();
//                var polling = setInterval(function () {
//                    return cha.requestResults()
//                        .then(function(resultsFromWahts) {
//                            if (resultsFromWahts.State === 1) {
//                                clearInterval(polling);
//                                $scope.btnsDisabled = false;
//                                // start maxResponseTimer
//                                startTimer();
//                                deferredExam.resolve();
//                            }
//                        }, function (err) {
//                            clearInterval(polling);
//                            deferredExam.reject(err);
//                        });
//                }, 500);
//                return deferredExam.promise;
//            }

            /**
             * Presentation handler. What to do with the results received from the WHATS.
             * @param resultFromWahts
             * @returns {*}
             */
            function presentationHandler(resultFromWahts){
                // If this is the first time going through this function for a presentation, initialize results,
                // enable buttons and start timer
                if (_.isUndefined(page.result.correctColor)) {
                    // Timeout to enable button and start time approximately when the sound ends. There is too much delay
                    // with the WAHTS communication to do it when it is ready to receive results.
                    $timeout(function() {
                        $scope.btnsDisabled = false;
                        // start maxResponseTimer
                        startTimer();
                    },1500);
                    // Increment presentation count
                    if ($scope.presentationCount<$scope.nPresentations) {
                        $scope.presentationCount += 1;
                    }
                    $scope.btnShowCorrect = false;
                    page.result = $.extend(page.result, {
                        conditionCode: resultFromWahts.ConditionCode,
                        correctColor: (_.invert(colorSubmissionMap))[resultFromWahts.CorrectColor],
                        correctNumber: resultFromWahts.CorrectNumber,
                        wavFileName: resultFromWahts.WavFileName,
                        responseTime: resultFromWahts.ResponseTime,
                        correctPercent: resultFromWahts.CorrectPercent,
                        chaInfo: chaExams.getChaInfo()
                    });
                    $scope.response = undefined;
                    $scope.correctColor = page.result.correctColor.toLowerCase();
                    $scope.correctNumber =  page.result.correctNumber;
                }

                // WAHTS results handling
                if (resultFromWahts.State === 0 || resultFromWahts.State === 3) {// State 0 = exam still running, State 3 = LOADING
                    $timeout(getPresentationInfo,100);
                } else if (resultFromWahts.State === 1) {  // State 1 =  waiting for result
                    // If subject has already submitted response, then submit it.
                    // Otherwise, record that the WAHTS is ready to receive results.
                    if ($scope.wahtsState === 'Response Ready to Submit to WAHTS') {
                        submitResponse($scope.response.slice(0, -2), parseInt($scope.response.slice(-1)));
                        $scope.wahtsState = 'Not Ready to Submit Results';
                    } else {
                        $scope.wahtsState = 'Ready to Submit Results';
                    }
                } else if (resultFromWahts.State === 2) {    // State 2 = End of exam
                    page.dm.isSubmittable = true;   // make the page submittable
                    page.result = {};
                    page.result = $.extend({}, results.default(page.dm), {
                        correctPercent: resultFromWahts.CorrectPercent,
                        chaInfo: chaExams.getChaInfo()
                    });
                    examLogic.submit();
                } else if (resultFromWahts.State === 4) { // State 4 = PAUSED
                    return cha.examSubmission('CRM$Submission', {Color: 1, Number: 1, Pause: 0})
                        .then($timeout(getPresentationInfo,2000)); // Use a long timeout to minimize burden on WAHTS
                }
            }

            /**
             * Push result to TabSINT, submit user response to the WHATS and move to next presentation
             * @param btncolor
             * @param number
             * @returns {*}
             */
            function submitResponse(btncolor, number) {
                if (btncolor === 'Incorrect') {
                    page.result.correct = false;
                    page.result.response = "Maximum number of seconds exceeded";
                } else {
                    page.result.response = $scope.response;
                    page.result.correct = ($scope.correctColor === $scope.response.slice(0,-2).toLowerCase()) && ($scope.correctNumber.toString() === $scope.response.slice(-1));
                }
                examLogic.pushResults();
                if ($scope.state==='paused') {
                    i=1;
                    $scope.state='paused - waiting for resume request';
                    $timeout(function() {$scope.btnShowCorrect = false;}, 2000);
                } else if ($scope.state==='playing') {
                    i=0;
                }
                return cha.examSubmission('CRM$Submission', {Color: colorSubmissionMap[btncolor], Number: number, Pause: i})
                    .then(function() {
                        var q = $q.defer();
                        // Reset results
                        page.result = {};
                        page.result = $.extend(page.result, results.default(page.dm), {
                            examType: chaExams.examType,
                            examProperties: chaExams.examProperties,
                            responseStartTime: new Date()
                        });
                        q.resolve();
                        return q.promise;
                    })
                    .then(getPresentationInfo)
                    .catch(function(err) {cha.errorHandler.main(err);});
            }

            /**
             * If the user takes too long to respond, the incorrect Z0 response is submitted to the WHATS,
             * then next presentation resumes.
             */
            function startTimer() {
                $scope.timeCounter = 0;
                timerInterval = $interval(function() {
                    $scope.timeCounter += 0.1;
                    if ($scope.timeCounter > maxResponseTime) {
                        $scope.crmchoose('Incorrect',0);
                    }
                }, 100);
            }

            /**
             * Stops timer when response is selected or maximum response time is reached.
             */
            function stopTimer() {
                $interval.cancel(timerInterval);
            }

            // Start new Three Digit Exam
            newCrmExam();

        });
});