define(['angular'], function (angular) {
    'use strict';

    angular.module('tabsint.components.response-areas.button-grid', [])

        .controller('ButtonGridResponseAreaCtrl', function ($scope, $timeout, examLogic, page) {

            function update() {
                $scope.rows = page.dm.responseArea.rows;   // dump rows into its own variable
            }

            $scope.gradeResponse = false;
            $scope.showCorrect = false;
            if (angular.isUndefined(page.dm.responseArea.correct)) {
                page.dm.responseArea.rows.forEach(function(item) {
                    item.choices.forEach(function(choice) {
                        if (angular.isDefined (choice.correct)) {
                            if (choice.correct === true) {
                                page.dm.responseArea.correct = choice.id;
                            }
                        }
                    });
                });
            }

            // set the button disabled for x msecs where x is specified by page.dm.delayEnable, HG 12/7/15
            $scope.buttonDisabled = true;
            $timeout(function() {
                $scope.buttonDisabled = false;
            }, page.dm.responseArea.delayEnable);


            // callback for page.dm
            if (page.dm.responseArea.feedback !== angular.undefined){
                page.dm.showFeedback = function(){
                    if (page.dm.responseArea.feedback === 'gradeResponse'){
                        $scope.gradeResponse = true;
                    }
                    else if (page.dm.responseArea.feedback === 'showCorrect') {
                        $scope.showCorrect = true;
                    }
                };
            }

            update();
        });
});
