
/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.disk', [])

    .factory('disk', function($localStorage){

    // persistent data store
    var disk = $localStorage.$default({
      debugMode: false,
      externalMode: false,
      pin: 7114,
      tabletGain: undefined,
      disableLogs: false,
      disableVolume: false,
      appDeveloperMode: false,
      appDeveloperModeCount : 0,
      autoUpload: false,
      uploadSummary: [],
      suppressAlerts: false,
      showUploadSummary: true,
			resultsMode:0,
			preventUploads:false,
			preventExports:false,
      reloadingBrowser: false,
      downloadInProgress: false,
      lastReleaseCheck: undefined,
      validateProtocols: false,
      completedResults: [],
      currentResults: undefined,
      tabletLocation: {
        latitude: undefined,
        longitude: undefined,
        accuracy: undefined
      },
      gitlab: {
        repos: [],
        useTagsOnly: true,
        useSeperateResultsRepo: false
      },
      protocol: {},         // metadata of the currently active protocol
      protocols: [],        // metadata of the protocols currently downloaded and available
      mediaRepos: [],       // metadata of the media repos currently downloaded and available
      server: undefined,    // one of 'tabsintServer', 'localServer', 'gitlab'
      output: undefined,    // one of 'tabsintServer', 'localServer', 'gitlab'
      servers: {
        tabsintServer: {
          url: undefined,
          username: undefined,
          password: undefined
        },
        localServer: {
          resultsDir: 'tabsint-results'
        },
        gitlab: {
          host: undefined,
          token: undefined,
          namespace: undefined,
          resultsGroup: undefined,
          resultsRepo: 'results'
        }
      },
      plugins: {},
      headset: undefined,
      language: undefined,
      interApp: {
        appName: undefined,
        dataIn: undefined,
        dataOut: undefined
      },
      init: true //when set to true, this is the initial opening of the app and disclaimer should be displayed
    });

    return disk;
  });
});
