/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */



define(['test-dep', 'app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Disk', function () {

    var disk;


    beforeEach(inject(function ($injector,  _disk_) {
      disk = _disk_;
    }));

    it('should have some defaults defined.', function () {
      expect(disk.debugMode).toBeFalsy();
      expect(disk.appDeveloperMode).toBeFalsy();
      expect(disk.autoUpload).toBeFalsy();
      expect(disk.gitlab.useTagsOnly).toBeTruthy();
    });

  });
});
