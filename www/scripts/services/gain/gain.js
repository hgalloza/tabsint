/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
    'use strict';

  angular.module('tabsint.services.gain', [])
    .factory('gain', function (devices, disk) {

      /*
       Loads in devices for access to devices.model, which determines the gain to apply to wav files
       */
      var gain = {
        reset: undefined
      };

      var tabletGain;

      var tabletGains = {
        Browser: -1,
        Nexus7: 0,
        SamsungTabE: -8.56
      };

      /**
       * Load the gain factor on app load
       * Only utilized the first time tabsint is loaded
       */
      gain.load = function() {
        if (!disk.tabletGain) {
          gain.reset();
        }
      };

      /**
       * Reset the custom gain parameter for the current tablet
       */
      gain.reset = function() {
        disk.tabletGain = undefined;

        if (Object.keys(tabletGains).indexOf(devices.model) > -1) {
          disk.tabletGain = tabletGains[devices.model];
        } else if (devices.model === 'SAMSUNG-SM-T377A') {
          disk.tabletGain = tabletGains.SamsungTabE;
        } else {
          disk.tabletGain = tabletGains.Nexus7;
        }
      };

      return gain;
    });
});
