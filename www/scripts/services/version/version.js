/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.version', [])
    .factory('version', function (json, logger, paths) {

      var version = {};

      version.load = function() {
        try {
          version.dm = json.load(paths.www('version.json'));
          logger.param.version = version.dm.tabsint;
          logger.info('Version object processed: ' + angular.toJson(version.dm));
        } catch(e) {
          console.log(e);   // for debugging
          logger.error('Version failed during load with error: ' + angular.toJson(e));
        }
      };

      return version;
    });

});
