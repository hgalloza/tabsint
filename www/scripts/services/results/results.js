/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.results', [])

    .factory('results', function ($cordovaFile, $q, app, config, csv, devices, disk, encryptResults, file, gettextCatalog, gitlab,
                                  json, logExport, logger, localServer, networkModel, notifications, paths, pm, sqLite,
                                  tabletLocation, tabsintServer, tasks, version) {

      var results = {};

      results.dm = {
        queuedResultsWarnLength: 10,
        uploadSummaryLength: 30,
        maxStoredResults: 50
      };

      // container to hold the current exam results
      results.current = undefined;
      // container to hold results for 'Completed Tests' results view
      results.completedTests = undefined;
      // container to hold result for the single result view
      results.singleResultView = undefined;

      results.default = function(pg) {
        return {
          presentationId: pg.id,
          response: undefined,
          correct: undefined,
          isSkipped: false,
          changedFields: pg.changedFields,
          responseArea: pg.responseArea ? pg.responseArea.type : undefined,
          page: {
            wavfiles: pg.wavfiles,
            chaWavFiles: pg.chaWavFiles,
            image: pg.image,
            video: pg.video,
            responseArea: pg.responseArea
          }
        };
      };

      /**
       * Generate a standardized filename across results
       * @param  {object} [result] - result object. If no result object provided, then it will use the current datetime
       * @param  {object} [suffix] - optional suffix to append
       * @return {string}        string filename
       */
      results.filename = function(result, suffix) {
        var dateTime, filename;

        // get test date time (time the test was started)
        if (result.testDateTime) {
          dateTime = result.testDateTime.replace(':', '-').replace(':', '-').split('.')[0];
        } else {
          dateTime = (new Date()).toJSON().replace(':', '-').replace(':', '-').split('.')[0];
        }

        // if resultFilename defined, use as prefix, otherwise use the device uuid
        if (result.resultFilename) {
          filename = result.resultFilename + '.' + dateTime;
        } else {
          filename = devices.shortUUID + '.' + dateTime;
        }

        // add any suffix asked for
        if (suffix) {
          filename = filename + suffix;
        }

        return filename;
      };

      results.getCurrentResults = function() {
        results.current = {

          siteId: disk.protocol.siteId || null,
          protocolId: disk.protocol.id || null,
          protocolName: disk.protocol.name,
          protocolHash: disk.protocol.hash,
          qrString: undefined,
          testDateTime: (new Date()).toJSON(),
          elapsedTime: undefined,
          subjectId: undefined,
          testResults: {
            protocol: angular.copy(disk.protocol),
            responses: [],
            softwareVersion: {
              version: version.dm.tabsint,
              date: version.dm.date,
              rev: version.dm.rev
            },
            cordovaPlugins: config.cordovaPlugins,
            tabsintPlugins: config.tabsintPlugins,
            buildName: config.build,
            platform: devices.platform,
            network: null,   // this gets set when uploaded, if uploaded
            tabletUUID: devices.UUID,
            tabletModel: devices.model,
            tabletLocation: tabletLocation.position,
            partialResults: undefined,
            headset: pm.root.headset || 'None',
            calibrationVersion: {
              audioProfileVersion: pm.root._audioProfileVersion,
              calibrationPySVNRevision: pm.root._calibrationPySVNRevision,
              calibrationPyManualReleaseDate: pm.root._calibrationPyManualReleaseDate
            },
            isAdminMode: disk.debugMode,
            chaMediaVersion: undefined,
            chaProtectedMediaVersion: undefined
          }
        };
      };

      /**
       * Method to get and return device parameters
       */
      results.getParams = function(devices) {
        return {
          build: devices.build,
          siteId: devices.siteId,
          uuid: devices.UUID,
          version: devices.version,
          platform: devices.platform,
          model: devices.model,
          os: devices.os,
          other: devices.other
        };
      };

      /**
       * Method to count results in the SQLite database
       */
      results.count = function(){
        return sqLite.count('results');
      };

      /**
       * Method to save a result in preperation for export/upload
       * Update of adminLogic.act.pushResult
       * @param  {object} result - raw result object
       * @return {promise}        [description]
       */
      results.save = function(result) {
        var q = $q.defer();

        if (sqLite.numLogs.results === results.dm.maxStoredResults-1) {
          notifications.alert(gettextCatalog.getString('Warning:  This is the ') + results.dm.maxStoredResults + gettextCatalog.getString(' and maximum allowed result stored in TabSINT. Please upload or export the stored results (under Completed Tests in the Results tab of the Admin View).'));
        } else if (sqLite.numLogs.results >= results.dm.maxStoredResults) {
          notifications.alert(gettextCatalog.getString('Error:  There are already') + results.dm.maxStoredResults + gettextCatalog.getString('results stored in TabSINT. This result cannot be saved. Please upload or export the stored results (under Completed Tests in the Results tab of the Admin View).'));
          logger.info('Maximum number of allowed stored results reached.');
        }
        logger.info('Saving result: ' + angular.toJson(result));

        // set dateString to testDateTime if result is passed, otherwise set to the current dateTime
        var dateString;
        if (result.testDateTime) {
          dateString = result.testDateTime.replace(':', '-').replace(':', '-').split('.')[0];
        } else {
          dateString = (new Date()).toJSON().replace(':', '-').replace(':', '-').split('.')[0];
        }
        var type = 'result';

        return encryptResults.encryptSQLite(dateString, angular.toJson(result))
          .then(function(encryptedAESResult) {
            // save encrypted result in results sqLite db
            return sqLite.store('results', dateString, type, encryptedAESResult, results.getParams(devices));
          })
          .then(function() {
            // TODO: when we encrypt results with public key, pass in result and encryptedRSAResult
            // var encryptedRSAResult = ...
            // return results.backup(result, encryptedRSAResult)
            return results.backup(result);
          })
          .then(results.count)
          .then(function(){
            // export/upload the result
            var idx = sqLite.numLogs.results;
            if (disk.autoUpload) {
              if (disk.server === 'localServer'){
                return results.export(idx-1);
              } else {
                return results.upload(idx-1);
              }
            }
          })
          .finally(function() {
            if (disk.exportCSV) {
              return results.exportCSV(result);
            }

            // alert if too many results are stored locally
            if (sqLite.numLogs.results >= results.dm.queuedResultsWarnLength) {
              return results.resultsAlert();
            }

            // upload logs
            if (!disk.disableLogs) {
              return logExport.export();
            }
          });

      };

      /**
       * Back up result object to disk. Backups will be saved in .tabsint-results-backup
       * @param {object} result - result object to backup to file on sd card
       */
      results.backup = function(result, encryptedResult) {
        var filename, dir, output;
        filename = results.filename(result, '.json');
        dir = '.tabsint-results-backup/' + result.protocolName;

        // output encryptedResult if defined, otherwise output plain text
        if (encryptedResult) {
          output = encryptedResult;
        } else {
          output = result;
        }

        return file.createDirRecursively(paths.public(''), dir)
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filename, angular.toJson(output));
          })
          .then(function() {
            logger.info('Successfully exported backup result to file: ' + filename);
          }, function(e) {
            logger.error('Failed to export backup result to file with error: ' + angular.toJson(e));
          });
      };


      /**
       * Pop alert that too many results are being queued.
       * Allow the user to suppress for one day
       */
      results.resultsAlert = function (){
        if (!disk.suppressAlerts) {
          logger.warn('' + sqLite.numLogs.results + gettextCatalog.getString(' have accumulated on the tablet'));
          notifications.confirm(sqLite.numLogs.results + gettextCatalog.getString(' results are saved on the tablet. Please upload or export and remove results from the tablet to avoid losing data'),
            function(btnIdx) {
              if (btnIdx === 2) {
                results.suppressAlerts();
              }
            }, 'Warning', ['OK', 'Suppress Warnings for 1 Day']
          );
        }
      };

      /**
       * Suppress queued results warning for 1 day
       */
      results.suppressAlerts = function() {
        logger.info('Too many results: Suppress turned on');
        disk.suppressAlerts = true;
        setTimeout(function(){
          disk.suppressAlerts = false;
          logger.info('Too many results: Suppress turned back off by timeout');
        }, 1000*60*60*24);
      };

      /**
       * Method to upload result(s) to an uploadable server type.
       * Errors during the upload process are handled within this method, so rejected promises will be empty.
       * @param {number} [idx] = 0 - index of result to upload
       * @return {promise} Promise to upload single result.
       */
      results.upload = function(idx) {
        var r = [];
        if (idx > sqLite.numLogs.results) {
          logger.error('Index ' + idx + ' is outside of results length: ' + sqLite.numLogs.results);
          notifications.alert(gettextCatalog.getString('Failed to upload result at index: ') + idx + '. ' + gettextCatalog.getString('Index is out of range of queued results. Please upload your logs if the issue persists.'));
          return $q.reject();
        }

        // write the file, update summary and delete results for each result uploaded
        function directoryHandling(r) {

          // only upload result if it was sourced from the right place
          if (_.includes(['tabsintServer', 'gitlab'], r.testResults.protocol.server)) {

            // tabsintServer
            if (r.testResults.protocol.server === 'tabsintServer') {
              logger.debug('Uploading test started ' + r.testDateTime + ' to tabsintServer');
              tasks.register('upload results', 'Uploading test started ' + r.testDateTime.replace('T', ' ').replace('Z', '') + ' to TabSINT Server');
              return tabsintServer.submitResults(r)
                .then(function () {
                  return results.updateSummary(r)
                    .then(function() { return results.delete(r.testDateTime) });
                }, function () {
                  logger.error('Failure while uploading result: ' + angular.toJson(r)); // put the whole result in the log at this point
                  notifications.alert('The device failed to upload a result to the TabSINT server. Please verify your TAbSINT server settings and upload your logs. Please upload your logs if the issue persists.');
                  return $q.reject();
                });

            // gitlab
            }  else if (r.testResults.protocol.server === 'gitlab') {
              var host, group, token, repositoryName;

              host = disk.servers.gitlab.host;                  // host is not yet configurable
              token = disk.servers.gitlab.token;                // token is not yet configurable

              // allow user to configure group and repository
              if (disk.gitlab.useSeparateResultsRepo) {
                group = disk.servers.gitlab.resultsGroup;
                repositoryName = disk.servers.gitlab.resultsRepo;
              } else {
                group = disk.servers.gitlab.namespace;
                repositoryName = 'results';
              }

              logger.debug(`Uploading test started ${r.testDateTime} to gitlab server with testDateTime: ${angular.toJson(r.testDateTime)}`);
              var toPath = `${r.testResults.protocol.name}/${results.filename(r, '.json')}`;
              tasks.register('upload results', `Uploading test started ${r.testDateTime.replace('T', ' ').replace('Z', '')} to gitlab repository ${group} / ${repositoryName}`);
  //            return encryptResults.encryptUploadExport(angular.toJson(r), token)
  //              .then(function(enc) {
  //                return gitlab.commit(host, group, repositoryName, token, enc, toPath, `Pushing result from tablet ${devices.shortUUID} for protocol ${r.testResults.protocol.name}`);
  //              })
              return gitlab.commit(host, group, repositoryName, token, r, toPath, `Pushing result from tablet ${devices.shortUUID} for protocol ${r.testResults.protocol.name}`)
                .then(function () {
                  return results.updateSummary(r)
                    .then(function() { return results.delete(r.testDateTime) });
                }, function (e) {
                  logger.error('Failure while uploading result: ' + angular.toJson(r)); // put the whole result in the log at this point
                  if (e && e.msg) {
                    notifications.alert(e.msg);
                  } else {
                    logger.error('Unknown failure while commiting gitlab result ${host} ${group} ${repositoryName} ${token} with error: ${angular.toJson(e)}');
                    notifications.alert(gettextCatalog.getString('TabSINT encountered an issue while pushing result to Gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists.'));
                  }

                  return $q.reject();
                });
            } else {
              notifications.alert(gettextCatalog.getString('Stored result(s) cannot be uploaded.  The protocol used to generate these results was loaded locally from the tablet and is not affiliated with a server, so the results cannot be uploaded to the currently selected server, ') + disk.server + '. ' + gettextCatalog.getString('Please export the result(s) locally to save.'));
              logger.error('Trying to upload a result that has an unknown protocol server: ' + r.testResults.protocol.server);
              return $q.reject();
            }
          }
        }

        function uploadResult(resultFromSQLite) {
          var q = $q.defer();
          var promise = q.promise;

          // check network status, set
          if (!networkModel.status) {
            logger.info('Offline while trying to upload a result');
            notifications.alert(gettextCatalog.getString('The device is having difficulty communicating with the network'));
            return $q.reject();
          }

          _.forEach(resultFromSQLite, function (res, i) {

            // if idx is defined, only upload this result. Otherwise upload all results
            if (angular.isDefined(idx)) {
              if (i === idx) {
                promise = promise
                  .then(function() {
                    return encryptResults.decrypt(res.date, res.data);
                  })
                  .then(function(dec) {
                    r = JSON.parse(dec);
                    r.testResults.network = networkModel.type;   // put network type for upload on the results structure
                    return directoryHandling(r);
                  });
              }
            } else {
              promise = promise
                .then(function() {
                  return encryptResults.decrypt(res.date, res.data);
                })
                .then(function(dec) {
                  r[i] = JSON.parse(dec);
                  r[i].testResults.network = networkModel.type;   // put network type for upload on the results structure
                  return directoryHandling(r[i]);
                })
                .catch(function() {
//                      catching rejection to continue uploading other results
                  return $q.resolve();
                });
            }
          });

          q.resolve();
          return promise;
        }

        // main promise chain to upload result(s)
        return sqLite.count('results')
          .then(sqLite.prepareForUpload('results'))
          .then(function () {
            return sqLite.get('results'); // return all results in sqLite db and pass to uploadResults function
          })
          .then(uploadResult)
          .catch(function(e){
            logger.error('The tablet encountered an issue while uploading result: ' + angular.toJson(e));
            $q.reject(e);
          })
          .finally(function () {
            return tasks.deregister('upload results');
          });
      };

      /**
       * Method to update the upload summary for uploadable results
       * @param  {object} r - result object
       */
      results.updateSummary = function(r) {
        var deferred = $q.defer();
        var meta = {
          siteId : r.siteId,
          protocolName : r.protocolName,
          testDateTime : r.testDateTime,
          nResponses : r.nResponses,
          source: r.testResults.protocol.server,
          output: r.exportLocation || r.testResults.protocol.server,
          uploadedOn : (new Date()).toJSON()
        };
        var len = disk.uploadSummary.unshift(meta);
        logger.info('Result summary: ' + angular.toJson(meta));

        // remove the last element if the summary is beyond the prescribed length
        if (len > results.dm.uploadSummaryLength) {
          disk.uploadSummary.splice(-1, 1);
        }
        deferred.resolve();
        return deferred.promise;
      };

      results.exportCSV = function(result) {
        var base, filenameCSV, filenameFlatHeaderCSV, filenameBroadHeaderCSV;
        base = results.filename(result);
        filenameFlatHeaderCSV = base + '_FLAT_HEADER.csv';
        filenameBroadHeaderCSV = base + '_BROAD_HEADER.csv';
        filenameCSV = base + '.csv';

        var dir;
        if (disk.servers.localServer.resultsDir) {
          dir = disk.servers.localServer.resultsDir;
        } else {
          dir = 'tabsint-results';
        }

        // append the protocol name
        dir = dir + '/' + result.protocolName;

        return file.createDirRecursively(paths.public(''), dir)
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameCSV, csv.generateFlatCSV(result));
          })
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameFlatHeaderCSV, csv.generateFlatHeaderCSV(result));
          })
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameBroadHeaderCSV, csv.generateBroadHeaderCSV(result));
          });
      };

      /**
       * Method to export results to local file
       * @param {number} [idx] - index of result to export. If undefined, will export all
       * @return {promise} promise to export result
       */
      results.export = function(idx) {
        var r = [];
        var filename = [];
        var dir = [];
        tasks.register('export result', 'Exporting results');

        // write the file, update summary and delete results for each result exported
        function directoryHandling(r,filename,dir){
          return file.createDirRecursively(paths.public(''), dir)
            .then(function () {
              return $cordovaFile.writeFile(paths.public(dir), filename, r);
//              return encryptResults.encryptUploadExport(angular.toJson(r), devices.UUID)
//                .then(function(enc) {
//                  return $cordovaFile.writeFile(paths.public(dir), filename, enc);
//                });
            })
            .then(function () {
              logger.info('Successfully exported results to file: ' + filename);
              notifications.alert(gettextCatalog.getString('Successfully exported results to file: ') + filename + gettextCatalog.getString(' in directory: ') + dir);
              return results.updateSummary(r)
                .then(function() { return results.delete(r.testDateTime) });
            })
            .catch(function (e) {
              logger.error('Failed to export results to file with error: ' + angular.toJson(e));
              notifications.alert(gettextCatalog.getString('Failed to export results to file. Please file an issue at') + ' https://gitlab.com/creare-com/tabsint');
              return $q.reject('Failed to write result with error: ' + e);
            });
        }

        function exportResult(resultsFromSQLite) {
          var q = $q.defer();
          var promise = q.promise;

          _.forEach(resultsFromSQLite, function (res, i) {

            // if idx is defined, only export this result. Otherwise export all results
            if (angular.isDefined(idx)) {
              if (i === idx) {
                promise = promise.then(function() {
                  return encryptResults.decrypt(res.date, res.data);
                })
                  .then(function(dec){
                    r = JSON.parse(dec);
                    filename = results.filename(r, '.json');
                    if (disk.servers.localServer.resultsDir) {
                      dir = disk.servers.localServer.resultsDir;
                    } else {
                      dir = 'tabsint-results';
                    }

                    logger.log('Exporting test started ' + r.testDateTime);
                    tasks.register('export result', 'Exporting test started ' + r.testDateTime.replace('T', ' ').replace('Z', '') + ' to the "' + dir + '" directory');

                    // append protocol name into path
                    dir = dir + '/' + r.protocolName;
                    r.exportLocation = dir + '/' + filename;
                    return directoryHandling(r,filename,dir);
                  });
              }
            } else {
              promise = promise.then(function() {
                return encryptResults.decrypt(res.date, res.data);
              })
                .then(function(dec) {
                  r[i] = JSON.parse(dec);
                  filename[i] = results.filename(r[i], '.json');
                  if (disk.servers.localServer.resultsDir) {
                    dir[i] = disk.servers.localServer.resultsDir;
                  } else {
                    dir[i] = 'tabsint-results';
                  }

                  logger.log('Exporting test started ' + r[i].testDateTime);
                  tasks.register('export result', 'Exporting test started ' + r[i].testDateTime.replace('T', ' ').replace('Z', '') + ' to the "' + dir[i] + '" directory');

                  // append protocol name into path
                  dir[i] = dir[i] + '/' + r[i].protocolName;
                  r[i].exportLocation = dir[i] + '/' + filename[i];
                  return directoryHandling(r[i],filename[i],dir[i]);
                })
                .catch(function() {
//                      catching rejection to continue uploading other results
                  return $q.resolve();
                });
            }
          });

          q.resolve();
          return promise;
        }

        // main promise chain to export result(s)
        return sqLite.count('results')
          .then(sqLite.prepareForUpload('results'))
          .then(function () {
            return sqLite.get('results'); // return all results in sqLite db and pass to exportResult function
          })
          .then(exportResult)
          .catch(function (e) {
            logger.error('The tablet encountered an issue while exporting result: ' + angular.toJson(e));
            $q.reject(e);
          })
          .finally(function() {
            return tasks.deregister('export result');
          });
      };

      /**
       * Delete a result from the queued Results
       * @param  {object} result object to delete
       */
      results.delete = function(date) {
        try {
          date = date.replace(':', '-').replace(':', '-').split('.')[0];
          return sqLite.deleteResult(date)
            .then(results.getResultsForResultsView);
        } catch(e) {
          logger.error('Failed to delete result at index: ' + date + ' with error: ' + angular.toJson(e));
          notifications.alert(gettextCatalog.getString('Failed to remove result. Please file an issue at') + ' https://gitlab.com/creare-com/tabsint');
        }
      };

      /**
       * Delete all results from the queued Results
       */
      results.deleteAll = function() {
        logger.info('Deleting all queued results');
        return sqLite.drop('results')
          .then(results.getResultsForResultsView);
      };

      /**
       * Delete all backup results
       */
      results.removeBackupResults = function() {
        logger.info('Deleting all backup results');
        return $cordovaFile.removeRecursively(paths.public(''), '.tabsint-results-backup')
          .then(function() {
            logger.info('Successfully deleted all backup result files');
          });
      };

      /**
       * Used by results-warnings component
       * @return {[type]} [description]
       */
      results.checkStoredResults = function(){
        var ageLimit = 1000*3600*24; // 1 day, in milliseconds
        var oldExamsStoredLocally = false;
        var now = Date.parse(new Date());
        for (var i=0; i<sqLite.numLogs.results; i++) {
          var d = Date.parse(sqLite.get('results').date);
          if ((now-d) > ageLimit){
            oldExamsStoredLocally = true;
          }
        }
        return oldExamsStoredLocally;
      };

      /**
       * Method to refresh completed tests from sqLite results table for Result(s) View
       * @param  {int} ind - integer (optional)
       * @return {promise} Promise to refresh one or all result(s).
       */
      results.getResultsForResultsView = function(ind) {

        function getResults(resultsFromSQLite) {
          var q = $q.defer();
          var promise = q.promise;

          // reset resultsView
          results.completedTests = [];

          _.forEach(resultsFromSQLite, function(res, idx){

            // list all the completed tests still in the sqLite db
            promise = promise
              .then(function() {
                return encryptResults.decrypt(res.date, res.data);
              })
              .then(function(dec) {
                results.completedTests.push(JSON.parse(dec));
                
                // if ind is defined, get one result for the single result viewer
                if (angular.isDefined(ind) && idx === ind) {
                  results.singleResultView = results.completedTests[idx];
                }

                return q.resolve();
              })
              .catch(function() {
                // catching rejection to continue getting other results
                return $q.resolve();
              });
          });

          q.resolve();
          return q.promise;
        }

        // if there are results in the sqLite table, refresh them
        if (angular.isDefined(sqLite.numLogs.results) && sqLite.numLogs.results > 0) {
          return sqLite.count('results')
            .then(function() {
              return sqLite.prepareForUpload('results');
            })
            .then(function () {
              return sqLite.get('results'); // return all results in sqLite db and pass to getResult function
            })
            .then(getResults)
            .catch(function (e) {
              logger.error('The tablet encountered an issue while getting results: ' + angular.toJson(e));
            });

        // if there are no results in the sqLite table, reset completed tests
        } else {
          results.completedTests = undefined;
          return $q.resolve();
        }

      };

      return results;
    });

});
