/**
 * Created by bpf on 10/12/2016.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.check', [])
    .factory('chaCheck', function () {

      return {
        isAudiometryExam: function (exam) {
          return _.includes(['BekesyLike', 'BekesyFrequency', 'HughsonWestlake', 'HughsonWestlakeFrequency', 'BHAFT'], exam);
        },
        isBekesyExam: function (exam) {
          return _.includes(['BekesyLike', 'BekesyFrequency', 'BHAFT'], exam);
        },
        isFrequencyExam: function (exam) {
          return _.includes(['BekesyFrequency', 'HughsonWestlakeFrequency'], exam);
        },
        isLevelExam: function (exam) {
          return _.includes(['BekesyLike', 'HughsonWestlake'], exam);
        },
        isHughsonWestlakeExam: function (exam) {
          return _.includes(['HughsonWestlake', 'HughsonWestlakeFrequency'], exam);
        },
        isThirdOctaveBandsExam: function (exam) {
          return _.includes(['ThirdOctaveBands'], exam);
        },
        isToneGenerationExam: function (exam) {
          return _.includes(['ToneGeneration'], exam);
        },
        isDPOAEExam: function (exam) {
          return _.includes(['DPOAE'], exam);
        }
      };

    });

});

