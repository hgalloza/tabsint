/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.cha-choose', [])
    .factory('chooseCha', function ($uibModal, disk, notifications, logger, $timeout) {
      var api = {
        discover: undefined
      };

      api.discover = function (bluetoothType, chaList, deferred) {

        var ChooseChaCtrl = function ($scope, $uibModalInstance) {
          $scope.chaList = chaList;
          $scope.chosenCha = undefined;
          $scope.selectable = undefined;
          $scope.bluetoothType = bluetoothType;

          $scope.bluetoothTypes = {
            BLUETOOTH: 'Bluetooth 2.0',
            BLUETOOTH_LE: 'Bluetooth 3.0',
            USB: 'USB Host'
          };

          // Start searching for the cha
          try {
            logger.debug('CHA - Modal - Starting cha search');
            ChaWrap.startChaSearch(bluetoothType, gotCha, noGotCha);
          } catch(e) {
            console.log(e);
            notifications.alert('Cha Wrapper function is not defined. Please hand the device to an administrator.');
            logger.error('CHA Wrapper is not defined. Make sure your config file (tabsintConfig.json) has the appropriate definitions for the plugins{cha}.' +
              'See the README.txt for more information about how to define the cha plugin in the config file');
          }

          // ChaWrap callback
          function gotCha(cha) {
            logger.debug('CHA - Modal - Got a CHA: ' + cha.getName());
            if (!_.includes(chaNameList(), cha.getName())){
              $scope.chaList.push(cha);
              selectable();
              $scope.$digest();
            }
          }
          function chaNameList(){
            var tempChaNameList = [];
            _.forEach($scope.chaList, function(cha){
              tempChaNameList.push(cha.getName());
            });
            return tempChaNameList;
          }

          function noGotCha(info) {
            if (info === 'done' && $scope.chaList.length === 0) {
              // continue searching as long as no cha has been found
              logger.debug('CHA - Modal - No cha found or selected. Starting discovery again...');
              $scope.chaList = [];
              ChaWrap.startChaSearch(bluetoothType, gotCha, noGotCha);
            } else if (info !=='done') {
              logger.error('CHA - Modal - Finished searching with info != done. Info: ' + angular.toJson(info));
              deferred.reject({code: 51, msg: 'ChaWrap.startChaSearch failed' });
            }
          }


          function cancelModal() {
            deferred.reject({code: 52, msg: 'ChooseCha model cancelled'});
            $uibModalInstance.dismiss('cancel');
          }

          // Cha option button choice logic
          $scope.choose = function(cha) {
            $scope.chosenCha = cha;
            selectable();
          };

          $scope.chosen = function(cha) {
            if ($scope.chosenCha) {
              return($scope.chosenCha.json.name === cha.json.name);
            } else {
              return false;
            }
          };

          function selectable() {
            $scope.selectable = !_.isUndefined($scope.chosenCha) && _.includes(chaNameList(), $scope.chosenCha.getName());
          }



          // Footer button logic (Select, Cancel)
          $scope.select = function (chosenCha) {
            try { ChaWrap.cancelChaSearch() }
            catch(e) {logger.error('CHA cancelChaSearch() failed to call')}
            $uibModalInstance.close(chosenCha);
          };

          $scope.cancel = function () {
            $scope.chaList = [];
            try { ChaWrap.cancelChaSearch() }
            catch(e) {logger.error('CHA cancelChaSearch() failed to call')}
            cancelModal();
          };

        };

        // Modal controllers and logic
        var modalInstance = $uibModal.open({
          templateUrl: 'scripts/services/cha-choose/modal_chooseCha.html',
          controller: ChooseChaCtrl,
          backdrop: 'static'
        });

        // function to call upon $uibModalInstance.close()
        modalInstance.result.then(function (chosenCha) {
          if (chosenCha) {
            disk.plugins.cha.myCha = chosenCha.getName();
            $timeout(function() {deferred.resolve(chosenCha);}, 500);
          } else {
            deferred.reject({code: 53, msg: 'chosenCha object was not properly passed out of the modal'});
          }
        });
      };

      return api;
    });


});
