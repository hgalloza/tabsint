/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular', 'cryptoJs'], function (angular, CryptoJS) {
  'use strict';

  angular.module('tabsint.services.encrypt-results', [])

    .factory('encryptResults', function ($cordovaFile, $q, devices, disk, file, logger, paths, app) {
      var encryptResults = {};

      // Persistent storage for crypto keys while debugging in the browser
      if (!app.tablet) {
        disk.browserTestingCryptoKeys = {}; 
      }

      /**
       * Encrypt results for storage in database
       * @param  {string} result - result to be encrypted in sqLite
       * @return (string) encrypted result
       */
      encryptResults.encryptSQLite = function(dateString, result) {

        // Generate an AES key from datestring and uuid to encrypt results
        var key256Bits = CryptoJS.PBKDF2(dateString, devices.UUID, { keySize: 256/32 }).toString();

        if (app.tablet) {

          // Write key to internal storage
          return $cordovaFile.writeFile(cordova.file.applicationStorageDirectory, dateString + '.txt', key256Bits, true)
            // AES (symmetric) encryption or data encapsulation
            .then(function() {
              // Return the string of the the encrypted data to store in db
              return CryptoJS.AES.encrypt(result, key256Bits, { iv: devices.UUID }).toString();
            })
            .catch( function(e){
              logger.log('Error while encrypting results: ' + angular.toJson(e));
              return $q.reject(e);
            });
        } 

        // browser testing
        else {
          disk.browserTestingCryptoKeys[dateString] = key256Bits;
          return $q.resolve(CryptoJS.AES.encrypt(result, key256Bits, { iv: devices.UUID }).toString());
        }

      };

      /**
       * Decrypt results
       * @param  {string} dateString string
       * @param  {string} encrypted result from sqLite
       * @return (string) decrypted result
       */
      encryptResults.decrypt = function(dateString, encryptedResult) {

        if (app.tablet) {

          // Read key from internal storage
          return $cordovaFile.readAsText(cordova.file.applicationStorageDirectory, dateString+'.txt')
            .then(function(key256Bits) {
              // AES (symmetric) decryption
              var bytes = CryptoJS.AES.decrypt(encryptedResult, key256Bits, { iv: devices.UUID });
              return bytes.toString(CryptoJS.enc.Utf8);
            })
            .catch( function(e){
              logger.log('Error while decrypting results: ' + angular.toJson(e));
              return $q.reject(e);
            });
        }

        // browser testing
        else {
          var key256Bits = disk.browserTestingCryptoKeys[dateString];
          var bytes = CryptoJS.AES.decrypt(encryptedResult, key256Bits, { iv: devices.UUID });
          return $q.resolve(bytes.toString(CryptoJS.enc.Utf8));
        }
      };

      return encryptResults;
    });

});
