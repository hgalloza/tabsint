/**
 * Created by rxc on 12/5/13.
 */
/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['tv4', 'test-dep'], function (tv4) {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Server', function () {

    var url, siteTokenSchema, tabsintServer, $http, $httpBackend, config, json, disk, networkModel, protocol;
    beforeEach(inject(function (_tabsintServer_, _$http_, _$httpBackend_, _config_, _json_, _disk_, _networkModel_, _protocol_) {
      tabsintServer = _tabsintServer_;
      $http = _$http_;
      $httpBackend = _$httpBackend_;
      config = _config_;
      json = _json_;
      disk = _disk_;
      networkModel = _networkModel_;
      protocol = _protocol_;
      _$httpBackend_.whenGET('res/translations/translations.json')
        .respond(200, 'a string');
    }));

    beforeEach(function() {
      disk.servers.tabsintServer.url = 'https://tabsintServer/';
      protocol.changeSource('tabsintServer');
      networkModel.status = true; // set online
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });



    describe('Site token', function () {

      beforeEach(function () {
        siteTokenSchema = json.load('base/test/spec/tabsint-server/sitetoken_schema.json');
      });

      it('should have a realistic-looking JSON-SCHEMA.', function () {
        expect(siteTokenSchema.$schema).toEqual('http://json-schema.org/draft-04/schema#');
        $httpBackend.flush();
      });

      it('if site doesn\'t exist, it throws an error.', function () {
        var fail = jasmine.createSpy();

        $httpBackend.expectGET(disk.servers.tabsintServer.url+'SiteToken?site=site_thats_imaginary')
          .respond(404, '{"success":false,"status":"site not found or no protocol assigned"}');

        var token = null;
        tabsintServer.updateConfiguration('site_thats_imaginary').then(
          function (d) {
            token=d;
          }, fail);
        $httpBackend.flush();

        expect(token).toBeNull();
        expect(fail).toHaveBeenCalled();
      });

    });

    describe('Upload Tests', function () {

      it('can insert a test', function () {

        $httpBackend.expectGET(disk.servers.tabsintServer.url+'countTests')
          .respond(200, '{"success":true,"status":"counted","count":"18"}');

        $httpBackend.expectPOST(disk.servers.tabsintServer.url+'InsertTest')
          .respond(200, '{"success":true,"status":"created","testid":10}');

        $httpBackend.expectGET(disk.servers.tabsintServer.url+'countTests')
          .respond(200, '{"success":true,"status":"counted","count":"19"}');

        tabsintServer.submitResults({siteId: 1, subjectId: null, results: {} });
        $httpBackend.flush();

      });

      it('should fail if the count does not increase', function() {

        $httpBackend.expectGET(disk.servers.tabsintServer.url+'countTests')
          .respond(200, '{"success":true,"status":"counted","count":"18"}');

        $httpBackend.expectPOST(disk.servers.tabsintServer.url+'InsertTest')
          .respond(200, '{"success":true,"status":"created","testid":10}');

        $httpBackend.expectGET(disk.servers.tabsintServer.url+'countTests')
          .respond(200, '{"success":true,"status":"counted","count":"18"}');

        var response = null;
        tabsintServer.submitResults({siteId: 1, subjectId: null, results: {} })
          .then(null, function(resp) {
            response = resp;
          });

        $httpBackend.flush();
        expect(response.msg === 'no increase');
      });

    });

  });
});
