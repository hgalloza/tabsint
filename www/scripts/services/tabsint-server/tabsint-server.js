/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

 define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.tabsint-server', [])

  .factory('tabsintServer', function ($http, $q, app, config, $interval, disk, tasks, 
                                      notifications, file, logger, sqLite, networkModel, 
                                      paths, gitlab, protocol, remote, gettextCatalog) {
      // API is defined by the server api
      var api = {
        init: undefined,
        updateConfiguration: undefined,
        submitLogs: undefined,
        downloadSubjectHistory: undefined,
        submitResults: undefined,
        link: undefined,
        download: undefined,
        authorize: undefined,
        checkStatus: undefined,
        ping: undefined,
        triggerReports: undefined,
        dm: {
          serverName: undefined,
          headers: undefined,
          authorized: undefined,
          timeout: 10000 // 10 s
        }
      };

      /**
       * Initialize the tabsint server.
       * Defaults will be taken from the config file, unless the values are already on disk.
       */
      api.$init = function() {
        api.overrideHttp();

        // Initialize Server object
        if (!disk.servers.tabsintServer) {
          disk.servers.tabsintServer = {};
        }

        // set config values if no disk value is present
        api.defaults();

        // run authorization
        api.authorize();

        logger.info('Tabsint Server initialized to: ' + disk.servers.tabsintServer.url + ' with headers: ' + angular.toJson(api.dm.headers));
      };


      /**
       * Get the default values from the config file
       * @param {boolean} reset - reset the disk values to the config file
       */
      api.defaults = function(reset) {

        function getConfig(key) {
          if (!reset && disk.servers.tabsintServer[key]) {
            return;
          } else if (config.server[key]) {
            logger.debug('Re-initializing tabsint server key: '+ key + ' to default config value');
            disk.servers.tabsintServer[key] = config.server[key];
          } else {
            disk.servers.tabsintServer[key] = undefined;
          }
        }

        getConfig('url');
        getConfig('username');
        getConfig('password');

        // cleanup url
        if (disk.servers.tabsintServer.url) {
          disk.servers.tabsintServer.url = paths.https(disk.servers.tabsintServer.url);
        }
      };

      api.clearAuthorization = function() {
        api.dm.authorized = undefined;
      };

      api.reauthorize = function() {
        api.clearAuthorization();
        api.authorize();
      };

      /**
       * Authorize the tabsint server
       * @return {promise} - promise to authorize
       */
      api.authorize = function() {

        // make standard authorization headers
        try {
          api.dm.headers = {
            'Authorization': 'Basic ' + btoa(disk.servers.tabsintServer.username + ':' + disk.servers.tabsintServer.password)
          };
        } catch (e) {
          logger.error('Failure while trying to convert tabsint server username and password to base64. Older browsers are incompatible with the btoa() method');
          //delete disk.servers.tabsintServer;
          disk.servers.tabsintServer = {};
          api.dm.headers = undefined;
          api.dm.authorized = false;

          return $q.reject('ERROR: Could not set headers for authorization in tabsintServer.  Please check TabSINT Server Configuration panel in Configuration Tab.');
        }

        if (app.tablet) {
          return api.getAuthorization()
            .then(api.getServerToken);
        } else {
          return $q.resolve();
        }
      };

      /**
       * Method to set authorization cookie from tabsint server
       * @return {promise}
       */
      api.getAuthorization = function() {
        var deferred = $q.defer();

        if (!api.dm.authorized) {
          api.get(disk.servers.tabsintServer.url + 'SetAuthorization')
            .then(function(){
              logger.debug('Server successfully set authorization cookie');
              api.dm.authorized = true;
              deferred.resolve();
            }, function() {
              logger.error('Server failed to set authorization cookie');
              api.dm.authorized = false;
              deferred.reject('ERROR: Server failed to set authorization cookie.  Please check TabSINT Server Configuration in Configuration Tab.');
            });
        } else {
          deferred.resolve();
        }

        return deferred.promise;
      };

      /**
       * Local method to get server toekn (shortname) from tabsint server
       * @return {promise}
       */
      api.getServerToken = function() {
        var deferred = $q.defer();

        if (!api.dm.serverName) {
          api.get(disk.servers.tabsintServer.url + 'ServerToken')
            .then(function(response){
              api.dm.serverName = response.data.shortname;
              logger.debug('Successfully set the server shortname to:  ' + response.data.shortname);
              deferred.resolve();
            }, function(response) {
              logger.error('Server failed to get Server Token');
              deferred.resolve();
            });
        } else {
          deferred.resolve();
        }

        return deferred.promise;
      };

      /**
       * Wrapper over remote.get to handle tabsintServer responses
       * @param  {[type]} url [description]
       * @return {[type]}     [description]
       */
      api.get = function(url) {
        return remote.get(url, {headers: api.dm.headers, timeout: api.dm.timeout})
          .then(function (response) {
            if (response.data.success || response.data.shortname) {
              return response;
            } else {
              return $q.reject(response);
            }
          });
      };


      /**
       * wrapper over remote.post to handle tabsintServer responses
       * @param  {[type]} url  [description]
       * @param  {[type]} data [description]
       * @return {[type]}      [description]
       */
      api.post = function(url, data) {
        return remote.post(url, data, {headers: api.dm.headers, timeout: api.dm.timeout})
          .then(function (response) {
            if ( !response.data.success ) {
              return $q.reject();
            } else {
              return response;
            }
          });
      };

      /**
       * Convience method to link to tabsintServer locations
       * @param  {string} dest - destination shorthand
       */
      api.link = function(dest)  {
        var url;
        if (dest === 'builds') {
          url = disk.servers.tabsintServer.url + 'builds/' + api.dm.serverName + '/' + config.build + '/release/index.html';
        } else if (dest === 'docs') {
          url = disk.servers.tabsintServer.url + 'docs/' + api.dm.serverName + '/' + config.build + '/release/';
        }

        logger.info('Opening link: ' + url);
        window.open(url, '_system');
      };

      /**
       * Convience file downloading method. Wraps file.download
       * @param  {string} remotePath [description]
       * @param  {string} localPath  - local path to download to. relative to file.fs.root.nativeURL directory
       * @param  {object} dirOption  - directory options
       * @return {promise} promise to download file
       */
      api.download = function(remotePath, localPath, dirOption){
        var url = disk.servers.tabsintServer.url + remotePath;
        var urlClean = url.replace(/ /g, '%20');
        logger.debug('tabsint server downloading from url: ' + urlClean);

        return file.download(urlClean, localPath, {'headers': api.dm.headers}, dirOption);
      };

      /**
       * Method to pull down new protocol from a tabsint server
       * @param  {string} siteName - site name to pull down
       * @return {promise} promise to update the site
       */
      api.updateConfiguration = function(siteName) {

        // check network connection
        if (!networkModel.status) {
          logger.error('Offline while trying to update configuration');
          notifications.alert(gettextCatalog.getString('The tablet is not currently connected to a wifi or cellular network. Please check your network connection'));
          return $q.reject();
        }

        if (!disk.servers.tabsintServer.url) {
          logger.error('No url specified while trying to update configuration');
          notifications.alert(gettextCatalog.getString('No url has been specified for use with the TabSINT server.  Please check your TabSINT Server Configuration on the Configuration tab.'));
          return $q.reject();
        }

        // check to see if task is already running NOTE - updating task already registered in protocols.js
        // if (tasks.isOngoing('updating')) {
        //   logger.warn('Site config task still in progress...');
        //   notifications.alert(gettextCatalog.getString('Site config task still in progress...'));
        //   return $q.reject();
        // }

        // select protocol to add or update
        var p = {};
        if (!siteName) {
          if (disk.protocol && disk.protocol.server === 'tabsintServer') {
            logger.debug('Updating tabsint server protocol: ' + angular.toJson(disk.protocol));
            p = disk.protocol;
          } else {
            notifications.alert(gettextCatalog.getString('The current protocol can not be updated from a TabSINT server'));
            return $q.reject();
          }
        } else {
          if (_.find(disk.protocols, {siteName: siteName})) {
            p = _.find(disk.protocols, {siteName: siteName});
            logger.debug('Updating tabsint server protocol: ' + angular.toJson(p));
          } else {
            logger.debug('Attempting to add new tabsint server protocol: ' + siteName);
            p.siteName = siteName;
          }
        }

        tasks.register('updating', 'Updating Protocol...');

        var newSite, headset;

        function checkForChanges(response) {
          // checks http token to see if siteId and/or protocol has changed
          var deferred = $q.defer();

          logger.debug('new site id: ' + response.data.sitetoken.siteId + ', with protocolId ' + response.data.sitetoken.protocolId + ', and hash ' + response.data.sitetoken.protocolHash);
          logger.debug('old site id: ' + p.siteId + ', with protocolId ' + p.id + ', and hash ' + p.hash);

          if (!response.data.sitetoken.protocolId || !response.data.sitetoken.protocolHash) {
            deferred.reject('no protocol');
          }

          if ( (response.data.sitetoken.siteId === p.siteId) && (response.data.sitetoken.protocolId === p.id) && (response.data.sitetoken.protocolHash === p.hash) ) {
            if (p.calibration === 'None' || p.calibration === disk.headset) {
              api.downloadSubjectHistory(p.siteName, p.path + '/subjecthistory.json')
                .finally(function() {
                  deferred.reject('unchanged');
                });
            } else {
              logger.debug('Site token unchanged, need to generate a new calibration for the headset selected');
              newSite = response.data.sitetoken;
              deferred.resolve();
            }
          } else {
            newSite = response.data.sitetoken;
            deferred.resolve();
          }
          return deferred.promise;
        }

        function getRemoteCal(action, site, tablet, headset) {

          // NOTE: tablet must always be set to Nexus 7 to continue to support the gain factor method in media playback
          tablet = 'Nexus 7';

          // encode the tablet name
          var tabletClean = tablet.replace(/ /g, '%20');

          var url = disk.servers.tabsintServer.url + action + 'CalibratedProtocolForSite?site='+site+'&tablet='+tabletClean+'&headset='+headset;
          return api.authorize()
            .then(function(){return api.get(url)});
        }

        function checkCalibration() {
          logger.debug('Running calibration check for site: "' + p.siteName + '" with the headset: ' + disk.headset);

          return getRemoteCal('Check', p.siteName, 'Nexus 7', disk.headset)
            .catch(function(response) {
              // calibration was not found, no headset specfied
              if (response.data.status === 'calibrated protocol file not found' && !angular.isDefined(disk.headset)) {
                return getRemoteCal('Check', p.siteName, 'Nexus 7', 'VicFirth')
                .then(function() {
                  headset = 'VicFirth';
                }, function() {
                    return getRemoteCal('Check', p.siteName, 'Nexus 7', 'HDA200')
                        .then(function () {
                            headset = 'HDA200';
                        }, function () {
                            return getRemoteCal('Check', p.siteName, 'Nexus 7', 'Audiometer')
                                .then(function () {
                                    headset = 'Audiometer';
                                })
                                .catch(function () {
                                    logger.error('Calibrated protocol file not found and headset is undefined. Tried to find calibrated protocols with VicFirth, HDA200, and Audiometer. All three failed');
                                    return $q.reject({msg: 'checkCalibration'});
                                });
                        });
                });
              } else if (response.data.status === 'calibrated protocol file not found' && angular.isDefined(disk.headset)) {
                logger.debug('Attempting to generate calibrated audio...');
                tasks.register('updating', 'Generating calibrated protocol - this may take a few minutes...');

                return getRemoteCal('Generate', p.siteName, 'Nexus 7', disk.headset)
                  .then(function() {return waitForCalibrationToComplete();});
              } else {
                logger.debug('The response from getRemoteCal(\'CHECK\') could not be handled: ' + angular.toJson(response));
                $q.reject({msg: 'checkCalibration'});
              }
            });
        }

        function waitForCalibrationToComplete() {
          var deferred = $q.defer();
          logger.debug('Waiting for calibration to complete...');
          var count = 0;

          var calibrationInquiry = $interval( function() {
            return getRemoteCal('Check', p.siteName, 'Nexus 7', disk.headset)
            .then(function () {
              $interval.cancel(calibrationInquiry);

              deferred.resolve();
            })
            .catch(function () {
              count += 1;
              logger.debug('Calibration still not ready...');
              tasks.register('updating', 'Generating calibrated protocol - this may take a few minutes...' + new Array(count%5).join('..'));
            });
          }, 1500);
          return deferred.promise;
        }

        function downloadNewProtocol(defaultHeadset) {
          tasks.register('updating', 'Downloading protocol...');

          // set headset to disk, if not defined in checkCalibration()
          if (!headset) {
            headset = disk.headset;
          }

          var url = disk.servers.tabsintServer.url +'CalibratedProtocolForSite?site=' + p.siteName + '&tablet=Nexus 7&headset=' + headset;
          var urlClean = url.replace(/ /g, '%20');
          logger.debug('Downloading '+ urlClean);

          return file.download(urlClean, 'protocol.zip', {'headers': api.dm.headers}, null);
        }

        function unzipProtocol(theFile) {
          // set protocol unique name to downloaded zip file (less the .zip part)
          p.path = paths.data() + paths.dir(p.siteName + '_' + Date.parse(Date()) );

          var deferred = $q.defer();
          tasks.register('updating', 'Processing protocol...');
          logger.debug('Unzipping protocol');

          zip.unzip(theFile.nativeURL, // source zip
            p.path,  // target dir
            function (ret) {
              if ( ret === 0 ) {
                logger.debug('Unzipped protocol');
                deferred.resolve();
              } else {
                logger.error('Could not unzip protocol. Return: ' + angular.toJson(ret));
                deferred.reject('Could not unzip protocol.');
              }
            },
            function (prog) {  // progress callback.
              if (prog.total !== undefined && prog.loaded !== undefined) {
                var pct = (prog.loaded / prog.total)*100;
                deferred.notify('Processing protocol: ' + pct.toFixed(2) + '% done.');
              } else {
                deferred.notify('Processing protocol:  Percent done is unknown.');
              }
            });
          return deferred.promise;
        }

        function finalize() {

          // protocol meta data object
          var newProtocol = protocol.define({
            name: newSite.protocolName,
            path: p.path,
            id: newSite.protocolId,
            date: newSite.protocolCreationDate,
            creator: newSite.protocolOwner,
            server: 'tabsintServer',
            hash: newSite.protocolHash,
            siteName: p.siteName,
            siteId: newSite.siteId,
            calibration: p.calibration
          });

          // store new version of the protocol
          protocol.store(newProtocol);

          // if no protocol is loaded, load this one
          if (!disk.protocol) {
            disk.protocol = newProtocol;
          }

          // return a list of files
          return file.list(newProtocol.path);
        }

        function removeOldZip() {
          return file.removeFile('protocol.zip')
          .then(function() {
            return file.removeFilesStar('','zip');
          });
        }

        function progressCB(msg) {
          tasks.register('updating', '' + msg);
        }


        return api.authorize()
          .then(function() {return api.get(disk.servers.tabsintServer.url + 'SiteToken?site=' + p.siteName)})
          .then(function(response) {return checkForChanges(response)})
          .then(checkCalibration)
          .then(function(headsetOverride) {return downloadNewProtocol(headsetOverride);})
          .then(unzipProtocol, $q.reject('Protocol download failed.'))
          .then(finalize, $q.reject('Could not unzip protocol.'))
          .then(removeOldZip, $q.reject('Could not finalize'), progressCB) // apparently only need one of these for all chained promises!!  Multiples conflict.
          .catch(
            function(e) {
              if (!e) {e = {}}

            // if protocol is up to date
            if (e === 'unchanged') {
              notifications.alert(gettextCatalog.getString('Protocol and calibration is up to date'));
              logger.info('Protocol is already up to date');
            } else {
              var msg;
              var logMsg = 'Site configuration failed with error: ' + angular.toJson(e);
              if (e.msg === 'The tablet is having trouble connecting to the network') {
                logger.warn(logMsg);
                msg = gettextCatalog.getString('The tablet is having trouble connecting to the network. Please check your network connection');
              } else if (e === 'no protocol') {
                logger.error(logMsg);
                msg = gettextCatalog.getString('No protocol assigned to this site. Please log in to the server and assign a protocol to this site name.');
              } else if ( e.status === 404 ) {
                logger.error(logMsg);
                msg = gettextCatalog.getString('No configuration found for this site name - Does the site name exist?');
              } else if (e === 'Site config task still in progress...') {
                logger.warn(logMsg);
                msg = gettextCatalog.getString('Site configuration still in progress...');
              } else if (e.msg === 'checkCalibration') {
                logger.error(logMsg);
                msg = gettextCatalog.getString('TabSINT failed to find the right calibration for this protocol. Please check your headset selection, the site configuration and protocol files.');
              } else if (e === 'Protocol download failed.'){
                logger.warn(logMsg);
                msg = gettextCatalog.getString('Protocol download failed.');
              } else if ((e.code === 4) || (e === 'Update Cancelled')) {
                logger.warn(logMsg);
                msg = gettextCatalog.getString('Site configuration update cancelled');
                /* jshint ignore:start */ // Apparently http_status should be camelCase, and jshint catches on it
              } else if ((e.code === 3) && (e.http_status === 200)) {
                /* jshint ignore:end */
                logger.warn(logMsg);
                msg = gettextCatalog.getString('Network connection lost during protocol download.  Try again, or try moving to a location with better wifi or cellular network coverage.');
              } else if (e === 'Could not unzip protocol.') {
                logger.error(logMsg);
                msg = gettextCatalog.getString('TabSINT failed to unzip the protocol files from the archive. Please check your protocol files and upload your logs.');
              } else if (e === 'no headset') {
                logger.error(logMsg);
                msg =gettextCatalog.getString('Protocol has calibrated audio files and requires a headset. Please choose a headset from below');
              } else if ( ((e || {}).data || {}).status === 'unable to obtain lock, other calibration in process') {
                logger.warn(logMsg);
                msg =gettextCatalog.getString('This protocol is currently being calibrated. Please wait a few minutes and try updating the site again.');
              } else {
                logger.error(logMsg);
                msg = gettextCatalog.getString('Site configuration failed for an unknown reason. Please upload your logs.');
              }
              notifications.alert(msg);
            }

            // clean up any zip files left over
            try {
              removeOldZip();
            } catch(e) {
              logger.error('failed to remove protocol.zip');
            }

            return $q.reject(e);
          })
          .finally(function () {
            tasks.deregister('updating');
          });

        };

      /**
       * Method to download subject history from tabsintServer
       * @param  {string} siteName - site name
       * @param  {string} path - local directory, relative to file.fs.root.nativeURL to download to
       */
      api.downloadSubjectHistory = function (siteName, path) {
        var url = disk.servers.tabsintServer.url +'data/' + api.dm.serverName + '/subject_history/' + siteName + '.json';
        return api.authorize()
          .then(function(){return remote.getHead(url); })
          .then(function() {
            logger.debug('Downloading subject history from '+ url);
            return file.download(url, path, {'headers': api.dm.headers}, null);
          }, function() {
            logger.debug('No subject history found for url: ' + url);
            return $q.reject();
          });
      };


      /**
       * Method to submit logs to a tabsint server
       * @param  {boolean} userCalled - boolean to determine if the log upload process was initiated by a user
       * @return {promise to submit logs}
       */
      api.submitLogs = function(userCalled)  {

        // Preliminary Checks
        if ( logger.getCount.logs === 0 ) {
          return  $q.reject();
        }

        if ( tasks.isOngoing('upload_logs') ) {
          if (userCalled){
            notifications.alert(gettextCatalog.getString('Log upload already in process'));
          }
          return $q.reject();
        }

        if ( !disk.servers.tabsintServer.url ) {
          if (userCalled){
            notifications.alert(gettextCatalog.getString('WARNING:  No url specified for log upload.  Please check TabSINT Server Configuration.'));
          }
          return $q.reject();
        }

        if (!networkModel.status) {
          logger.warn('network.status is false while trying to upload logs');
          if (userCalled) {
            notifications.alert(gettextCatalog.getString('The tablet is not currently connected to a wifi or cellular network. Please check your network connection'));
          }
          return $q.reject();
        }

        if (userCalled){
          logger.info('Uploading logs');
        } else {
          logger.info('Automatically uploading logs');
        }

        // initialize process
        var totalLogs = 0, logsUploaded = 0;
        tasks.register('upload_logs', 'Uploading log messages(s)...');

        // upload logs
        return api.authorize()
          .then(logger.count)
          .then(function(n) {
            totalLogs = n;
            logger.info('Counted ' + totalLogs + ' logs to upload');
          })
          .then(sqLite.prepareForUpload('newlogs'))
          .then(sendLogPackage)
          .then(logger.count)
          .then(function(n) {
            logsUploaded = totalLogs - n;
          })
          .catch(function(e) {
            logger.error('The tablet encountered an issue while uploading logs: ' + angular.toJson(e));
            if (userCalled) {
              notifications.alert(gettextCatalog.getString('The tablet encountered an issue while uploading log messages. ') + logsUploaded + gettextCatalog.getString(' logs were successfully uploaded. Please export your logs and file an issue')+' at https://gitlab.com/creare-com/tabsint');
            }
          })
          .finally(function () {
            logger.info('Succesfully uploaded ' + logsUploaded + ' logs');
            tasks.deregister('upload_logs');
          });

        /** Internal method to send all logs to the server recursively */
        function sendLogPackage() {
          var q = $q.defer();

          function processLogs(logsFromSQLite){
            if (!networkModel.status) {
              q.reject('The tablet lost network connection while uploading logs');
            }

            if (logsFromSQLite.length > 0){
              var ret = {};
              ret.logs = [];
              logsFromSQLite.forEach(function(log){ret.logs.push(log);});
              ret = angular.toJson(ret);

              return api.post(disk.servers.tabsintServer.url + 'UploadLogJSON', ret)
                .then(processResponse);
            } else {
              q.resolve();
            }
          }

          function processResponse(serverResponse){
            if (serverResponse.data.acceptedLogs && serverResponse.data.acceptedLogs.length > 0){
              return deleteRows(serverResponse.data.acceptedLogs)
                .then(processBatch);
            } else {
              q.reject('Inappropriate server response: ' + angular.toJson(serverResponse));
            }
          }

          function deleteRows(idList){
            var deleteRowPromises = [];
            idList.forEach(function(id){ // MUST create a function scope for each promise, otherwise they overwrite
              var promise = sqLite.deleteLogs(id);

              deleteRowPromises.push(promise);
            });
            return $q.all(deleteRowPromises); // return once all posted rows have been purged
          }

          function processBatch(){
            sqLite.get('newlogs')
            .then(processLogs)
            .catch(function(e){
              q.reject(e);
            });
          }

          // start the first batch process, will get recursively each round
          processBatch();
          return q.promise;
        }
      };

      /**
       * Method to upload a single result to a tabsintServer
       * @param  {[type]} r [description]
       * @return {[type]}        [description]
       */
      api.submitResults = function (r)  {

        if ( !disk.servers.tabsintServer.url ) {
          notifications.alert(gettextCatalog.getString('WARNING:  Cannot submit results to the TabSINT Server.  No URL defined.  Please check TabSINT Server Configuration in Configutation Tab.'));
          return $q.reject({msg: 'no url defined'});
        }

        if (!networkModel.status){
          logger.error('network false while submitting result to tabsintServer');
          return $q.reject({msg: 'offline'});
        }

        var nBefore=1, nAfter= 0;

        /** Convience method to count tests on the server */
        function countTests() {
          return api.get(disk.servers.tabsintServer.url + 'countTests')
            .then(function (response) {
              return parseInt(response.data.count);
            });
        }

        return api.authorize()
          .then(countTests)
          .then(function (n) {
            nBefore = n;
            return api.post(disk.servers.tabsintServer.url + 'InsertTest', {siteId: r.siteId, subjectId: r.subjectId, results: angular.toJson(r) });
          })
          .then(function (response) {
            return countTests();
          })
          .then(function (n) {
            if ( nBefore >= n ) {
              logger.error('Number of tests on server did no increase');
              return $q.reject({msg: 'no increase'});
            }
          });
      };




      /**
       * Method to trigger reports on jenkins server
       * @return {promise} Promise to post trigger report
       */
      api.triggerReports = function(){
        var url = 'https://jenkins.crearecomputing.com/buildByToken/buildWithParameters';
        var data = {
          job: 'hearing-products/tabsint-auto-reports',
          token: 'analyze_results',
          ServerName: api.dm.serverName,
          BuildName: config.build,
          SiteName: disk.protocol.siteName
        };

        return remote.post(url, data);
      };


      /**
       * Override angular http mechanism
       *
       */
      api.overrideHttp = function() {
        // Use x-www-form-urlencoded Content-Type
        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        // Override $http service's default transformRequest
        $http.defaults.transformRequest = [function(data)
        {
          /**
           * The workhorse; converts an object to x-www-form-urlencoded serialization.
           * @param {Object} obj
           * @return {String}
           */
           var param = function(obj)
           {
            var query = '';
            var name, value, fullSubName, subName, subValue, innerObj, i;

            for(name in obj)
            {
              value = obj[name];

              if(value instanceof Array)
              {
                for(i=0; i<value.length; ++i)
                {
                  subValue = value[i];
                  fullSubName = name + '[' + i + ']';
                  innerObj = {};
                  innerObj[fullSubName] = subValue;
                  query += param(innerObj) + '&';
                }
              }
              else if(value instanceof Object)
              {
                for(subName in value)
                {
                  subValue = value[subName];
                  fullSubName = name + '[' + subName + ']';
                  innerObj = {};
                  innerObj[fullSubName] = subValue;
                  query += param(innerObj) + '&';
                }
              }
              else if(value !== undefined && value !== null)
              {
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
              }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
          };

          return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];
      };



      return api;
    });

});
