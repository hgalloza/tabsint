/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* global CordovaSphinx, FileError */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.gitlab', [])

    .factory('gitlab', function($http, logger, disk, $q, file, paths, config, app, $cordovaFile, 
                                notifications, tasks, protocol, devices, gettextCatalog) {

      var gitlab = {};

      // model constants
      gitlab.api = 'api/v4/';             // api version of gitlab to use
      gitlab.tempDir = paths.dir('tmp');  // place to put temp files

      /**
       * Initiation method to run when the app first loads
       */
      gitlab.$init = function() {

        // backwards compatibility
        if (!disk.servers.gitlab) {
          disk.servers.gitlab = {};
        }

        // set all disk values to config values if they are not defined
        function getConfig(key) {
          if (!disk.servers.gitlab[key] && config.gitlab[key]) {
            disk.servers.gitlab[key] = config.gitlab[key];
          }
        }

        getConfig('host');
        getConfig('namespace');
        getConfig('token');


        // make sure host name path is correct
        if (disk.servers.gitlab.host) {
          disk.servers.gitlab.host = paths.https(disk.servers.gitlab.host);
        }

        logger.debug('Gitlab server initialized to: ' + angular.toJson(disk.servers.gitlab));
      };

      /**
       * Init gitlab server with defaults from config file
       */
      gitlab.initDefault = function(){

        // reset all disk values to config values, even if they are already defined
        function getConfig(key) {
          if (config.gitlab[key]) {
            disk.servers.gitlab[key] = config.gitlab[key];
          }
        }

        getConfig('host');
        getConfig('namespace');
        getConfig('token');

        // make sure host name path is correct
        if (disk.servers.gitlab.host) {
          disk.servers.gitlab.host = paths.https(disk.servers.gitlab.host);
        }

        logger.debug('Gitlab server re-initialized to: ' + angular.toJson(disk.servers.gitlab));
      };

      /**
       * Sets or Resets disk.servers.gitlab.results* variables
       * when the user selects to use seperated results repository
       */
      gitlab.toggleSeparateResultsRepo = function() {
        if (disk.gitlab.useSeparateResultsRepo) {

          // set default results group to the protocol group
          if (angular.isUndefined(disk.servers.gitlab.resultsGroup) || disk.servers.gitlab.resultsGroup === '') {
            disk.servers.gitlab.resultsGroup = disk.servers.gitlab.namespace;
          }

          // set default results repository to 'results'
          if (angular.isUndefined(disk.servers.gitlab.resultsRepo) || disk.servers.gitlab.resultsRepo === '') {
            disk.servers.gitlab.resultsRepo = 'results';
          }
        }
      };

      /**
       * Download and return a repository metadata object for a gitlab repository
       * Errors will NOT get handled with notifications at the end of this method
       * See @link https://github.com/gitlabhq/gitlabhq/blob/master/doc/api/projects.md Gitlab Projects repository objects
       * @param {string} host - gitlab host (i.e. https://gitlab.com/)
       * @param {string} group - Repository group (or subgroups, seperated by "/")
       * @param {string} name - Repository name
       * @param {string} token - API token.
       * @param {string} [version=undefined] - Repository version. Will get the latest if none is specified
       * @param {string} [type='repository'] -   Group to associate the repository with
       * @return {promise} Promise to add repository
       */
      gitlab.add = function(host, group, name, token, version, type) {

        type = type || 'repository';                    // optional type that can be used later to group repositories

        // make sure all inputs are defined
        if (!host || !group || !name || !token || !type) {
          logger.error(`Incorrect inputs to gitlab.add ${host} ${group} ${name} ${token} ${type}`);
          return $q.reject({code: 1501, msg: `${gettextCatalog.getString('TabSINT needs more information to add the Gitlab repository:')}\n\n- ` +
                                                  `${gettextCatalog.getString('Host:')} ${host}\n- ${gettextCatalog.getString('Group:')} ${group}\n- ` +
                                                  `${gettextCatalog.getString('Repository:')} ${name}\n- ${gettextCatalog.getString('Token:')} ${token}` });
        }

        // clean up path names
        host = paths.https(host);
        group = paths.dir(group);
        name = paths.file(name);
        var repoPath = paths.dir(group) + paths.file(name);

        tasks.register('gitlab',  `${gettextCatalog.getString('Adding repository')} ${repoPath}`);

        // try to get the project ( / is encoded %2F)
        var url = `${host}${gitlab.api}projects/${encodeURIComponent(repoPath)}`;
        return gitlab.get(url, token)
          .then(function(ret) {
            var repo = ret.data;
            // store configuration on `repo` object
            repo.type = type;
            repo.group = group;
            repo.host = host;
            repo.token = token;
            return checkout(repo, version);
          })
          .then(function(repo) {
            logger.info(`Successfully checked out repository: ${repo.path_with_namespace} to version: ${repo.version}`);

            // return the repository to the caller for storage/display
            return repo;
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      };


      /**
       * Wrapper for $http.get request. If the response is not an object, it will get rejected.
       * Errors will NOT get handled with notifications in this method
       * @param {string} url - url for get request.  Must already have host and api prepended.
       * @param {string} token - API token
       * @returns {promise} Promise for HTTP GET return object
       */
      gitlab.get = function(url, token) {

        // make sure all inputs are defined
        if (!token) {
          logger.error('No gitlab token defined in GET request');
          return $q.reject({code: 1510, msg: `${gettextCatalog.getString('TabSINT requires an API token to submit a GET request to the Gitlab server.')}`});
        }

        // perform a get request with TOKEN in the headers
        return $http.get(url, { headers: {
            'PRIVATE-TOKEN': token
          }})
          .then(function(response, url) { return filterGitlabResponse(response, url); })
          .catch(function(e){ return filterGitlabErrorResponse(e, url); });
      };

      /**
       * Wrapper for $http.post request. If the response is not an object, it will get rejected.
       * Errors will NOT get handled with notifications in this method
       * @param {string} url - url for POST request
       * @param {object} data - data object to post
       * @param {string} [token] - API token. Uses disk default if not provided.
       * @returns {promise} Promise for get return object
       */
      gitlab.post = function(url, data, token) {

        // make sure all inputs are defined
        if (!token) {
          logger.error('No gitlab token defined in POST request');
          return $q.reject({code: 1512, msg: `${gettextCatalog.getString('TabSINT requires an API token to submit a POST request to the Gitlab server.')}`});
        }

        // set default for data
        data = data || {};

        // perform a POST request with TOKEN in headers
        return $http.post(url, angular.toJson(data), { headers: {
            'PRIVATE-TOKEN': token,
            'Content-Type': 'application/json'
          }})
          .then(function(response, url) { return filterGitlabResponse(response, url); })
          .catch(function(e){ return filterGitlabErrorResponse(e, url); });
      };

      /**
       * Get file list and download updated files.
       * This method will notify user of success via an alert.
       * Errors are NOT handled with notifications
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @return {promise} Returns promise to update all changes, resolves with the repository object
       */
      gitlab.pull = function(repo) {
        var newVersion, newCommit, previousVersion;

        // check inputs
        if (!repo) {
          logger.error('No repository specified during pull');
          return $q.reject({code: 1540, msg: `${gettextCatalog.getString('TabSINT needs more information to update the Gitlab repository. ' +
                                                              'Please submit your logs.')}`});
        }

        var root = paths.data('');
        tasks.register('gitlab', `Updating repository ${repo.path_with_namespace}`);

        return getRepositoryState(repo, undefined)   // don't pass in any version to update
          .then(function(obj) {
            var term = obj.commit ? 'name':'short_id';   // only 'commit' defined in getTag - short_id comes back from commit, name comes back from tag
            if (repo.version !== obj[term]) {
              previousVersion = angular.copy(repo.version);
              newVersion = obj[term];
              newCommit = obj.commit || obj.short_id;   // save new commit id (even if its just the short_id)
              return compare(repo, previousVersion, newVersion);
            } else {
              logger.info(`Repository ${repo.path_with_namespace} is up to date`);
              return $q.reject({code: 1541, msg: `${gettextCatalog.getString('Repository')} ${repo.path_with_namespace} ${gettextCatalog.getString('is up to date')}`});
            }
          })
          .then(function(comp) {
            return $cordovaFile.copyDir(root, paths.gitlab(repo), root, gitlab.tempDir)
              .then(function() {
                logger.debug('Successfully copied repository to temp directory');
                return handleDiffs(repo, comp);
              });
          })
          .then(function() {
            logger.debug('Successfully handled diffs');
            return $cordovaFile.copyDir(root, paths.gitlab(repo), root, paths.dir('bak'))  // back up original
              .then(function() {
                return file.removeDirRecursively(paths.gitlab(repo));  // remove original
              });
          })
          .then(function() {
            return $cordovaFile.copyDir(root, gitlab.tempDir, root, paths.gitlab(repo)); // copy updated temp directory back to original location
          })
          .then(function() {
            // save version
            repo.previousVersion = previousVersion;
            repo.version = newVersion;
            repo.commit = newCommit;

            logger.debug(`Successfully updated repository ${repo.path_with_namespace} to version ${repo.version}`);
            notifications.alert(`${gettextCatalog.getString('Successfully updated repository to version:')} ${repo.version}`);

            // return updated repository object
            return repo;
          })
          .catch(function(e) {

            // handle up to date repositories
            if (e && e.code === 1541) {
              notifications.alert(e.msg);
              return repo;
            } else {
              return $q.reject(e);
            }
          })
          .finally(function() {
            tasks.deregister('gitlab');

            // clean up temp directories
            file.removeDirRecursively(gitlab.tempDir)
              .then(function() {
                file.removeDirRecursively(paths.dir('bak')); // remove backup
              });
          });
      };

      /**
       * Defines protocol from gitlab repository
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       */
      gitlab.defineProtocol = function(repo) {
        return protocol.define({
          group: repo.group,
          name: repo.name,
          path: paths.data(paths.gitlab(repo)),
          id: repo.id,
          date: repo.last_activity_at,
          version: repo.version,
          creator: repo.creator_id,
          server: 'gitlab',
          repo: repo,
          hash: repo.commit
        });

      };

      /**
       * Commit text file to repository
       * @param {string} host - gitlab host (i.e. https://gitlab.com/)
       * @param {string} group - Repository group (or subgroups, seperated by "/").
       * @param {string} name - Repository name.
       * @param {string} token - API token
       * @param {object} data - content to commit
       * @param {string} [toPath] - where to put file in the repository (can be multiple directories deep)
       * @param {string} [msg] - commit message. Defaults to "TabSINT commit to repository"
       * @param {string} [branch] - branch to commit to. Defaults to master
       */
      gitlab.commit = function(host, group, name, token, data, toPath, msg, branch) {

        // apply defaults
        toPath = toPath || `${devices.shortUUID}_${(new Date()).toJSON().replace(':', '-').replace(':', '-').replace('.', 'm').replace('Z', '')}.json`;   // this is overridden during the test results upload
        msg = msg || 'TabSINT commit to repository';
        branch = branch || 'master';

        // make sure all inputs are defined
        if (!host || !group || !name || !token || !data) {
          logger.error(`Incorrect inputs to gitlab.commit ${host} ${group} ${name} ${token} ${angular.toJson(data)}`);
          return $q.reject({code: 1550, msg: `${gettextCatalog.getString('TabSINT needs more information to commit data to Gitlab:')}\n\n- ` +
                                                  `${gettextCatalog.getString('Host:')} ${host}\n- ${gettextCatalog.getString('Group:')} ${group}\n- ` +
                                                  `${gettextCatalog.getString('Repository:')} ${name}\n- ${gettextCatalog.getString('Token:')} ${token}` });
        }

        // clean up path names
        host = paths.https(host);
        group = paths.dir(group);
        name = paths.file(name);
        var repoPath = `${paths.dir(group)}${paths.file(name)}`;

        // create post package
        var pkg = {
          branch: branch,
          commit_message: msg,
          author_email: 'tabsint@creare.com',
          author_name: 'tabsint',
          actions: [
            {
              action: 'create',
              file_path: toPath,
              content: angular.toJson(data)
            }
          ]
        };
        logger.info(`Committing data to path: ${pkg.actions[0].file_path} in repository: ${repoPath}`);

        // get the project id of repoPath, then post data to that project id
        var url = `${host}${gitlab.api}projects/${encodeURIComponent(repoPath)}`;
        return gitlab.get(url, token)
          .then(function(ret) {
            url = `${host}${gitlab.api}projects/${ret.data.id}/repository/commits`;
            return gitlab.post(url, pkg, token);
          })
          .then(function(){
            logger.info(`Successfully committed data to repository: ${repoPath}`);
          });
      };

      /**
       * Internal handler for gitlab HTTP responses
       * @param  {object} response - response object from http calls to gitlab
       * @param  {string} url - url called
       * @return {Promise} Rejected or Resolved promise based on response
       */
      function filterGitlabResponse(response, url) {
        if (typeof(response.data) !== 'object') {
          logger.error(`Data returned from gitlab url: ${url} was not an object`);
          return $q.reject({code: 1513, msg: `${gettextCatalog.getString('TabSINT received an irregular response from the Gitlab server. ' +
                                                                          'Please submit your logs.')}`});
        } else {
          return $q.resolve(response);
        }
      }

      /**
       * Internal handler for gitlab HTTP error responses
       * @param  {object} e - error response object from http calls to gitlab
       * @param  {string} url - url called
       * @return {Promise} Rejected or Resolved promise based on response
       */
      function filterGitlabErrorResponse(e, url) {
        // log errors
        logger.error(`Failure while trying to access url: ${url}`);
        if (e && e.data && e.data.error) {
          logger.error(`Error returned from gitlab: ${JSON.stringify(e.data.error)}`);
        }

        // returned handled errors
        if (e && e.msg) {
          return $q.reject(e);
        } else if (e && e.status === 400) {
            return $q.reject({code: 1540, msg: `${gettextCatalog.getString('The result cannot be committed to your Gitlab repository. Please check that your result is not already uploaded. ' +
                                                                        'If it is already uploaded, please manually delete this result from the Completed Tests section.')}`, err: e});
        } else if (e && e.status === 401) {
          return $q.reject({code: 1514, msg: `${gettextCatalog.getString('The Token entered in your Gitlab Configuration is not authorized to access this group/repository. ' +
                                                                        'Please check that your Token is entered correctly.')}`, err: e});
        } else if (e && e.status === 404) {
          return $q.reject({code: 1515, msg: `${gettextCatalog.getString('The Gitlab repository could not be found. ' +
                        'Please make sure the Group entered in your Gitlab Configuration is correct, and that the repository exists.')}`, err: e});
        } else {
          return $q.reject({code: 1516, msg: `${gettextCatalog.getString('TabSINT is having trouble accessing the Gitlab server. ' +
                                      'Please submit your logs.')}`, err: e});
        }
      }

      /**
       * Wrapper for file-transfer download requests
       * Errors will NOT get handled with notifications in this method
       * @param {string} url - url to download from
       * @param {string} token - API token
       * @param {string} filename - filename to download to
       * @param {string} [dir=file.fs.root.nativeURL] - directory to download into (see `file.download` in services/cordova/file/file.js for details)
       * @returns {promise} Promise for get return object
       */
      function download(url, token, filename, dir) {

        // make sure all inputs are defined
        // make sure all inputs are defined
        if (!url || !token || !filename ) {
          logger.error(`Incorrect inputs to gitlab.download ${url} ${token} ${filename} `);
          return $q.reject({code: 1517, msg: `${gettextCatalog.getString('TabSINT needs more information to download files from the Gitlab repository. ' +
                                                                                'Please submit your logs.')}`});
        }

        return file.download(url, filename, { headers: { 'PRIVATE-TOKEN': token } }, dir)
          .then(null, null, function(msg) {   // download progress
            tasks.register('gitlab',  msg);
          })
          .catch(function(e) {
            logger.error(`Failure while trying to download files from gitlab repostiory: ${url}`);
            return $q.reject({code: 1518, msg: `${gettextCatalog.getString('TabSINT failed to download files from the Gitlab repository. ' +
                                                                                'Please submit your logs.')}`});
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      }


      /**
       * Wrapper for gitlab compare
       * @param {object} repo - repository object returned from `gitlab.add`
       * @param {string} a - tag, commit sha, or branch to compare (old)
       * @param {string} b - tag, commit sha, or branch name to compare (new)
       * @returns {promise} Promise for return comparison array
       */
      function compare(repo, a, b) {

        // check inputs
        if (!repo || !a || !b) {
          logger.error('Not enough input arguments for gitlab repository comparison');
          return $q.reject({code: 1520, msg: `${gettextCatalog.getString('TabSINT needs more information to compare Gitlab repository states. ' +
                                                              'Please submit your logs.')}`});
        }

        var url = `${repo.host}${gitlab.api}projects/${repo.id}/repository/compare?from=${a}&to=${b}`;
        tasks.register('gitlab', `Comparing repository version ${a} to version ${b}`);
        return gitlab.get(url, repo.token)
          .catch(function(e) {
            return $q.reject({code: 1521, msg: `${gettextCatalog.getString('TabSINT failed to to compare Gitlab repository states. ' +
                                                                  'Please submit your logs.')}`});
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      }

      /**
       * Download a repository from a certain tag or commit
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @param {string} [version=undefined] - tag/commit sha to check out, defaults to the latest tag
       * @return {promise} Returns promise to checkout repository, resolves with the repository object
       */
      function checkout(repo, version) {

        // check inputs
        if (!repo) {
          logger.error('No repository specified during checkout');
          return $q.reject({code: 1530, msg: `${gettextCatalog.getString('TabSINT needs more information to checkout the Gitlab repository. ' +
                                                              'Please submit your logs.')}`});
        }

        // clean up path names
        var root = paths.data('');                              // root download directory (for temp files)
        var dest = `gitlab/${paths.dir(repo.namespace.path)}`;  // containing folder destination, i.e. 'gitlab/hffd/'

        return getRepositoryState(repo, version)
          .then(function(obj) {

            // handle different responses that come from `getTag` or `getCommit`
            repo.commit = obj.commit ? obj.commit.id : obj.id;    // obj.commit will be defined if the response come from `getTag`
            repo.version = obj.commit ? obj.name : obj.short_id;  // obj.commit will be defined if the response come from `getTag`

            var url = `${repo.host}${gitlab.api}projects/${repo.id}/repository/archive.zip?sha=${repo.commit}`;
            tasks.register('gitlab', `Downloading repository at version ${repo.version}`);
            return download(url, repo.token, 'archive.zip');
          })
          .then(function() { return file.createDirRecursively(paths.data(''), dest); })
          .then(function() {
            logger.info('Downloaded repository archive');
            tasks.register('gitlab', 'Processing repository');
            var q = $q.defer();

            zip.unzip(`${root}archive.zip`, `${root}${dest}`,
              function(ret) {
                if (ret === 0) {  // success
                  logger.info('Unzipped repository archive');
                  repo.dir = paths.dir(`${repo.name}-${repo.commit}-${repo.commit}`); // save crazy filename for later use...
                  q.resolve();
                } else {  // failure
                  logger.error('Failed to unzip gitlab repository archive');
                  return $q.reject({code: 1531, msg: `${gettextCatalog.getString('TabSINT failed to unzip repository archive downloaded from Gitlab.' +
                                                              'Please submit your logs.')}`});
                }
              }, function(progress) {   // unzip progress
                tasks.register('gitlab', `Processing ${repo.type}: ${Math.round((progress.loaded / progress.total) * 100)}%`);
              });
            return q.promise;
          })
          .then(function() {return file.list(root, `${dest}${repo.dir}`); })
          .then(function() {
            // try removing archive file downloaded.  If this fails, it will get overwritten next it is downloaded
            try {
              file.removeFile('archive.zip');
            } catch(e) {}

            // finally return the repo (the finally block does not modify this value)
            return repo;
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      }

      /**
       * Handler to decide if gitlab should use the tag handler or commit handler
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @param {string} [version=undefined] - version (tag, commit) to check out, defaults to the latest
       * @return {promise}         Promise that resolves with either getTag or getCommit
       */
      function getRepositoryState(repo, version){

        // check inputs
        if (!repo) {
          logger.error('No repository specified during getRepositoryState');
          return $q.reject({code: 1532, msg: `${gettextCatalog.getString('TabSINT needs more information to determine the Gitlab repository state. ' +
                                                              'Please submit your logs.')}`});
        }

        if (disk.gitlab.useTagsOnly) {
          return getTag(repo, version, false);
        } else if (version) {
          // if version is specified, try getting the version as a tag first, then try getting the version as a commit
          return getTag(repo, version, true)   // get strict tag version
            .catch(function() {return getCommit(repo, version, false);});
        } else {
          return getCommit(repo, version, false);
        }
      }

      /**
       * Get a commit from the repository
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @param {string} sha - commit hash to download. If none provided, it will get the latest
       * @param {boolean} [strict=false] - fail if we don't find this exact tag
       * @return {Promise} Promise that resolves with the commit object (or the most recent commit object) (obj.short_id is the commit ref)
       */
      function getCommit(repo, commit, strict) {

        // check inputs
        if (!repo) {
          logger.error('No repository specified during getCommit');
          return $q.reject({code: 1533, msg: `${gettextCatalog.getString('TabSINT needs more information to get the commit from the Gitlab repository. ' +
                                                              'Please submit your logs.')}`});
        }

        tasks.register('gitlab', `Getting repository at version: ${commit || 'latest'}` );
        var url = `${repo.host}${gitlab.api}projects/${repo.id}/repository/commits`;
        return gitlab.get(url, repo.token)
          .then( function(ret) {
            if (ret.data.length > 0) {
              var idx = _.findIndex(ret.data, function(d) { return d.id === commit }); // return the commit that contains an id with all or part of the input 'commit'
              if (idx !== -1) {
                return ret.data[idx];  // return specific commit value
              } else if (!strict) {
                return ret.data[0];   // return the latest (first in the list, confirmed) commit value
              } else {
                logger.error(`The commit ${commit} was not found in repository: ${repo.path_with_namespace}`);
                return $q.reject({code: 1534, msg: `${gettextCatalog.getString('TabSINT could not find version ${commit} in the repository. ' +
                                                      'Please check your Gitlab configuration and make sure the commit version is listed in the protocol repository')}`});
              }
            } else {
              logger.error(`No commits found in repository: ${repo.path_with_namespace}`);
              return $q.reject({code: 1535, msg: `${gettextCatalog.getString('TabSINT could not find any commits listed in the repository. ' +
                                                    'Please check your Gitlab configuration and make sure the protocol files are stored in your repository.')}`});
            }
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      }

      /**
       * Get tag object for tag string. If no tag string input, returns the most recent repository tag.
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @param {string} tagString - tag to check out
       * @param {boolean} [strict=false] - fail if we don't find this exact tag
       * @return {promise} Promise that resolves with the tag object (or the most recent tag object) (tag.name is the tag name)
       */
      function getTag(repo, tagString, strict) {

        // check inputs
        if (!repo) {
          logger.error('No repository specified during getCommit');
          return $q.reject({code: 1536, msg: `${gettextCatalog.getString('TabSINT needs more information to get the tag from the Gitlab repository. ' +
                                                              'Please submit your logs.')}`});
        }

        tasks.register('gitlab',  `Getting repository at version: ${tagString || 'latest'}`);
        var url = `${repo.host}${gitlab.api}projects/${repo.id}/repository/tags`;
        return gitlab.get(url, repo.token)
          .then( function(ret) {
            if (ret.data.length > 0) {
              var idx = _.findIndex(ret.data, function(d) { return d.name === tagString; });
              if (idx !== -1) {
                return ret.data[idx];  // return specific tagString
              } else if (!strict) {
                var commits = _.map(ret.data, 'commit');
                var dates = _.map(commits, function(c) {
                  var d = new Date(c.committed_date).toJSON();
                  return d;
                });
                var maxVersion = angular.copy(dates).sort().reverse()[0]; //sorts in ascending order, need to flip with reverse...
                var ind = _.findIndex(dates, function(d) {return d === maxVersion});
                logger.info(`Gitlab getTag returning the most recent tag: ${ret.data[ind].name}`);
                return ret.data[ind];
              } else {
                logger.error(`The tag ${tagString} was not found in repository: ${repo.path_with_namespace}`);
                return $q.reject({code: 1537, msg: `${gettextCatalog.getString('TabSINT could not find the tag ${tagString} in the repository. ' +
                                                      'Please check your Gitlab configuration and make sure the tag is assigned to the protocol repository')}`});
              }
            } else {
              logger.error(`No tags found in repository: ${repo.path_with_namespace}`);
              return $q.reject({code: 1535, msg: `${gettextCatalog.getString('TabSINT could not find any tags listed in the repository. ' +
                                                    'Please check your Gitlab configuration and make sure the tag is assigned to the repository. ' +
                                                    'You may also unselect "Only Track Tags" option on the Config page to search by commit version.')}`});
            }
          })
          .finally(function() {
            tasks.deregister('gitlab');
          });
      }


      /**
       * Handle single diff in the gitlab temp directory (defined in gitlab.tempDir)
       * @param {object} repo - repository object (return.data object) returned from gitlab.get(project) API
       * @param {object} comp - comparison object returned from compare
       * @return {promise} Returns promise to update single diff
       * @instance
       */
      function handleDiffs(repo, comp) {

        // check inputs
        if (!repo || !comp) {
          logger.error('No repository or comparison specified during getCommit');
          return $q.reject({code: 1536, msg: `${gettextCatalog.getString('TabSINT needs more information to handle the differences between Gitlab repository states. ' +
                                                              'Please submit your logs.')}`});
        }

        var q = $q.defer();
        var promise = q.promise; // collects promises throughout

        // set path to temp working directory
        var root = paths.data('');
        tasks.register('gitlab', 'Updating repository to new version');

        // delete files that have been deleted
        _.forEach(comp.data.diffs, function(diff) {
          if (diff.deleted_file) {
            promise = promise.then(function() {
              tasks.register('gitlab', 'Removing repository files');
              logger.debug(`Gitlab removing file: ${diff.old_path}`);
              return $cordovaFile.removeFile(root, `${gitlab.tempDir}${diff.old_path}`);
            });
          }
        });

        // rename / move files
        _.forEach(comp.data.diffs, function(diff) {
          if (diff.renamed_file || (diff.new_path !== diff.old_path)) {
            promise = promise.then(function() {
              tasks.register('gitlab', 'Renaming/Moving repository files');
              logger.debug(`Gitlab moving file from: ${diff.old_path} to: ${diff.new_path}`);
              return $cordovaFile.moveFile(root, `${gitlab.tempDir}${diff.old_path}`, root, `${gitlab.tempDir}${diff.new_path}`);
            });
          }
        });

        // download files that are not renamed, deleted, or moved
        if (comp.data.commit) {
          var urlBase = `${repo.host}${gitlab.api}projects/${repo.id}/repository/files/`;
          _.forEach(comp.data.diffs, function (diff) {
            if (!(diff.renamed_file || diff.deleted_file || (diff.new_path !== diff.old_path))) {
              promise = promise.then(function () {
                tasks.register('gitlab', 'Downloading new repository files');
                logger.debug(`Gitlab downloading file: ${diff.new_path}`);
                var url = `${urlBase}${encodeURIComponent(diff.new_path).replace('.', '%2E')}/raw?ref=${comp.data.commit.id}`;
                return download(url, repo.token, `${gitlab.tempDir}${diff.new_path}`, undefined);
              });
            }
          });
        } else {
          logger.debug(`new tag points to the same commit as the old tag`);
        }

        // finally, deregister any gitlab tasks
        promise = promise.finally(function() {
          tasks.deregister('gitlab');
        });

        // start the promise chain
        q.resolve();
        return promise;
      }

      return gitlab;

    });
});
