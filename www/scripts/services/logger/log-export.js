/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.log-export', [])

    .factory('logExport', function (logger, $q, sqLite, $cordovaFile, disk, notifications, tasks, tabsintServer, 
                                    networkModel, paths, file, remote, gettextCatalog, devices, config, page, version) {
      // Service to load json files synchronously

      var logExport = {};

      /**
       * Export logs to the appropriate output location
       * @param  {boolean} userCalled - boolean to determine if the log upload process was initiated by a user
       * @return {promise} - promise to upload logs
       * @deprecated
       */
      logExport.export = function(userCalled) {
        return logExport.upload(userCalled);
      };

      /**
       * Method to save logs as local file
       * @param  {boolean} userCalled - boolean to determine if the log upload process was initiated by a user
       *
       */
      logExport.save = function(userCalled) {

        // if no logs, resolve ? todo
        if (logger.getCount.logs === 0) {
          return  $q.reject();
        }

        // if already exporting, hold off
        if (tasks.isOngoing('upload_logs') ) {
          if (userCalled){
            notifications.alert(gettextCatalog.getString('Log export already in process'));
          }
          return $q.reject();
        }

        // log the start of this method
        if (userCalled){
          logger.info('Exporting logs');
        } else {
          logger.info('Automatically exporting logs');
        }

        var d = (new Date()).toISOString().replace(':','_').replace(':', '-').split('.')[0];
        var filename =  d + '.json';

        function writeLogs(logsFromSQLite) {
          var q = $q.defer();
          var ret = {
            logs: []
          };

          // put each log message into return object
          _.forEach(logsFromSQLite, function(log){
            ret.logs.push(log);
          });

          file.createDirRecursively(paths.public(''), '.tabsint-logs')
            .then(function() {
              return $cordovaFile.writeFile(paths.public('.tabsint-logs'), filename, angular.toJson(ret));
            })
            .then(function() {
              logger.info('Successfully wrote logs to file: ' + filename);
              return sqLite.drop('newlogs');
            }, function(e) {
                logger.error('Failed to write log file with error: ' + angular.toJson(e));
                return $q.reject('Failed to write log file');
            })
            .then(function(){
              q.resolve();
            });

          return q.promise;
        }

        return logger.count()
          .then(sqLite.prepareForUpload('newlogs'))
          .then(function() {return sqLite.getAll('newlogs');})
          .then(writeLogs)
          .catch(function(err) {
            logger.error('The tablet encountered an issue while exporting logs: ' + angular.toJson(err));
            if (userCalled) {
              notifications.alert(gettextCatalog.getString('The tablet encountered an issue while exporting log messages.')+'\n\n'+gettextCatalog.getString('Details: ') + angular.toJson(err));
            }
          })
          .finally(function () {
            logger.info('Successfully exported logs');
            tasks.deregister('upload_logs');
          });
      };

     /**
     * Method to submit logs to the server
     * @param  {boolean} userCalled - boolean to determine if the log upload process was initiated by a user
     * @return {promise to submit logs}
     */
      logExport.upload = function(userCalled)  {

        // Preliminary Checks
        if ( logger.getCount.logs === 0 ) {
          return  $q.reject();
        }

        if ( tasks.isOngoing('upload_logs') ) {
          if (userCalled){
            notifications.alert(gettextCatalog.getString('Log upload already in process'));
          }
          return $q.reject();
        }

        if (!networkModel.status) {
          logger.warn('network.status is false while trying to upload logs');
          if (userCalled) {
            notifications.alert(gettextCatalog.getString('The tablet is not currently connected to a wifi or cellular network. Please check your network connection'));
          }
          return $q.reject();
        }

        if (userCalled){
          // if the user selects to upload a log, it likely means there is an issue, so we grab some features of their build to make debugging easier
          logger.debug('LOGUPLOAD: ' + angular.toJson({
            uuid: devices.UUID,
            model: devices.model,
            build: config.build,
            version: version.dm.tabsint,
            buildDate: version.dm.date,
            buildRev: version.dm.rev,
            protocolName: disk.protocol ? disk.protocol.name : undefined,
            server: disk.server,
            pageData: page.dm,
            pageResult: page.result
          }));
        } else {
          logger.info('Automatically uploading logs');
        }

        // initialize process
        var totalLogs = 0, logsUploaded = 0;
        tasks.register('upload_logs', 'Uploading log messages(s)...');

        // upload logs
        return logger.count()
          .then(function(n) {
            totalLogs = n;
            logger.info('Counted ' + totalLogs + ' logs to upload');
          })
          .then(sqLite.prepareForUpload('newlogs'))
          .then(sendLogPackage)
          .then(logger.count)
          .then(function(n) {
            logsUploaded = totalLogs - n;
          })
          .catch(function(e) {
            logger.error('The tablet encountered an issue while uploading logs: ' + angular.toJson(e));
            if (userCalled) {
              notifications.alert(gettextCatalog.getString('The tablet encountered an issue while uploading log messages. ') + logsUploaded + gettextCatalog.getString(' logs were successfully uploaded. Please export your logs and file an issue at')+' https://gitlab.com/creare-com/tabsint');
            }
          })
          .finally(function () {
            logger.info('Succesfully uploaded ' + logsUploaded + ' logs');
            tasks.deregister('upload_logs');
          });

        /** Internal method to send all logs to the server recursively */
        function sendLogPackage() {
          var q = $q.defer();

          // uploaded each set of 50 logs
          function uploadLogChunk(logsFromSQLite) {
            var url = 'https://hp.crearecomputing.com/tabsint/logs';
            var auth = btoa('creare-post:cn2093nw04vna0w348a');

            // calculate properties based on inputs and constants
            var headers = {
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + auth
            };

            // check network status
            if (!networkModel.status) {
              q.reject('The tablet lost network connection while uploading logs');
            }

            if (logsFromSQLite.length > 0){
              var data = angular.toJson(logsFromSQLite);  // this will strip the $$hashKey generated by angular in displaying the logs
              var ids = _.map(logsFromSQLite, function(log) {return log.msgID; });
              return remote.post(url, data, {headers: headers})
                .then(function() {return deleteRows(ids);})
                .then(processBatch);
            } else {
              q.resolve();  // finished uploading
            }

          }

          // function to delete from sqlite
          function deleteRows(idList){
            var deleteRowPromises = [];
            idList.forEach(function(id){ // MUST create a function scope for each promise, otherwise they overwrite
              var promise = sqLite.deleteLogs(id);
              deleteRowPromises.push(promise);
            });
            return $q.all(deleteRowPromises); // return once all posted rows have been purged
          }

          // wrapper for each batch
          function processBatch(){
            sqLite.get('newlogs')
              .then(uploadLogChunk)
              .catch(function(e){
                q.reject(e);
              });
          }

          // start the first batch process, will get recursively each round
          processBatch();
          return q.promise;
        }
      };




      return logExport;
    });

});
