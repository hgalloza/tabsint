/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.logger', [])

    .factory('logger', function(sqLite, disk) {
      var api = {
        initialize: undefined,
        log: undefined,
        getLogs: undefined,
        clearLogs: undefined,
        count: undefined,
        numLogs: undefined,
        param:{
          build: null,
          siteId: null,
          uuid: null,
          version: null,
          platform: null,
          model: null,
          os: null,
          other: null
        },
        testing: {
          insertSQL: undefined,
          selectSQL: undefined,
          deleteSQL: undefined
        }
      };

      function pad(val, n){
        var ret = val+'';
        for (var i = n-1; i > 0; i--){
          if (val < Math.pow(10,i)){
            ret = '0'+ret;
          }
        }
        return ret;
      }

      api.getLogs = function(){
        return sqLite.getAll('newlogs');
      };

      api.clearLogs = function(){
        return sqLite.drop('newlogs');
      };

      api.getCount = sqLite.numLogs;

      api.count = function(){
        return sqLite.count('newlogs');
      };

      api.numLogs = function() {
        return sqLite.numLogs;
      };

      function getDateString() {
        var d = new Date();
        var month = pad(d.getMonth()+1,2);
        var date = pad(d.getDate(),2);
        var hours = pad(d.getHours(),2);
        var minutes = pad(d.getMinutes(),2);
        var seconds = pad(d.getSeconds(),2);

        var ds = d.getFullYear()+'-'+month+'-'+date+
          ' '+hours+':'+minutes+':'+seconds;
        return ds;
      }

      api.log = function (msg) {
        var type = 'log';

        if (!disk.disableLogs) {
          console.log(msg);
          if (typeof(msg) !== 'string') {
            msg = angular.toJson(msg);
          }
          var ds = getDateString();
          sqLite.store('newlogs', ds, type, msg, api.param);
        }
      };

      api.debug = function (msg) {
        var type = 'debug';
        if (!disk.disableLogs) {
          if (typeof(msg) !== 'string') {
            msg = angular.toJson(msg);
          }
          console.log('DEBUG: ' + msg);
          var ds = getDateString();
          sqLite.store('newlogs', ds, type, msg, api.param);
        }
      };

      api.info = function (msg) {
        var type = 'info';
        if (!disk.disableLogs) {
          if (typeof(msg) !== 'string') {
            msg = angular.toJson(msg);
          }
          console.log('INFO: ' + msg);
          var ds = getDateString();
          sqLite.store('newlogs', ds, type, msg, api.param);
        }
      };

      api.warn = function (msg) {
        var type = 'warn';
        if (!disk.disableLogs) {
          if (typeof(msg) !== 'string') {
            msg = angular.toJson(msg);
          }
          console.log('WARN: ' + msg);
          var ds = getDateString();
          sqLite.store('newlogs', ds, type, msg, api.param);
        }
      };

      api.error = function (msg) {
        var type = 'error';
        if (!disk.disableLogs) {
          if (typeof(msg) !== 'string') {
            msg = angular.toJson(msg);
          }
          console.log('ERROR: ' + msg);
          var ds = getDateString();
          sqLite.store('newlogs', ds, type, msg, api.param);
        }
      };

      return api;
    });

});
