/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.tasks', [])

    .factory('tasks', function ($timeout, $q, noSleep, app, $cordovaFile, devices, file, logger) {
      // Service to keep track of active tasks and when TabSINT should be 'busy'
      // when 'busy', many buttons are disabled to prevent conflicting actions
      // Registering a task establishes busy state
      // When all tasks have been deregistered, state becomes not busy

      var tasks = {
        register: undefined,
        deregister: undefined,
        deregisterAll: undefined,
        isOngoing: undefined,
        numOngoing: undefined,
        list: {},
        busy: undefined,
        notBusy: undefined,
        disabled: undefined,
        hidden: false
      };

      /**
       * Method that runs each time a task is registered
       */
      tasks.busy = function() {
        // tasks.disabled = true;
      };

      /**
       * Method that runs each time all tasks have finished running
       */
      tasks.notBusy = function() {
        // tasks.disabled = false;
        noSleep.allowSleepAgain();
        tasks.hidden = false;  // reset the hidden boolean when tasks are over

        // get free disk space once done with operation
        if (app.tablet) {
          devices.getDiskSpace();
        }
      };
      tasks.notBusy();      // run this when the app first loads

      /**
       * Return the number of ongoing tasks
       * @return {number} - number of ongoing tasks
       */
      tasks.numOngoing = function(){
        return _.keys(tasks.list).length;
      };

      /**
       * Return if a task is currently ongoing
       * @param  {string}  task - string id of the task that may be running
       * @return {Boolean}      boolean indiciating if the current task is running
       */
      tasks.isOngoing = function(task) {
        return _.has(tasks.list, task);
      };

      /**
       * Register a new running tasks
       * @param  {string} task - string id of the task
       * @param  {string} msg  - message to display in the active tasks window
       * @return {number}      return a handle to the timeout to start the tasks
       */
      tasks.register = function(task, msg) {
        tasks.busy();
        _.debounce(noSleep.keepAwake, 1000, true);
        tasks.list[task] = msg;

        return $timeout(function(){}, 20);
      };

      /**
       * Remove a task for the current active tasks list
       * If its the last task in the queue, run the tasks.notBusy() method
       * 
       * @param  {string} task - string id of the task to remove
       * @return {[type]}      [description]
       */
      tasks.deregister = function(task) {
        delete tasks.list[task];

        if (tasks.numOngoing() < 1){
          tasks.notBusy();
        }

        return $timeout(function(){}, 20);
      };

      /** 
       * Remove all running tasks
       */
      tasks.deregisterAll = function() {
        _.forEach(tasks.list, function(key, val) {
          tasks.deregister(key);
        });
      };

      /**
       * Hide the active tasks window
       */
      tasks.hide = function() {
        tasks.hidden = true;
      };

      /**
       * Unhide the active tasks window
       */
      tasks.unhide = function() {
        tasks.hidden = false;
      };

      return tasks;
    });

});
