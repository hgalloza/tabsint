/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.json', [])

    .factory('json', function (logger, $q) {

      var json = {};

      /**
       * Synchronously load JSON data from file.
       * @param  {string} path - absolute file path. Use methods from `paths` service
       * @return {object}      - loaded json file as a javascript object. Returns undefined if file can't be loaded because it does not exist or because the file has a syntax error.
       */
      json.load = function(path) {
        var obj = {};
        $.ajax({
          'async': false,
          'cache': false,
          'global': false,
          'url': path,
          'dataType': 'json'
        })
        .done(function (data) {
          obj = data;
        })
        .fail(function () {
          logger.error('Failed to load json at path: ' + path);
          obj = undefined;
        });

        return obj;
      };

      /**
       * Asynchronously load JSON data from file.
       * @param  {string} path - absolute file path. Use methods from `paths` service
       * @return {promise}    - promise to load json file which resolves with object or rejects with error
       * @ignore
       */
      json.loadAsync = function(path) {
        var q = $q.defer();
        $.ajax({
          'async': true,
          'cache': false,
          'global': false,
          'url': path,
          'dataType': 'json'
        })
        .done(function(data) {
          q.resolve(data);
        })
        .fail(function(err) {
          logger.error('Failed to load json at path: ' + path);
          q.reject(err);
        });

        return q.promise;
      };


      return json;
    });

});
