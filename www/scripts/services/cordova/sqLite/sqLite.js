/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global alert */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.sqLite', [])   

    .factory('sqLite', function (app, cordova, $q) {
      var sqLite = {
        ready: undefined,
        open: undefined,
        drop: undefined,
        store: undefined,
        prepareForUpload: undefined,
        getLogs: undefined,
        get: undefined,
        getAll: undefined,
        count: undefined,
        numLogs: {
          newlogs: undefined,
          results: undefined
        },
        delete: undefined
      };
      var db;
      var uploadStartIndex, uploadStopIndex;

      sqLite.open = function(){
        var deferred = $q.defer();
        
        // define ready by this promise
        sqLite.ready = function() {
          return deferred.promise;
        };
        
        if (app.tablet) {  // Tablet
          cordova.ready()
              .then(function() {
                if (angular.isUndefined(window.sqlitePlugin)) {
                  alert('Cordova plugin "sqlitePlugin" is undefined');
                  deferred.reject('WARNING: Sql plugin is undefined');
                }
              })
              .then(function() {
                if (db === angular.undefined) {
                  db = window.sqlitePlugin.openDatabase({name: 'test.db', location: 1},
                      function() {
                        sqLite.numLogs.newlogs = 0;
                        sqLite.numLogs.results = 0;
                        console.log('-- Database ready.');
                        deferred.resolve();
                      },
                      function(e) {deferred.reject(e)}
                  );
                } else {
                  console.log('-- DB already defined');
                  deferred.resolve();
                }
              });
        } else {  // Browser
          db = {
            newlogs: [],
            results: [],
            ids: {newlogs: 0, results: 0}
          };
          sqLite.numLogs.newlogs = 0;
          sqLite.numLogs.results = 0;
          console.log('-- Browser ready');
          deferred.resolve();
        }

        return deferred.promise;
      };

      // run open at launch
      sqLite.open();

      sqLite.drop = function(tableName){
        var deferred = $q.defer();

        if (app.tablet) { // tablet
          sqLite.ready()
            .then(function() {
              db.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS ' + tableName, [],
                function () {
                  sqLite.numLogs[tableName] = 0;
                  deferred.resolve();
                },
                function (e) { deferred.reject(e); }
                );
              });
            });
        } else { // browser
          if (db && db.hasOwnProperty(tableName)) {
            db[tableName] = [];
            db.ids[tableName] = 0;
          }
          deferred.resolve();
        }

        return deferred.promise;
      };

      sqLite.store = function (tableName, date, type, data, param) {
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              if (tableName === 'results' || tableName === 'newlogs') {
                db.transaction(function (tx) {
                  tx.executeSql('CREATE TABLE IF NOT EXISTS '+tableName+' (msgID INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, data TEXT, type TEXT, uuid TEXT, siteID TEXT, build TEXT, version TEXT, platform TEXT, model TEXT, os TEXT, other TEXT)');
                  tx.executeSql('INSERT INTO '+tableName+' (date, data, type, uuid, siteID, build, version, platform, model, os, other) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [date, data, type, param.uuid, param.siteId, param.build, param.version, param.platform, param.model, param.os, param.other], null, null);
                  sqLite.numLogs[tableName] +=1;
                  deferred.resolve();
                });
              } else {
                deferred.reject('ERROR: Unknown table name');
              }
            });
        } else {
          if (db.hasOwnProperty(tableName)) {
            if (db[tableName] === angular.undefined){db[tableName] = [];}

            db[tableName].push({
              msgID: db.ids[tableName]++,
              type: type,
              date: date,
              data: data,
              uuid: param.uuid,
              siteID: param.siteID,
              build: param.build,
              version: param.version,
              model: param.model,
              os: param.os,
              other: param.other
            });
            sqLite.numLogs[tableName] +=1;
            deferred.resolve();
          } else {
            deferred.reject('ERROR: Unknown table name');
          }
        }

        return deferred.promise;
      };

      sqLite.prepareForUpload = function(tableName){
        var deferred = $q.defer();

        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM '+tableName+' ORDER BY msgID ASC LIMIT 1;', [], function (tx, res) {
                  uploadStartIndex = res.rows.item(0).msgID;
                  tx.executeSql('SELECT * FROM '+tableName+' ORDER BY msgID DESC LIMIT 1;', [], function (tx, res) {
                      uploadStopIndex = res.rows.item(0).msgID;
                    deferred.resolve();
                  }, function (e) { deferred.reject(e);
                  });
                }, function (e) { deferred.reject(e);
                });
              });
            });
        } else {
            if (db && db[tableName] === angular.undefined) {
                db[tableName] = [];
            }
            uploadStopIndex = 0;
            db[tableName].forEach(function (item) {
                uploadStopIndex = Math.max(uploadStopIndex, item.msgID);
            });
            uploadStartIndex = uploadStopIndex;
            db[tableName].forEach(function (item) {
                uploadStartIndex = Math.min(uploadStartIndex, item.msgID);
            });
            deferred.resolve();
        }

        return deferred.promise;
      };

      sqLite.get = function(tableName){
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM '+tableName+' WHERE msgID >= ? AND msgID <= ? ORDER BY msgID ASC LIMIT 50;', [uploadStartIndex, uploadStopIndex],
                  function (tx, res) {
                    var ret = [];
                    for (var i = 0; i < res.rows.length; i++) {
                      ret.push(res.rows.item(i));
                    }
                    deferred.resolve(ret);
                  },
                  function (e) { deferred.reject(e); }
                );
              });
            });
        } else {
          if (db && db[tableName] === angular.undefined){db.tableName = [];}
          var tmp = [];
          db[tableName].forEach(function(item){
            if (item.msgID >= uploadStartIndex && item.msgID <= uploadStopIndex){
              if (tmp.length < 20){
                tmp.push(item);
              }
            }
          });
          deferred.resolve(tmp);
        }
        return deferred.promise;
      };

      sqLite.getAll = function (tableName) {
          var deferred = $q.defer();
          if (app.tablet) {
              sqLite.ready()
                  .then(function () {
                      db.transaction(function (tx) {
                          if (tableName === 'results' || tableName === 'newlogs') {
                              tx.executeSql('SELECT * FROM '+tableName+';', [],
                                  function (tx, res) {
                                      var ret = [];
                                      for (var i = 0; i < res.rows.length; i++) {
                                          ret.push(res.rows.item(i));
                                      }
                                      deferred.resolve(ret);
                                  },
                                  function (e) { deferred.reject(e); }
                              );
                          } else {
                              deferred.reject('ERROR: Unknown table name');
                          }
                      });
                  });
          } else {
              if (db && db[tableName] === angular.undefined){db[tableName] = [];}
              var tmp = db[tableName];
              deferred.resolve(tmp);
          }

          return deferred.promise;
      };

      sqLite.count = function(tableName){
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                if (tableName === 'newlogs' || tableName === 'results') {
                  tx.executeSql('SELECT COUNT(*) AS c FROM '+tableName, [],
                    function (tx, res) {
                      sqLite.numLogs[tableName] = res.rows.item(0).c;
                      deferred.resolve(res.rows.item(0).c);
                    },
                    function (e) { deferred.reject(e); }
                  );
                } else {
                  deferred.reject('ERROR: Unknown table');
                }
              });
            });
        } else {
          if (db && db[tableName] === angular.undefined){db[tableName] = [];}
          console.log('-- DB entry count: '+db[tableName].length);
          sqLite.numLogs[tableName] = db[tableName].length;
          deferred.resolve(db[tableName].length);
        }

        return deferred.promise;
      };

      sqLite.deleteLogs = function (id) {
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                  tx.executeSql('DELETE FROM newlogs WHERE msgID = ?', [id],
                    function () {
                      sqLite.numLogs.newlogs -= 1;
                      deferred.resolve();
                    },
                    function (e) { deferred.reject(e); }
                  );
              });
          });
        } else {
          if (db && db.newlogs === angular.undefined){ db.newlogs = []; }
          var rmID;
          for (var i = 0; i < db.newlogs.length; i++){
            var item = db.newlogs[i];
            if (item.msgID === id){
              rmID = i;
            }
          }
          if (rmID !== angular.undefined){
            db.newlogs.splice(rmID,1);
            sqLite.numLogs.newlogs -= 1;
          }
          deferred.resolve();
        }

        return deferred.promise;
      };

      sqLite.deleteResult = function (date) {
          var deferred = $q.defer();
          if (app.tablet) {
              sqLite.ready()
                  .then(function () {
                      db.transaction(function (tx) {
                          tx.executeSql('DELETE FROM results WHERE date = ?', [date],
                              function (tx, res) {
                                  sqLite.numLogs.results -= 1;
                                  deferred.resolve();
                              },
                              function (e) { deferred.reject(e); }
                          );
                      });
                  });
          } else {
              if (db && db.results === angular.undefined){ db.results = []; }
              var rmID;
              for (var i = 0; i < db.results.length; i++){
                  var item = db.results[i];
                  if (item.date === date){
                      rmID = i;
                  }
              }
              if (rmID !== angular.undefined){
                  db.results.splice(rmID,1);
                  sqLite.numLogs.results -= 1;
              }
              deferred.resolve();
          }

          return deferred.promise;
      };

      return sqLite;
    });
});