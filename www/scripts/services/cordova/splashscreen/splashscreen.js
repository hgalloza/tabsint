/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global LocalFileSystem, FileTransfer */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.splashscreen', [])   

    .factory('splashscreen', function (app, logger) {

      return {
        hide: function () {
          if (app.tablet){
            navigator.splashscreen.hide();
          } else {
            console.log('DEBUG: No splashscreen on browser');
          }
        }
      };
    });
});