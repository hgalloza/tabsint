/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.tabletLocation', [])   

    .factory('tabletLocation', function (app, $q, logger, disk) {

      // API
      var tabletLocation = {};

      /**
       * Update the current position on the tablet.
       * Called every time an exam starts, and when the tablet comes online.
       * If successful, the location is updated on the `disk.tabletLocation` variable
       * If unsuccessful, nothing happens, so `disk.tabletLocation` remains unchanged
       * @return {promise} - promise to update geolocation
       */
      tabletLocation.updateCurrentPosition = function () {
        var deferred = $q.defer();

        var onSuccess = function(position) {
          if (angular.isDefined(position)) {
            disk.tabletLocation = {
              latitude: position.coords.latitude.toFixed(3), // dropping resolution to 100m
              longitude: position.coords.longitude.toFixed(3), // dropping resolution to 100m
              accuracy: position.coords.accuracy
            };
          }

          deferred.resolve();
        };

        var onError = function(e) {
          deferred.resolve();
        };

        if (app.tablet) { // tablet
          app.ready()
            .then(function () {
              if (angular.isUndefined(navigator.geolocation.getCurrentPosition)) {
                logger.error('Cordova plugin "geolocation" is undefined');
                deferred.resolve();
              } else {
                var options = { enableHighAccuracy: true, timeout: 5*60*1000, maximumAge: 3600*1000*12 };
                navigator.geolocation.getCurrentPosition( onSuccess, onError, options );
              }
            });
        } else { // Browser testing
          deferred.resolve();
        }

        return deferred.promise;
      };

      return tabletLocation;
    });
});