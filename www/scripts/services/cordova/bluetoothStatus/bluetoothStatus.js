/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.bluetoothStatus', [])   

    .factory('bluetoothStatus', function (app, cordova, logger) {
      var bluetoothStatus = {
        init: undefined,
        update: undefined,
        state: undefined,
        type: undefined
      };


      bluetoothStatus.update = function() {
        if (app.tablet) {
          bluetoothStatus.state = cordova.plugins.BluetoothStatus.BTenabled;
        } else {
          bluetoothStatus.state = true;
        }

        if (bluetoothStatus.state) {
          logger.info('Bluetooth enabled on the device');
        } else{
          logger.info('Bluetooth disabled on the device');
        }
        
      };

      // user cordova-plugin-bluetooth-status
      bluetoothStatus.$init = function() {
        if (app.tablet) {
          cordova.ready()
            .then(cordova.plugins.BluetoothStatus.initPlugin)
            .catch(function(err) {
              logger.error('Bluetooth service failed while trying to set the initiate');
              bluetoothStatus.state = undefined;
            });
        } else {
          cordova.ready()
            .then(function() {
              bluetoothStatus.state = true;
            });
          
        }
      };

      return bluetoothStatus;
    });
});
