/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular', './network.model'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.network', ['tabsint.services.cordova.network.model'])

    .factory('network', function ($q, app, cordova, logger, $window, notifications, remote, networkModel, gettextCatalog){

      var network = {};

      /**
       * Method to check the current status of the network connection
       * @return {promise} - returns promise to check network connection
       */
      network.checkStatus = function(){

        return cordova.ready()
          .then(function() {

             // test and non-tablet mocks, will automatically resolve promise
            if (app.test) {
              return;
            } else if (!app.tablet) {
              networkModel.setOffline();
              return;
            }

            // on the tablet
            if (angular.isDefined($window.navigator) && angular.isDefined($window.navigator.connection)){
              if (_.includes(['none', '2g', null, undefined, 'undefined'], navigator.connection.type) || navigator.onLine === false ) {
                networkModel.setOffline();
                return;
              } else {
                return remote.ping()
                  .then(function() {
                    networkModel.setOnline();
                  })
                  .catch(function(){
                    networkModel.setOffline(); // catch ping errors here, so checkStatus doesn't break promise chain in app.js
                  });
              }
            } else {
              logger.error('navigator.connection not defined - cannot check network status.');
              notifications.alert(gettextCatalog.getString('The tablet is having trouble checking the network status. Please file an issue at')+' https://gitlab.com/creare-com/tabsint');
              return $q.reject();
            }
          });


      };

      return network;
    });


});
