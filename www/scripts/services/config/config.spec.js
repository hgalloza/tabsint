/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['tv4','test-dep', 'app'], function (tv4) {
  'use strict';


  beforeEach(module('tabsint'));

  describe('Config', function () {
    describe('Inputs', function () {

      var configSchema, config, json;
      beforeEach(inject(function (_json_) {
        json = _json_;

        // Load config schema (sync).
        configSchema = json.load('base/config/config_schema.json');
        config = json.load('base/www/tabsintConfig.json');
      }));

      it('should pass Config JSON-SCHEMA validation.', function () {
        var valid = tv4.validate(config, configSchema);
        if (!valid) {
          console.log(angular.toJson(tv4.error, true));
        }
        expect(valid).toBeTruthy();
      });

    });

    describe('Processing', function () {

      var config;
      beforeEach(inject(function (_config_) {
        config = _config_;
      }));

      it('should have values defined for plugins, even if they are defaults', function () {
        config.load();
        expect(config.tabsintPlugins).toBeDefined();
      });


    });


  });
});
