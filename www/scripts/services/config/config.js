/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function () {
  'use strict';

  angular.module('tabsint.services.config', [])
    .factory('config', function (json, logger) {
      var api = {
        load: undefined,
        build: '',
        description: undefined,
        platform: undefined,
        releases: {},
        gitlab: {},
        server: {},
        cordovaPlugins: {},
        tabsintPlugins: {}
      };

      api.load = function() {
        // Load tabsintConfig.json and load it into the config object

        _.forEach(json.load('tabsintConfig.json'), function(value, key) {
          if (key === 'ERROR'){
            logger.error('Processing config object: ' + angular.toJson(value));
          }

          // backwards compatibility as we move from "plugins" to "tabsintPlugins"
          if (key === 'plugins') {
            key = 'tabsintPlugins';
          }
          
          api[key] = value;
        });

        // put the build name in the logger parameters
        logger.param.build = api.build;

        // configure creare specific gitlab endpoints
        api.gitlab.creareHost = 'https://gitlab.com/';
        api.gitlab.creareProtocolsGroup = 'creare-com/tabsint-protocols';
        api.gitlab.creareToken = 'GA9aq4wHiFWU5rGTeeTw';

        
        logger.info('Config object processed: ' + angular.toJson(api));
      };

      return api;
    });
});
