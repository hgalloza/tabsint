/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*jshint bitwise: false*/

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.checksum', [])
    .factory('checksum', function ($q, file, logger) {

      var checksum = {
        crc32Table: undefined,
        crc32: undefined,
        crc32ofString: undefined
      };

      // perform once.  establishes the Look Up Table for CRC32 calculations
      function makeCrc32Table() {
        var c;
        var crcTable = [];
        for (var n=0; n < 256; n++) {
            c = n;
            for (var k = 0; k < 8; k++) {
                if (c & 1) {
                  c = 0xEDB88320 ^ (c >>> 1);
                } else {
                  c = c >>> 1;
                }
            }
            crcTable[n] = c;
        }
        checksum.crc32Table = crcTable;
      }

      /*
       * Calculate the CRC32 value of an Uint8 array using the Look Up Table.
       *
       * @params arr - the array of data to use in generating a CRC32
       * @returns 32 bit checksum, decimal
       */
      function calculateCrc32(arr) {
        if (!checksum.crc32Table) {
          makeCrc32Table();
        }
        var crc = 0 ^ (-1);

        for (var i = 0; i < arr.length; i++ ) {
            crc = checksum.crc32Table[(crc ^ arr[i]) & 0xFF] ^ (crc >>> 8);
        }

        return (crc ^ (-1)) >>> 0;
      }

      // helper function to convert filename to Uint8Array
      function str2arr(str) {
        var buf = new ArrayBuffer(str.length); // 1 bytes for each char
        var bufView = new Uint8Array(buf);
        for (var i=0; i < str.length; i++) {
          bufView[i] = str.charCodeAt(i);
        }
        return new Uint8Array(buf);
      }

      checksum.crc32ofString = function(s) {
        s = s.toUpperCase(); // convert to all upper case for uniformity
        var sArray = str2arr(s);
        var crc = calculateCrc32(sArray).toString(16).toUpperCase();
        while (crc.length < 8) {crc = '0'+crc;} // occasionally the leading digits are 0's and the crc ends up too short
        return crc;
      };

      /*
       * Generate the CRC32 representation of a file, including filename and contents.
       *
       * @params filePath - local path to the file
       * @returns 32 bit checksum - as an 8 byte hex
       */
      checksum.crc32 = function(filePath) {
        var deferred = $q.defer();

        // combine filename and file data, generate CRC32
        function onFileEntry(file){
          var reader = new FileReader();
          reader.onloadend = function() {
              var data = reader.result;
              var dataArray = new Uint8Array(data);
              // prepend filename to dataArray
              var fileName = file.name.toUpperCase(); // convert to all upper case for uniformity
              var nameArray = str2arr(fileName);
              var combinedArray = new Uint8Array(dataArray.byteLength + nameArray.byteLength);
              combinedArray.set(nameArray);
              combinedArray.set(dataArray, nameArray.byteLength);
              // Calculate the crc of the combined name+data, then convert to hex
              var crc = calculateCrc32(combinedArray).toString(16).toUpperCase();
              while (crc.length < 8) {crc = '0'+crc;} // occasionally the leading digits are 0's and the crc ends up too short
              deferred.resolve(crc);
          };

          try {
            reader.readAsArrayBuffer(file);
          } catch(e) {
            deferred.reject('Could not calculate CRC: ' + JSON.stringify(e));
          }
        }

        // expose the fileEntry object
        function onGetFile(fileEntry){
          return fileEntry.file(onFileEntry, function(e){logger.warn('Could not get file to calculate CRC: ' + JSON.stringify(e)); deferred.reject(e)});
        }

        file.getFile(filePath)
            .then(onGetFile)
            .catch(function(e){
              logger.warn('Could not calculate CRC32 of file ' + filePath +'.  Error: ' + JSON.stringify(e));
            });

        return deferred.promise;
      };

      return checksum;
    });

});
