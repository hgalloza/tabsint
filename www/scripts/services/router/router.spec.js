/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['test-dep', 'app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Router', function () {

    var router;

    beforeEach(inject(function(_router_){
        router = _router_;
    }));

    it('should be initialized to WELCOME', function(){
        expect(router.page).toEqual('WELCOME');
    });

  });

});