/*jshint bitwise: false*/
/*jshint bitwise: false*/
define(['angular'], function(angular) {
  'use strict';

  angular.module('tabsint.services.svantek', [])
    .factory('svantek', function(bluetoothStatus, $q, logger, tabsintble, tasks, notifications) {

      var svantek = {
        device: undefined,
        state: 'disconnected',
        connect: undefined,
        cancelConnect: undefined,
        disconnect: undefined,
        start: undefined,
        stop: undefined,
        data: undefined
      };

      // Values from Svantek documentaiton and data advertisement
      var deviceName = 'SV 104A';
      var interval;
      var msgBuff;
      var service = '0bd51666-e7cb-469b-8e4d-2742f1ba77cc';
      var characteristics = {
        exchange: {
          uuid: 'e7add780-b042-4876-aae1-112855353cc1',
          value: undefined
        },
        firstProfile: {
          uuid: '1e7d1d51-482f-4710-84b8-4c9db3199f83',
          value: undefined
        },
        start: {
          uuid: '014e3c91-3326-488d-a20a-a2963d5984cc',
          value: undefined
        },
        pin: {
          uuid: '15da06a2-c25f-4f20-ad8f-5c2e992fba76',
          value: undefined
        }
      };


      /**
       * Write data to svantek device
       * @param  {string} characteristic - characteristric to write to
       * @param  {array} writeData      - array of values to write (i.e. UInt8Array, Int8Array, etc). must contain a writeData.buffer method
       * @return {promise}                promise to write value
       */
      function write(characteristic, writeData) {
        if (svantek.device && svantek.device.id) {
          return tabsintble.write(svantek.device.id, service, characteristic.uuid, writeData.buffer)
            .catch(function(error) {
              logger.error(`Failed to write data: ${JSON.stringify(writeData)} to svantek characteristic: ${JSON.stringify(characteristic)}`);
              return $q.reject(error);
            });
        } else {
          logger.error('Attempting to write to dosimeter, but no dosimeter is connected');
        }
      }

      /**
       * [from_ascii description]
       * @param  {string} string - input string
       * @return {Int8Array}        - Int array representing string
       */
      function from_ascii(string) {
        var bytes = [];
        for (var i = 0; i < string.length; ++i) {
          var code = string.charCodeAt(i);
          bytes = bytes.concat([code]);
          // bytes = bytes.concat([code & 0xff, code / 256 >>> 0]);
        }
        return new Int8Array(bytes);
      }

      /**
       * Convert Int array to ascii
       * @param  {UInt8Array} data - array to convert
       * @return {string}      ascii version of array
       */
      function as_ascii(data) {
        return String.fromCharCode.apply(null, data);
      }

      /**
       * Includes byte swapping, since this is used exclusively for fields that
       * need to be endian-converted.
       * @param  {Int8Array} data [description]
       * @return {?}      [description]
       */
      function as_i16_endian(data) {
        if (data.length !== 2) {
          console.log(`Input Int8Array incorrect size`);
          return;
        }
        var temp = data[0];
        data[0] = data[1];
        data[1] = temp;

        let sample = (data[1] & 0xFF) << 8;
        sample |= (data[0] & 0xFF);

        if ((sample & 0x8000) > 0) {
          sample = sample - Math.pow(2, 16);
        }

        return sample;
      }

      /**
       * Does Not include byte swapping
       * @param  {Int8Array} data [description]
       * @return {?}      [description]
       */
      function as_i16(data) {
        if (data.length !== 2) {
          console.log(`Input Int8Array incorrect size`);
          return;
        }

        let sample = (data[1] & 0xFF) << 8;
        sample |= (data[0] & 0xFF);

        if ((sample & 0x8000) > 0) {
          sample = sample - Math.pow(2, 16);
        }

        return sample;
      }


      /**
       * Function to process any data that is sent through notification to the exchange characteristic
       * Any time we write to the `characteristics.exchange`, the data will be sent back through this callback
       * This method gets called many times per message
       * @param  {ArrayBuffer} data - data sent back in 20 byte packets from BLE device
       * @return {undefined}
       */
      function processMessage(data) {
        var idata = new Int8Array(data);  // convert array buffer into Int8Array
        // var sdata = as_ascii(idata);
        if (msgBuff) {
          var c = new Int8Array(msgBuff.length + idata.length);
          c.set(msgBuff);
          c.set(idata, msgBuff.length);
          msgBuff = c;
          // msgBuff = msgBuff.concat(idata);
        } else {
          msgBuff = idata;
        }

      }

      /**
       * Interpret a finished message sent from the svantek device
       * @return {undefined}
       */
      function interpretMessage() {
        if (!msgBuff) {
          return;
        }

        var statusByte = msgBuff[3];

        // this code compares 1/1 octave msbBuff with 1/3 octave msgBuff
        // var expectedLength = ((statusByte & (1 << 3)) !== 0) ? 68 : ((statusByte & (1 << 2)) !== 0) ? 30 : 0;
        // if (msgBuff.length !== expectedLength) {
        //   // This is likely an error, and should fail out.
        //   logger.log('Received a msgBuff of non-standard length: ' + msgBuff.length);
        //   logger.log('Expected length = ' + expectedLength);
        // }

        var dataArray = msgBuff.slice(6, msgBuff.length);  // loop to process with as_i16 to be processed
        if (dataArray.length !== 62) {
          logger.error('Data returned from svantek dosimeter does not have enough level values');
        }

        var Leq = [];
        var i = 0;
        while (i < dataArray.length-1) {
          Leq[Leq.length] = as_i16(dataArray.slice(i, i+2))/100;
          i = i + 2;
        }

        svantek.data = {
          time: (new Date()).toJSON(),
          status: statusByte,
          Leq: Leq.slice(0,28),   // grab the first 28 frequencies for 1/3 octave bands
          Frequencies: [20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000],
          // Frequencies: [10000, 8000, 6300, 5000, 4000, 3150, 2500, 2000, 1600, 1250, 1000, 800, 630, 500, 400, 315, 250, 200, 160, 125, 100, 80, 63, 50, 40, 31.5, 25, 20],
          LeqA: Leq[28],
          LeqC: Leq[29],
          LeqZ: Leq[30]
        };

        // reset message buffer
        msgBuff = undefined;
      }


      function startRecurring() {

        // start notification for the exchange characeristic
        tabsintble.startNotification(svantek.device.id, service, characteristics.exchange.uuid, processMessage, function(error) {
          logger.error(`Failed to start recurring notification with error: ${JSON.stringify(error)}`);
        });

        interval = setInterval(function() {
          interpretMessage();

          // collect data each time
          write(characteristics.exchange, from_ascii('#3;'));  // will get all available data
        }, 1000);
      }

      function stopRecurring() {

        tabsintble.stopNotification(svantek.device.id, service, characteristics.exchange.uuid, function(data) {
          logger.debug(`Stop svantek notification success`);
        }, function(error) {
          logger.error(`Failed to stop svantek notifications with error: ${JSON.stringify(error)}`);
        });

        clearInterval(interval);
      }


      /**
       * Connect to scanned device
       * @return {promise}              promise to connect to scanned device
       */
      function connectToDevice() {
        return tabsintble.connect(svantek.device.id, function() {
            // this function will get called if the svantek fails to connect, or disconnects abruptly while connected (perhaps much later)
            if (svantek.state === 'connected') {
              notifications.alert(`The tablet is having difficulty communicating with the dosimeter. Please try reconnecting to the dosimeter on the Admin page.`);
            }

            // if it disconnects while recording, try to stop first, then disconnect. Otherwise just disconnect
            if (svantek.state === 'recording') {
              return svantek.stop()
                .finally(function() {
                  svantek.disconnect();
                });
            } else {
              svantek.disconnect();
            }
            
          })
          .then(function() {
            if (svantek.state === 'connecting') {
              logger.debug(`Starting svantek device: ${svantek.device.name}`);
              return write(characteristics.start, new Int8Array([1]));
            }
            return $q.reject();
          })
          .then(function(data) {
            if (svantek.state === 'connecting') {
              logger.info(`Connected to svantek device: ${JSON.stringify(data ? data.name : '')}`);
              return write(characteristics.pin, new Int8Array([1, 2, 3, 4]));
            }
            return $q.reject();
          })
          .then(function() {
            if (svantek.state === 'connecting') {
              logger.debug(`Completed svantek connection process`);
              svantek.state = 'connected';
              return write(characteristics.exchange, from_ascii('#1,M3,f1;'));  // set the measurement to 1/3 octaves and no filter (z-filter)
            }
            return $q.reject();
          })
          .then(function() {
            logger.debug('Wrote control settings successfully.');
          })
          .catch(function(error) {
            tasks.register('svconnect', `Failed while connecting to ${svantek.device.name}. Attempting to reconnect.`);
            logger.error(`Failed to connect with error: ${JSON.stringify(error)}`);

            // attempt to disconnect, then try to reconnect after 1 second
            return svantek.disconnect()
              .finally(function() { // evaluates even if svantek.disconnect fails
                var q = $q.defer();

                setTimeout(function() {
                  svantek.connect()
                    .then(function() {
                      q.resolve();
                    }, function() {
                      q.reject();
                    });
                }, 1000);

                return q.promise;
              });
          });
      }

      /**
       * Connect to svantek device
       * @return {promise} promise to connect to device
       */
      svantek.connect = function() {
        var q = $q.defer();

        if (svantek.state === 'disconnected') {
          tasks.register('svconnect', 'Scanning for Svantek 104A');
          svantek.state = 'connecting';

          tabsintble.startScan([], function(scannedDevice) {
            logger.debug('ScannedDevice: ' + JSON.stringify(scannedDevice));
            if (scannedDevice && scannedDevice.name && scannedDevice.name.indexOf(deviceName) !== -1) {
              logger.debug(`Found svantek device: ${JSON.stringify(scannedDevice.name)}`);
              tasks.register('svconnect', `Connecting to Svantek ${scannedDevice.name}`);

              svantek.device = scannedDevice;
              svantek.device.advertising = new Int8Array(svantek.device.advertising);

              connectToDevice()
                .then(function() {
                  tabsintble.stopScan();
                  q.resolve();
                }, function(error) {
                  q.reject();
                });
            }
          }, function(error) {
            q.reject();
          });
        } else {
          q.reject();
        }
        // deregister task once its all done
        return q.promise.finally(function() {
          tasks.deregister('svconnect');
        });
      };

      svantek.cancelConnect = function() {
        svantek.state = 'cancelled';
        setTimeout(function() {
          return svantek.disconnect();
        }, 500);
      };

      /**
       * Disconnect from svantek device
       * @return {promise} promise to disconnect from device
       */
      svantek.disconnect = function() {
        svantek.state = 'disconnected';
        tasks.deregister('svconnect');
        return tabsintble.stopScan()
          .then(function() {
            if (svantek.device && svantek.device.id) {
              return tabsintble.disconnect(svantek.device.id)
                .finally(function() {
                  logger.info(`Disconnected from device: ${svantek.device.id}`);
                  svantek.device = undefined;
                });
            } else {
              logger.debug(`No svantek device to disconnect`);
              return $q.resolve();
            }
          });
      };

      /**
       * Start recording
       * @return {promise} promise to start recording from device
       */
      svantek.start = function() {
        svantek.data = undefined;
        if (svantek.device) {
          return write(characteristics.exchange, from_ascii('#1,S1;'))
            .then(function(success) {
              svantek.state = 'recording';
              startRecurring();
            });
        } else {
          notifications.alert('Attempted to start recording from dosimeter, but no dosimeter is connected.');
          return $q.reject();
        }
      };

      /**
       * Stop recording
       * @return {promise} promise to stop recording from device
       */
      svantek.stop = function() {
        if (svantek.device) {
          return write(characteristics.exchange, from_ascii('#1,S0;'))
            .then(function(success) {
              stopRecurring();
              svantek.state = 'connected';
              return svantek.data;
            });
        } else {
          logger.warn('Attempted to stop recording without a dosimeter connected.');
          return $q.reject();
        }
      };

      /**
       * On hughson westlake pages, this function will calculate the amount of noise in a certain octave Frequency band
       * Called by exam.js
       * Parameter is used by the audiometryTable page
       * @param  {array} L - array of 1/3 octave band levels from svantek.data result
       * @param  {number} F - octave band frequency of interest
       * @return {number}      amount of band level noise
       */
      svantek.calculateBandLevel = function(svantekResult, F) {
          // Values for noise calculations
          var thirdOctaveBands = svantekResult.Frequencies;
          
          // Find 1/3 octave band indexes and noise levels
          var idx = thirdOctaveBands.indexOf(F);

          if (idx > 0 && idx < svantekResult.Leq.length - 1) {
            var Lcenter = svantekResult.Leq[idx];
            var Llower = svantekResult.Leq[idx-1];
            var Lupper = svantekResult.Leq[idx+1];
            
            // Now we can do the band level calculation
            // @val - what happens at the outer bounds? idx === 0 and idx === L.length?
            return 10*Math.log10(Math.pow(10,Llower/10)+Math.pow(10,Lcenter/10)+Math.pow(10,Lupper/10));
          } else {
            return undefined;
          }
      };

      return svantek;
    });
});
