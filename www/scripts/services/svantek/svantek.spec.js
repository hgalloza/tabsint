define(['test-dep', 'app'], function() {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Svantek', function() {

    var svantek;
    beforeEach(inject(function(_svantek_) {
      svantek = _svantek_;
    }));

    it('should load the svantek factory', function () {
      expect(svantek).toBeDefined();
    });

  });
});
