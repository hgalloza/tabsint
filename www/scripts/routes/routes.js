/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular',
  './tabsint-content/tabsint-content',
  './exam/exam',
  './welcome/welcome',
  './admin/admin'
  ], function (angular) {
  'use strict';

  angular.module('tabsint.routes', [
    'tabsint.routes.tabsint-content',
    'tabsint.routes.welcome',
    'tabsint.routes.exam',
    'tabsint.routes.admin'
  ]);

});
