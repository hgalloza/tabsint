/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.routes.tabsint-content', [])

    .directive('tabsintContent', function () {
      return {
        templateUrl: 'scripts/routes/tabsint-content/tabsint-content.html',
        controller: 'TabsintCtrl',
        scope: {}
      };
    })

    .controller('TabsintCtrl', function ($scope, router) {
      $scope.router = router;
    });

});