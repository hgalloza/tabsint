

/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.routes.exam', [])

    .directive('examView', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/routes/exam/exam.html',
        controller: 'ExamCtrl',
        scope: {}
      };
    })

    .controller('ExamCtrl', function ($scope, pm, adminLogic, examLogic, page, disk, $sce, remote) {
      
      $scope.pm = pm;
      $scope.admin = adminLogic;
      $scope.disk = disk;
      $scope.link = remote.link;
      $scope.isString = angular.isString;

      // exam logic actions
      $scope.dm = examLogic.dm;
      $scope.page = page;
      $scope.examLogic = examLogic;

      // trust all input as html
      $scope.trustAsHtml = $sce.trustAsHtml;

    });
});