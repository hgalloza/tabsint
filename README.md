# TabSINT

[![](resources/android/icon/drawable-xxhdpi-icon.png)](https://creare-com.gitlab.io/tabsint/)

- [Documentation](http://tabsint.org)
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

TabSINT is an open source platform for administering tablet based hearing-related exams, as well as general-purpose questionnaires. 
It is meant to be flexible, easy-to-use, and useful for administrators who manage studies of all sizes.

Exams and questionnaires are presented as a series of pages using a crisp and intuitive interface. 
Results are uploaded to a central server or saved locally on the tablet.

This software is &copy; Creare 2015-2018, released under the Apache v2 License provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). Please see [License](#license) for additional restrictions and licensing information. License agreements for open source libraries used in the project are listed in [NOTICE](https://gitlab.com/creare-com/tabsint/blob/master/NOTICE.md).

## Releases

The latest TabSINT releases for Android are available on the [Releases Page](http://tabsint.org/releases).

## Quick Start

To get started using TabSINT on a tablet, see the [TabSINT Quick Start](http://tabsint.org/docs/QuickStart/TabSINT/). If you are interested in making changes to the TabSINT source code, see the [Developer Guide](http://tabsint.org/docs/DeveloperGuide/).

## How to Cite

Use the following citations to attribute TabSINT and WAHTS headset in publications. See [References](http://tabsint.org/docs/references/) for more information.

- *TabSINT Mobile Framework for Hearing Related Tests and Questionnaires*. Creare, 2018. <http://tabsint.org>
- Meinke, D. K., Norris, J. A., Flynn, B. P., & Clavier, O. H. (2017). *Going wireless and booth-less for hearing testing in industry*. International Journal of Audiology, 56(sup1), 41-51.


## Translations

TabSINT supports translations in the `gettext` format using the [`angular-gettext`](https://angular-gettext.rocketeer.be/) library. To add a translation:

- Use [Poedit](https://poedit.net/) to open `translations/extract.pot`
- Create a new translation file `language.po` in the `translations` directory.
- Run `npm run compile-translations` and build the application.

To extend the translation support in the code base:

- Add the `translate` directive to any HTML tag that you want to translate.
- After adding the tag, run the script `npm run extract-translations`
- Add the appropriate translations in Poedit using `Update from POT file` from the `Catalog` menu, then opening `extract.pot` 
- To view the newly added HTML tags to translate, select `Untranslated entries first` from the `View` menu
- Run `npm run compile-translations`


## License

Code and documentation Copyright (C) 2015-2018 Creare. Code released under the Apache v2 License, provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). All rights reserved.

License agreements for open source libraries used in the project can be found in [NOTICE](https://gitlab.com/creare-com/tabsint/blob/master/NOTICE.md).

Creare has used commercially reasonable efforts in preparing the
TabSINT Software but makes no guarantee or warranty of any nature
with regard to its use, performance, or operation.

Creare makes no representations or warranties, and Creare shall
incur no liability or other obligation of any nature whatsoever to
any person from any and all actions arising from the use of this
software.  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. The final
responsibility for the proper use and functioning of the TabSINT
Software shall rest solely with the USAMRAA.

## Acknowledgement

This code development was supported by the US Army Medical Research Materiel Command and the Army Public Health Command under SBIR Phase III Award #W81XWH-13-C-0194 to [Creare LLC](http://www.creare.com). In particular, we gratefully acknowledge the support and contributions of the Audiology and Speech Center at the Walter Reed National Military Medical Center and the Department of Defense Hearing Center of Excellence in the development and extensive testing of this software.
